* convert installer to ebuild

* fix add_to_playlist

* make submenu system

* caching requests in a ramdisk

* websockets for fetching player time & status

* embed style sheet on request; make only a single transfer

* can svg be embedded?

* must add last-seen member to session

* pydoc or sphinx
