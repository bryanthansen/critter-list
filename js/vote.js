/*
 * Bryant Hansen
 * javascript voting system
 */

var debugCache;

function DebugMessage(newText) {
    var timestamp = new Date().toString().split(" ")[4] + " ";
    debugTextboxObj = document.getElementById("jsDebugTextbox");
    if (debugTextboxObj) {
        debugTextboxObj.value += timestamp + " " + newText + "\n";
        debugCache = debugTextboxObj.value;
      //if ((debugTextboxObj.scrollTop) && (debugTextboxObj.scrollHeight)) {
            debugTextboxObj.scrollTop = debugTextboxObj.scrollHeight;
      //}
    }
}

/*
 * function viewport() from:
 * http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript
 *
 * thanks! :)
 *
 */
function viewport() {
    var e = window, a = 'inner';
    if (!( 'innerWidth' in window )) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
}

function basename(str) {
   return path.split('/').reverse()[0];
}

function fetch_server_debug() {
}

function submitVote(vote, sessionId, trackId, updateElement) {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else {
        // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    vp = viewport();
    DebugMessage ("vote: window.location = " + window.location);
    url = "vote.wsgi?"  + "sessionId=" + sessionId
                        + "&trackId=" + trackId
                        + "&width=" + vp.width
                        + "&height=" + vp.height
                        + "&vote=" + vote
                        + "&page=" + basename(window.location.pathname);
    DebugMessage ("url = " + url);
    xmlhttp.open("GET", url, true);
    xmlhttp.send();

    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                location_basename = basename(window.location.pathname)
                DebugMessage("location_basename = " + location_basename + ", updateElement = " + updateElement);
                if (location_basename == "tracks.wsgi") {
                    document.getElementById(updateElement).innerHTML = xmlhttp.responseText;
                    DebugMessage("updated " + updateElement);
                }
                else if (location_basename == "critter.wsgi") {
                    document.getElementById("div_tables").innerHTML = xmlhttp.responseText;
                    DebugMessage("updated div#div_tables");
                }
                else if (location_basename == "player_playlist.wsgi") {
                    document.getElementById(updateElement).innerHTML = xmlhttp.responseText;
                    // updateElement.innerHTML = xmlhttp.responseText;
                    DebugMessage("updated " + updateElement);
                }
                else {
                    document.getElementById("table_tracks").innerHTML = xmlhttp.responseText;
                    DebugMessage("unknown path " + window.location.pathname + "  updated div#div_tables.  basename = " + location_basename);
                }
                pyDebugTextboxObj = document.getElementById("pyDebugTextbox");
                if (pyDebugTextboxObj) {
                    pyDebugTextboxObj.scrollTop = pyDebugTextboxObj.scrollHeight;
                }
            }
            else {
                DebugMessage("submitVote xmlhttp.onreadystatechange: xmlhttp.status = " + xmlhttp.status);
            }
        }
        /*
        else {
            DebugMessage("submitVote xmlhttp.onreadystatechange: xmlhttp.readyState = " + xmlhttp.readyState);
        }
        */
    }

}

function vote_yae(sessionId, trackId, updateElement) {
    DebugMessage( "vote_yae: sessionId = " + sessionId + 
                  ", trackId = " + trackId +
                  ", updateElement = " + updateElement);
    submitVote("yae", sessionId, trackId, updateElement);
}

function vote_nay(sessionId, trackId, updateElement) {
    DebugMessage( "vote_nay: sessionId = " + sessionId +
                  ", trackId = " + trackId +
                  ", updateElement = " + updateElement);
    submitVote("nay", sessionId, trackId, updateElement);
}

function delete_voter(sessionId, deletee) {
    DebugMessage("delete_voter: sessionId = " + sessionId + ", voterId(deletee) = " + deletee);
    if (window.XMLHttpRequest) {  // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else {  // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                location_basename = basename(window.location.pathname)
                DebugMessage("location_basename = " + location_basename);
                //DebugMessage("len(xmlhttp.responseText) = " + len(xmlhttp.responseText));
                if (location_basename == "voters"){
                    // if (len(xmlhttp.responseText) > 0) {
                        document.getElementById("table_voters").innerHTML = xmlhttp.responseText;
                    //}
                    //else {
                    //    document.getElementById("table_voters").innerHTML = "<br />NO ACTIVE VOTER<br />";
                    //}
                    DebugMessage("updated table#table_voters");
                }
                else {
                    document.getElementById("div_tables").innerHTML = xmlhttp.responseText;
                    DebugMessage("unknown path " + window.location.pathname + "  updated div#div_tables.  basename = " + location_basename);
                }
                pyDebugTextboxObj = document.getElementById("pyDebugTextbox");
                if (pyDebugTextboxObj) {
                    pyDebugTextboxObj.scrollTop = pyDebugTextboxObj.scrollHeight;
                }
            }
            else if (xmlhttp.status == 401) {
                window.location.href=window.location.href
            }
            else {
                DebugMessage("submitVote xmlhttp.onreadystatechange: xmlhttp.status = " + xmlhttp.status);
            }
        }
    }
    vp = viewport();
    DebugMessage ("delete_voter: vp.width = " + vp.width + ", vp.height = " + vp.height);
    DebugMessage ("delete_voter: window.location = " + window.location);
    url = "delete_voter.wsgi?sessionId=" + sessionId +
                                "&page=" + window.location.pathname +
                                "&deletee=" + deletee,
                                "&width=" + vp.width +
                                "&height=" + vp.height;
    DebugMessage ("url = " + url);
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function disconnect_voter(sessionId, voterId, disconnectee) {
    DebugMessage("delete_voter: sessionId = " + sessionId + ", voterId = " + voterId + ", voterId(disconnectee) = " + disconnectee);
    if (window.XMLHttpRequest) {  // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else {  // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                location_basename = basename(window.location.pathname)
                DebugMessage("location_basename = " + location_basename);
                //DebugMessage("len(xmlhttp.responseText) = " + len(xmlhttp.responseText));
                if (location_basename == "voters"){
                    // if (len(xmlhttp.responseText) > 0) {
                        document.getElementById("table_voters").innerHTML = xmlhttp.responseText;
                    //}
                    //else {
                    //    document.getElementById("table_voters").innerHTML = "<br />NO ACTIVE VOTER<br />";
                    //}
                    DebugMessage("updated table#table_voters");
                }
                else {
                    document.getElementById("div_tables").innerHTML = xmlhttp.responseText;
                    DebugMessage("unknown path " + window.location.pathname + "  updated div#div_tables.  basename = " + location_basename);
                }
                pyDebugTextboxObj = document.getElementById("pyDebugTextbox");
                if (pyDebugTextboxObj) {
                    pyDebugTextboxObj.scrollTop = pyDebugTextboxObj.scrollHeight;
                }
            }
            else if (xmlhttp.status == 401) {
                window.location.href=window.location.href
            }
            else {
                DebugMessage("disconnect_voter xmlhttp.onreadystatechange: xmlhttp.status = " + xmlhttp.status);
            }
        }
    }
    vp = viewport();
    DebugMessage ("disconnect_voter: vp.width = " + vp.width + ", vp.height = " + vp.height);
    DebugMessage ("disconnect_voter: window.location = " + window.location);
    url = "disconnect_voter.wsgi?sessionId=" + sessionId +
                                "&page=" + window.location.pathname +
                                "&voterId=" + voterId +
                                "&disconnectee=" + disconnectee,
                                "&width=" + vp.width +
                                "&height=" + vp.height;
    DebugMessage ("url = " + url);
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function delete_vote(sessionId, voterId, voteId) {
    DebugMessage("delete_vote:: sessionId = " + sessionId + ", voterId = " + voterId + ", voteId = " + voteId);
    if (window.XMLHttpRequest) {  // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else {  // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                location_basename = basename(window.location.pathname)
                DebugMessage("location_basename = " + location_basename);
                if ((location_basename == "critter") || (location_basename == "admin")){
                    document.getElementById("div_tables").innerHTML = xmlhttp.responseText;
                    DebugMessage("updated div#div_tables");
                }
                else {
                    document.getElementById("div_tables").innerHTML = xmlhttp.responseText;
                    DebugMessage("unknown path " + window.location.pathname + "  updated div#div_tables.  basename = " + location_basename);
                }
                pyDebugTextboxObj = document.getElementById("pyDebugTextbox");
                if (pyDebugTextboxObj) {
                    pyDebugTextboxObj.scrollTop = pyDebugTextboxObj.scrollHeight;
                }
            }
            else {
                DebugMessage("submitVote xmlhttp.onreadystatechange: xmlhttp.status = " + xmlhttp.status);
            }
        }
        else {
            DebugMessage("delete_vote xmlhttp.onreadystatechange: xmlhttp.readyState = " + xmlhttp.readyState);
        }
    }
    vp = viewport();
    DebugMessage ("delete_vote: vp.width = " + vp.width + ", vp.height = " + vp.height);
    DebugMessage ("delete_vote: window.location = " + window.location);
    url = "delete_vote.wsgi?sessionId=" + sessionId +
                                "&page=" + window.location.pathname +
                                "&voterId=" + voterId +
                                "&width=" + vp.width +
                                "&height=" + vp.height +
                                "&voteId=" + voteId;
    DebugMessage ("url = " + url);
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function delete_track(sessionId, voterId, trackId) {
    DebugMessage("delete_track: sessionId = " + sessionId + ", voterId = " + voterId + ", trackId = " + trackId);
    if (window.XMLHttpRequest) {  // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else {  // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                location_basename = basename(window.location.pathname)
                DebugMessage("location_basename = " + location_basename);
                //DebugMessage("len(xmlhttp.responseText) = " + len(xmlhttp.responseText));
                if ((location_basename == "critter") || (location_basename == "admin")){
                    // if (len(xmlhttp.responseText) > 0) {
                        document.getElementById("div_tables").innerHTML = xmlhttp.responseText;
                    //}
                    //else {
                    //    document.getElementById("div_tables").innerHTML = "<br />NO ACTIVE VOTER<br />";
                    //}
                    DebugMessage("updated div#div_tables");
                }
                else {
                    document.getElementById("div_tables").innerHTML = xmlhttp.responseText;
                    DebugMessage("unknown path " + window.location.pathname + "  updated div#div_tables.  basename = " + location_basename);
                }
                pyDebugTextboxObj = document.getElementById("pyDebugTextbox");
                if (pyDebugTextboxObj) {
                    pyDebugTextboxObj.scrollTop = pyDebugTextboxObj.scrollHeight;
                }
            }
            else {
                DebugMessage("delete_track xmlhttp.onreadystatechange: xmlhttp.status = " + xmlhttp.status);
            }
        }
    }
    vp = viewport();
    DebugMessage ("delete_track: vp.width = " + vp.width + ", vp.height = " + vp.height);
    DebugMessage ("delete_track: window.location = " + window.location);
    url = "delete_track.wsgi?sessionId=" + sessionId +
                                "&page=" + window.location.pathname +
                                "&voterId=" + voterId +
                                "&width=" + vp.width +
                                "&height=" + vp.height +
                                "&voteId=" + voteId;
    DebugMessage ("url = " + url);
    xmlhttp.open("GET", url, true);




    xmlhttp.send();
}

function loadPage() {

    d = new Date();
    StartTime = d.getTime();

    //is = new Is();

    // Call the args_init () function to set up the args [] array:
    // args_init ();

    if (document.getElementById) {
        debugTextboxObj = document.getElementById("jsDebugTextbox");
    }
    //if (debugTextboxObj) debugTextboxObj.value = "";
    DebugMessage ("loadPage complete");
    DebugMessage ("screen.width = " + screen.width + ", screen.height = " + screen.height);
    DebugMessage ("screen.availWidth = " + screen.availWidth + ", screen.availHeight = " + screen.availHeight);

    vp = viewport();
    DebugMessage ("vp.width = " + vp.width + ", vp.height = " + vp.height);

    pyDebugTextboxObj = document.getElementById("pyDebugTextbox");
    if (pyDebugTextboxObj) {
        pyDebugTextboxObj.scrollTop = pyDebugTextboxObj.scrollHeight;
    }

    // DebugMessage ("loadPage complete, screen.width + " screen.width + ", screen.height = " + screen.height);
    //DebugMessage ("window.location = " + window.location);
    //DebugMessage ("window.location.pathname = " + window.location.pathname);

}
