/*
 * Bryant Hansen
 * javascript player controls
 */

var debugCache;

function basename(path) {
   return path.split('/').reverse()[0];
}

function play(sessionId, voterId) {
    if (window.XMLHttpRequest) {  // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else {  // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                location_basename = basename(window.location.pathname)
                DebugMessage("location_basename = " + location_basename);
                if (location_basename == "tracks") {
                    document.getElementById("div_tracks").innerHTML = xmlhttp.responseText;
                    DebugMessage("updated div#div_tracks");
                }
                else if (location_basename == "critter") {
                    document.getElementById("div_tables").innerHTML = xmlhttp.responseText;
                    DebugMessage("updated div#div_tables");
                }
                else if (location_basename == "player") {
                    document.getElementById("player_interface").innerHTML = xmlhttp.responseText;
                    DebugMessage("updated td#player_interface");
                }
                else {
                    document.getElementById("div_tables").innerHTML = xmlhttp.responseText;
                    DebugMessage("unknown path " + window.location.pathname + "  updated div#div_tables.  basename = " + location_basename);
                }
                pyDebugTextboxObj = document.getElementById("pyDebugTextbox");
                if (pyDebugTextboxObj) {
                    pyDebugTextboxObj.scrollTop = pyDebugTextboxObj.scrollHeight;
                }
            }
            else {
                DebugMessage("play xmlhttp.onreadystatechange: xmlhttp.status = " + xmlhttp.status);
            }
        }
    }

    vp = viewport();
    url = "player_command.wsgi?command=play" +
                        "&sessionId=" + sessionId +
                        "&voterId=" + voterId +
                        "&width=" + vp.width +
                        "&height=" + vp.height +
                        "&page=" + basename(window.location.pathname);
    DebugMessage ("play: url = " + url);
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function seek_end(sessionId, voterId) {
    if (window.XMLHttpRequest) {  // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else {  // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                location_basename = basename(window.location.pathname)
                DebugMessage("location_basename = " + location_basename);
                if (location_basename == "tracks") {
                    document.getElementById("div_tracks").innerHTML = xmlhttp.responseText;
                    DebugMessage("updated div#div_tracks");
                }
                else if (location_basename == "critter") {
                    document.getElementById("div_tables").innerHTML = xmlhttp.responseText;
                    DebugMessage("updated div#div_tables");
                }
                else if (location_basename == "player") {
                    document.getElementById("div_playlist").innerHTML = xmlhttp.responseText;
                    DebugMessage("updated div#div_playlist");
                }
                else {
                    document.getElementById("div_tables").innerHTML = xmlhttp.responseText;
                    DebugMessage("unknown path " + window.location.pathname + "  updated div#div_tables.  basename = " + location_basename);
                }
                pyDebugTextboxObj = document.getElementById("pyDebugTextbox");
                if (pyDebugTextboxObj) {
                    pyDebugTextboxObj.scrollTop = pyDebugTextboxObj.scrollHeight;
                }
            }
            else {
                DebugMessage("submitVote xmlhttp.onreadystatechange: xmlhttp.status = " + xmlhttp.status);
            }
        }
    }

    vp = viewport();
    url = "player_command.wsgi?command=seek_end" +
                        "&sessionId=" + sessionId +
                        "&voterId=" + voterId +
                        "&width=" + vp.width +
                        "&height=" + vp.height +
                        "&page=" + window.location.pathname;
    DebugMessage ("url = " + url);
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function do_command(sessionId, voterId, command) {
    if (window.XMLHttpRequest) {  // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else {  // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                location_basename = basename(window.location.pathname)
                DebugMessage("location_basename = " + location_basename);
                if (location_basename == "tracks") {
                    document.getElementById("div_tracks").innerHTML = xmlhttp.responseText;
                    DebugMessage("updated div#div_tracks");
                }
                else if (location_basename == "critter") {
                    document.getElementById("div_tables").innerHTML = xmlhttp.responseText;
                    DebugMessage("updated div#div_tables");
                }
                else if (location_basename == "player") {
                    document.getElementById("div_playlist").innerHTML = xmlhttp.responseText;
                    DebugMessage("updated div#div_playlist");
                }
                else {
                    document.getElementById("div_tables").innerHTML = xmlhttp.responseText;
                    DebugMessage("unknown path " + window.location.pathname + "  updated div#div_tables.  basename = " + location_basename);
                }
                pyDebugTextboxObj = document.getElementById("pyDebugTextbox");
                if (pyDebugTextboxObj) {
                    pyDebugTextboxObj.scrollTop = pyDebugTextboxObj.scrollHeight;
                }
            }
            else {
                DebugMessage("submitVote xmlhttp.onreadystatechange: xmlhttp.status = " + xmlhttp.status);
            }
        }
    }

    vp = viewport();
    url = "player_command.wsgi?command=" + command +
                        "&sessionId=" + sessionId +
                        "&voterId=" + voterId +
                        "&width=" + vp.width +
                        "&height=" + vp.height +
                        "&page=" + window.location.pathname;
    DebugMessage ("url = " + url);
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function gong(sessionId) {
    if (window.XMLHttpRequest) {  // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else {  // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                document.getElementById("table#div_gong").innerHTML = xmlhttp.responseText;
                DebugMessage("updated table#div_gong");
            }
            else {
                DebugMessage(
                  "play xmlhttp.onreadystatechange: xmlhttp.status = "
                  + xmlhttp.status
                );
            }
        }
    }
    vp = viewport();
    url = "do_gong.wsgi?" +
                        "sessionId=" + sessionId +
                        "&width="     + vp.width +
                        "&height="    + vp.height +
                        "&page="      + basename(window.location.pathname);
    DebugMessage ("gong: url = " + url);
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function set_player_playlist(sessionId, playlistId) {
    if (window.XMLHttpRequest) {  // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else {  // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                document.getElementById("table#table_playlists").innerHTML = xmlhttp.responseText;
                DebugMessage("updated table#table_playlists");
            }
            else {
                DebugMessage(
                  "play xmlhttp.onreadystatechange: xmlhttp.status = "
                  + xmlhttp.status
                );
            }
        }
    }
    vp = viewport();
    url = "set_player_playlist.wsgi?" +
                        "sessionId="   + sessionId +
                        "&playlistId=" + playlistId +
                        "&width="      + vp.width +
                        "&height="     + vp.height +
                        "&page="       + basename(window.location.pathname);
    DebugMessage ("set_player_playlist: url = " + url);
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

