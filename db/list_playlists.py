#!/usr/bin/python3 -u
# Bryant Hansen

import sys
import os
import sqlite3
import critterdb_lib
from trace import TRACE

ME = os.path.basename(sys.argv[0])
USAGE = "%s" % ME
VERBOSE = True

USAGE = "%s  DB_FILE" % ME


def get_and_validate_args(argv):
    if len(argv) <= 1:
        sys.stderr.write("ERROR: this script requires 1 argument\n")
        sys.stderr.write("USAGE:\n   %s\n" % USAGE)
        sys.exit(2)

    db_file = argv[1]
    if not os.path.exists(db_file):
        TRACE('%s ERROR: database %s does not exist.  Exiting abnormally\n'
                % (ME, db_file))
        sys.exit(3)

    return db_file


SUCCESS = 0
if __name__ == '__main__':
    TRACE('enter %s' % str(sys.argv))

    db_file = get_and_validate_args(sys.argv)
    if db_file == None:
        TRACE("Failed to get db_file from command args")
        sys.exit(2)
    conn = critterdb_lib.connect_to_db(db_file)
    if conn == None:
        TRACE("Failed to open connection to database %s" % str(db_file))
        sys.exit(3)
    playlists = critterdb_lib.list_playlists(conn)
    if playlists == None:
        TRACE("Failed to find any playlists in db_file %s"
              % (str(db_file)))
        sys.exit(3)
    for playlist in playlists:
        print("Playlist %s:" % playlist['name'])
        for key in playlist.keys():
            if key == "name": continue
            print("   %s: %s" % (key, str(playlist[key])))

    sys.exit(SUCCESS)

