#!/bin/sh
# Bryant Hansen
# GPLv3

# Description:
# Crate a new black database for the critter list

ME="$(basename "$0")"

CONF="./critters.conf"

USAGE="$ME  database-file"

DB="$1"
[[ -f "$CONF" ]] && . $CONF

if [[ ! "$DB" ]] ; then
    echo "$ME ERROR: no database file specified" >&2
    echo "USAGE: $USAGE" >&2
    exit 2
fi
if [[ -f "$DB" ]] ; then
    echo "$ME ERROR: database $DB already exists" >&2
    echo "USAGE: $USAGE" >&2
    exit 3
fi

cat blank.db.dump | sqlite3 "$DB"
