#!/usr/bin/python3 -u
# Bryant Hansen

'''
This script will return the next tracks to be played from a playlist, based on the number of votes, the last-played-time, and the presence of the files on the filesystem.
'''

import sys
import os
import sqlite3
import critterdb_lib
import find_track
from trace import TRACE

ME = sys.argv[0]
USAGE = "%s" % ME
VERBOSE = True

USAGE = "%s  DB_FILE  PLAYLIST_ID  NUM_TRACKS" % ME


def get_and_validate_args(argv):
    if len(argv) != 3:
        sys.stderr.write("ERROR: this script requires 2 arguments\n")
        sys.stderr.write("USAGE:\n   %s\n" % USAGE)
        sys.exit(2)

    db_file = argv[1]
    playlist_name = argv[2]

    if not os.path.exists(db_file):
        TRACE('%s ERROR: database %s does not exist.  Exiting abnormally\n'
                % (ME, db_file))
        sys.exit(3)

    return db_file, playlist_name


SUCCESS = 0
if __name__ == '__main__':
    TRACE('enter %s' % str(sys.argv))

    db_file, playlist_name = get_and_validate_args(sys.argv)
    if db_file == None:
        TRACE("Failed to get db_file from command args")
        sys.exit(2)
    conn = critterdb_lib.connect_to_db(db_file)
    if conn == None:
        TRACE("Failed to open connection to database %s" % str(db_file))
        sys.exit(3)
    cur = conn.cursor()
    playlist_record = critterdb_lib.get_playlist(conn, playlist_name)
    if playlist_record == None:
        TRACE("Failed to get id of playlist %s, db_file %s"
              % (str(playlist_name), str(db_file)))
        sys.exit(3)
    playlist_id = playlist_record['id']
    # somewhat minimal version:
    #playlist_records = critterdb_lib.dump_playlist_tracks(conn, playlist_id)
    # detailed version:
    playlist_records = critterdb_lib.get_next_N_tracks(conn, playlist_id, 5)
    for track in playlist_records:
        print("URL %s:" % track['url'])
        for key in track.keys():
            if key == "url": continue
            if track[key] == None: continue
            print("   %s: %s" % (key, str(track[key])))
    sys.exit(SUCCESS)

