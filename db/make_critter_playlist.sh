#!/bin/sh
# Bryant Hansen

ME="$(basename "$0")"
DB="$1"
PLAYLIST="$2"
USAGE="$ME  DB  PLAYLIST"


CONF="./critters.conf"

TRUE=0
FALSE=1

db_exists() {
    [[ -f "$1" ]] && return $TRUE
    return $FALSE
}

table_exists() {
    retval="$(sqlite3 "$DB" \
        "SELECT name FROM sqlite_master WHERE type='table' AND name='$1';"
    )"
    [[ "$retval" ]] && return $TRUE
    return $FALSE
}

create_playlist() {
    local playlist_name="$1"
    local max_tracks="$2"
    echo "create_playlist($playlist_name)" >&2
    create_playlist "$playlist_name"
}

get_playlist_id() {
    local playlist_name="$1"
    records="$(
            sqlite3 $DB \
                    "
                    SELECT id FROM playlists 
                    WHERE name = '${playlist_name}';
                    " \
            | head -n 1
              )"
    if [[ "$records" ]] ; then
        echo "$records"
        return 0
    else
        echo "get_playlist_id: failed to return any records for playlists matching '${playlist_name}'" >&2
        echo "0"
        return 1
    fi
}

get_track_id_for_filename() {
    DB="$1"
    fileName="$2"
    query="SELECT id FROM tracks WHERE url = '$fileName';"
    echo "${ME}:INFO:SQL: $query" >&2
    trackId="$(sqlite3 "$DB" "$query")"
    echo "$trackId"
    [[ "$trackId" ]] && return $TRUE
    return $FALSE
}

add_track() {
    
}


# it must first be in the tracks table before it can be in the playlists table
add_file_to_playlist() {
    DB="$1"
    playlistId="$2"
    fileName="$3"
    trackId="$(get_track_id_for_filename "$DB" "$fileName")"
    if [[ ! "$trackId" ]] ; then
        add_track "$fileName"
    fi
    trackId="$(get_track_id_for_filename "$DB" "$fileName")"
    if [[ ! "$trackId" ]] ; then
        echo "$ME ERROR: failed to create track entry in DB '$DB' for file $fileName"
        return 2
    fi
    if [[ "$playlistId" -gt 0 ]] ; then
        sqlite3 "$DB" "SELECT id FROM tracks" \
        | sort -R \
        | head -n $max_tracks \
        | while read trackId ; do
            echo "INSERT INTO playlist_tracks (playlist_id, track_num) VALUES ('${playlistId}', '${trackId}');"
        done \
        | sqlite3 "$DB"
    else
        echo "no playlist found for playlistId $playlistId" >&2
        echo "cannot create playlist" >&2
    fi
    return $?
}

if ! db_exists "$DB" ; then
    echo "$ME ERROR: DB $DB does not exist" >&2
    exit 2
fi

if ! table_exists "$PLAYLIST" ; then
    echo "$ME TODO: create table '$PLAYLIST' in database '$DB'" >&2
    exit 3
fi

playlistId="$(get_playlist_id "$playlist_name")"
echo "playlistId = $playlistId" >&2

if [[ "$3" ]] ; then
    if [[ "$4" ]] ; then
        echo "$ME INFO File(s) specified on the command line" >&2
    else
        echo "$ME INFO File specified on the command line" >&2
    fi
    # read tracks either from the remaining arguments or from stdin
    # if extra arguments exist
    while "$3" ; do
        echo "$ME INFO adding track '$3'..." >&2
        add_file_to_playlist "$DB" $playlistId "$3"
        shift
    done
else
    echo "$ME INFO no files specified on the command line; reading from stdin" >&2
    while read inputline ; do
        echo "$ME INFO adding track '$inputline'..." >&2
        add_file_to_playlist "$DB" $playlistId "$3"
    done
fi
