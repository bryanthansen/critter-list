#!/usr/bin/python3 -u
# Bryant Hansen

import sys
import os
import sqlite3
import critterdb_lib
import find_track
from trace import TRACE

ME = sys.argv[0]
USAGE = "%s" % ME
VERBOSE = True

USAGE = "%s  DB_FILE  PLAYLIST_NAME  MEDIA_FILE  [ MEDIA_FILE ... ]" % ME


# check for existence of playlist
#   error if non-existent
# check for track in DB (find_track)
#   add track if non-existent
# add track ID to playlist


def get_and_validate_args(argv):
    if len(argv) <= 3:
        sys.stderr.write("ERROR: this script requires at least 3 arguments\n")
        sys.stderr.write("USAGE:\n   %s\n" % USAGE)
        sys.exit(2)

    db_file = argv[1]
    playlist_name = argv[2]
    media_url = argv[3]

    if not os.path.exists(db_file):
        TRACE('%s ERROR: database %s does not exist.  Exiting abnormally\n'
                % (ME, db_file))
        sys.exit(3)

    if not os.path.exists(media_url):
        TRACE('%s ERROR: media file %s does not exist.  Exiting abnormally\n'
                % (ME, db_file))
        sys.exit(4)

    return db_file, playlist_name


SUCCESS = 0
if __name__ == '__main__':
    TRACE('enter %s' % str(sys.argv))

    db_file, playlist_name = get_and_validate_args(sys.argv)
    if db_file == None:
        TRACE("Failed to get db_file from command args")
        sys.exit(2)
    conn = critterdb_lib.connect_to_db(db_file)
    if conn == None:
        TRACE("Failed to open connection to database %s" % str(db_file))
        sys.exit(3)
    cur = conn.cursor()
    playlist_record = critterdb_lib.get_playlist(conn, playlist_name)
    if playlist_record == None:
        TRACE("Failed to get id of playlist %s, db_file %s"
              % (str(playlist_name), str(db_file)))
        sys.exit(3)
    playlist_id = playlist_record['id']
    if playlist_id <= 0:
        TRACE("invalid playlist id returned from get_playlist(%s): %s"
              % (playlist_name, str(playlist_id)))
        sys.exit(4)
    for url in sys.argv[3:]:
        record = critterdb_lib.get_url_record(conn, url)
        url_id = record['id']
        rpath = record['rpath']
        if rpath != url:
            TRACE("received matching URL for specified track %s => %s"
                  % (url, rpath))
        track_record = critterdb_lib.get_track(conn, url_id)
        if track_record == None:
            TRACE("no track record found for URL %s (id %s)" % (rpath, url_id))
            continue
        track_id = track_record['id']
        TRACE("track ID matching URL %s (id %s) is %s"
              % (rpath, str(url_id), str(track_id)))
        if track_id <= 0:
            TRACE("invalid track id returned from get_track(%s): %s"
                % (url, str(track_id)))
        critterdb_lib.remove_from_playlist(conn, playlist_id, track_id)

    sys.exit(SUCCESS)

