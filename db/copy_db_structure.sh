#!/bin/bash
# Bryant Hansen

# The following was shown to work:
# ./copy_db_structure.sh | sqlite3 test1.db

INFILE=/var/www/localhost/critter_list/db/critters.db
#OUTFILE=/projects/critter_list/db/create_critter_db.sql.$(dt)

[[ "$1" ]] && INFILE="$1"
[[ "$2" ]] && OUTFILE="$2"

if [[ ! -f "$INFILE" ]] ; then
    echo "INFILE $INFILE does not exist.  Exiting." >&2
    exit 2
fi
if [[ ! "$OUTFILE" ]] ; then
    echo "OUTFILE not specified.  Writing to stdout" >&2
else
    if [[ -f "$OUTFILE" ]] ; then
        echo "OUTFILE $OUTFILE already exists.  Exiting." >&2
        exit 3
    fi
fi

# should .schema be used here?
sqlite3 "$INFILE" .dump \
| grep -v "^INSERT\ INTO\ .*)\;" \
| sed "/^INSERT INTO \"/,/)\;/d" \
| (
    # this is a method of optionally writing to stdout or a file
    [[ "$OUTFILE" ]] && cat - > "$OUTFILE" || cat -
)

