#!/usr/bin/python3 -u
# Bryant Hansen

'''
@brief read a media file from the filesystem, extract the id3 tags, and
update the database with all relevant information

Several python-3 tag libraries were attempted


I just wasted so much time trying to get and use and id3v2.4 tag
library working in python, with no luck.
It looks like I must use id3ted and capture stdout for read operations and
result code for writes.
The fix that I contributed to the project has not yet made it's way back to
the Gentoo dist, so I'll have to use my customized version.
'''

# This was the original add_track:
# add_track() {
#     local filename="$1"
#     filename="${filename//\'/''}"
#     records="$(
#         sqlite3 $DB "SELECT * FROM files WHERE filename LIKE '${filename}'";
#     )"
#     if [[ "$records" ]] ; then
#         echo -e "records exist: $records" >&2
#     else
#         echo "adding record to DB $DB for $filename" >&2
#         sqlite3 $DB "INSERT INTO files (filename) VALUES ('${filename}');"
#     fi
#     (( num_files++ ))
# }

# However, this does not populate all of the other tables where track
# information is stored (and pointed to via field id's)

# Track Fields to Look-up
#
# Other Dependent Tables
#   directories
#   devices
#   composers

# Fields to use as look-up criteria (based on exact filename matching and
# access to storage location)
#   "url" int(11) DEFAULT NULL,
#    + table:urls
#    + table:files
#    + table:devices
#    + table:directories

# Field differences that trigger a new record in the table
#   "filesize" int(11) DEFAULT NULL,

# Fields to update in case of conflict
#   @NOTE: overwrite on inconsistency, but leave a log
#   "artist" int(11) DEFAULT NULL,
#   "album" int(11) DEFAULT NULL,
#   "tracknumber" int(11) DEFAULT NULL,
#   "length" int(11) DEFAULT NULL,
#   "genre" int(11) DEFAULT NULL,
#   "year" int(11) DEFAULT NULL,
#   "composer" int(11) DEFAULT NULL,
#
#   "filetype" int(11) DEFAULT NULL,  # should this be mimetype?
#   "createdate" int(11) DEFAULT NULL,
#   "modifydate" int(11) DEFAULT NULL,
#   "albumgain" float DEFAULT NULL,
#   "albumpeakgain" float DEFAULT NULL,
#   "bitrate" int(11) DEFAULT NULL,
#   "samplerate" int(11) DEFAULT NULL,
#   "trackgain" float DEFAULT NULL,
#   "trackpeakgain" float DEFAULT NULL 
#   "bpm" float DEFAULT NULL,
#
# Fields to process
#   # only update if the current record has an older date:
#   "createdate" int(11) DEFAULT NULL,

import sys
import os
import re
import subprocess
import sqlite3
from datetime import datetime
import string

import traceback
from trace import TRACE
import lib.filetags

from hashlib import md5

ME = sys.argv[0]
USAGE = "%s  DB_FILE  MEDIA_FILE  [ MEDIA_FILE ... ]" % ME
VERBOSE = True
VERBOSE = False

create_db_sql_script = "/projects/critter_list/db/create_test_db.sql"

# dependencies:
BLKID = "/sbin/blkid"
EXIFTOOL = "exiftool"

dir_hashcache = {}
dev_hashcache = {}


# Get the difference of all ASCII characters from the set of printable characters
nonprintable = set([chr(i) for i in range(128)]).difference(string.printable)

class Field():

    def __init__(self, name='', value=None, db_type='', property_list=None):
        self.name = name
        self.value = value
        self.db_type = db_type
        self.property_list = property_list

    def to_string(self):
        if self.name == None: name = ''
        else: name = str(self.name)
        if self.value == None: value = ''
        else: value = str(self.value)
        if self.db_type == None: db_type = ''
        else: db_type = str(self.db_type)
        return "name='%s', value='%s', db_type='%s', property_list=%s" % (
               name, value, db_type, str(self.property_list))

    def __str__(self):
        return self.to_string()
        #return "name='%s', value='%s', db_type='%s', property_list=%s" % (
        #       str(n), str(v), str(d), str(self.property_list))

    def set_value(self, value):
        self.value = value

    def get_value(self):
        return self.value

def find_mount_point(path):
    path = os.path.abspath(path)
    while not os.path.ismount(path):
        path = os.path.dirname(path)
    return path

def get_mounted_device(pathname):
    "Get the device mounted at pathname"
    # uses "/proc/mounts"
    device = ""
    pathname = os.path.normcase(pathname) # might be unnecessary here
    try:
        with open("/proc/mounts", "r") as ifp:
            for line in ifp:
                fields = line.rstrip('\n').split()
                # note that line above assumes that
                # no mount points contain whitespace
                if fields[1] == pathname:
                    device = fields[0]
                    if device == "rootfs":
                        TRACE("get_mounted_device(%s) = %s"
                            % (pathname, fields[0]))
                        continue
                    else:
                        TRACE("get_mounted_device(%s) = %s"
                            % (pathname, fields[0]))
                    return device
    except EnvironmentError:
        pass
    if device == "rootfs":
        TRACE("get_mounted_device: path %s matches rootfs, but no other device"
            % pathname)
        return device
    else:
        TRACE("get_mounted_device: no device found matching mountpoint %s"
            % pathname)
    return None # explicit

'''
control_chars = ''.join(map(chr, range(0,32) + range(127,160)))
control_char_re = re.compile('[%s]' % re.escape(control_chars))
def remove_control_chars(s):
    return control_char_re.sub('', s)
'''
def get_device_info(device_node):
    BLKID_COMMAND = [ "/sbin/blkid" ]

    proc = subprocess.Popen(BLKID_COMMAND,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            shell=False)

    # capture output, wait for completion, and dump the output
    stdout, stderr = proc.communicate(input=input)
    returncode = proc.returncode
    if VERBOSE:
        TRACE("subprocess.Popen complete; result = %s"
                        % str(returncode))
    if returncode != 0:
        TRACE("subprocess.Popen %s return code(%d) indicates some type of "
              "failure.  Exiting abnormally."
              % (command, returncode))
        sys.exit(returncode)

    if VERBOSE:
        TRACE(stdout)
        TRACE(stderr)

    # initialze a blank dict
    devinfo = {}
    out = stdout.decode('utf8')
    for line in out.split('\n'):
        line = line.rstrip()
        if len(line) < 1:
            continue
        if not line.startswith(device_node + ": "):
            continue
        for param in line.split(" ")[1:]:
            tag = param.split("=")[0]
            val = param[len(tag)+1:]
            label = "device %s" % tag
            val = val.replace('"','')
            val = val.strip()
            #if len(val) < 1: continue
            #label = remove_control_chars(label)
            label = label.replace(" ", "_")
            devinfo[label] = val
            TRACE("%30s: %s" % (label, val))
    return devinfo

def get_exif(filename):
    EXIFTOOL_COMMAND = [ EXIFTOOL , filename ]

    proc = subprocess.Popen(EXIFTOOL_COMMAND,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            shell=False)

    # capture output, wait for completion, and dump the output
    stdout, stderr = proc.communicate(input=input)
    returncode = proc.returncode
    if VERBOSE:
        TRACE("subprocess.Popen complete; result = %s"
                        % str(returncode))
    if returncode != 0:
        TRACE("subprocess.Popen %s return code(%d) indicates some type of "
              "failure.  Skipping reading."
              % (command, returncode))
        return returncode

    if VERBOSE:
        TRACE(stdout)
        TRACE(stderr)

    # initialze a blank dict
    devinfo = {}
    out = stdout.decode('utf8')
    for line in out.split('\n'):
        line = line.rstrip()
        if len(line) < 1:
            continue
        tag = line.split(":")[0].strip()
        tag = re.sub(r'Date\/', "", tag)
        val = line[len(tag)+1:]
        label = "exif %s" % tag
        val = re.sub(r'[ \t]*:[ \t]*', "", val)
        val = val.strip()
        if len(val) < 1: continue
        label = label.replace(" ", "_")
        if label == "exif_Audio_Bitrate":
            val = val.replace(" kbps", "")
        devinfo[label] = val
        #TRACE("%37s: %s" % (label, val))
    return devinfo

def get_media_file_properties(filename):

    device_info = {}
    fileinfo = {}
    if not os.path.exists(filename):
        TRACE("%s ERROR: filename '%s' does not exist;  Exiting abnormally"
            % (ME, filename))
        sys.exit(3)
    if filename[0] != ".":
        TRACE("filename does not start with an '.' as is unexplainably-done in the Amarok DB.  Prepending the dot... to %s" % filename)
        filename = "." + filename
    fileinfo['filename'] = filename

    dir = os.path.dirname(filename)
    fileinfo['dir'] = dir

    # the directory and device operations are time-consuming; check cache to
    # see if we've already fetched this info
    h = md5(fileinfo['dir'].encode('ascii', 'replace')).hexdigest()
    if h in dir_hashcache.keys() or h in dev_hashcache.keys():
        TRACE("Cache hit on directory '%s', key %s: id = %d"
              % (fileinfo['dir'], str(h), dir_hashcache[h]))
    else:
        TRACE("No cache hit on directory '%s', key %s; dircache has %d entries"
              % (fileinfo['dir'], str(h), len(dir_hashcache.keys())))
        dir_ctime = os.path.getctime(dir)
        fileinfo['dir_ctime'] = dir_ctime
        dir_mtime = os.path.getmtime(dir)
        fileinfo['dir_mtime'] = dir_mtime
        realpath = os.path.realpath(dir)
        fileinfo['realpath'] = realpath
        mountpoint = find_mount_point(realpath)
        fileinfo['mountpoint'] = mountpoint
        device = get_mounted_device(mountpoint)
        fileinfo['device'] = device

        device_info = get_device_info(device)

        stat = os.stat(filename)
        TRACE("stat = %s" % str(stat))
        from inspect import getmembers, \
                            ismethod, \
                            isfunction, \
                            isroutine,\
                            isbuiltin
        for f in getmembers(stat):
            if ismethod(f[1]): continue
            if f[1] is None: continue
            tag = str(f[0])
            val = str(f[1])
            if tag[0:3] != 'st_': continue
            tag = tag.replace('st_', 'file_')
            fileinfo[tag] = val

    id3_tags = lib.filetags.list_tags(filename)
    '''
    for tag in id3_tags.keys():
        val = str(id3_tags[tag]).strip()
        if len(val) < 1: continue
        label = "idv3 tag %s" % tag
        TRACE("%37s: %s" % (label, val))
    '''

    exif_tags = get_exif(filename)

    combined = dict(
                    list(fileinfo.items()) +
                    list(device_info.items()) +
                    list(id3_tags.items()) +
                    list(exif_tags.items())
                   )
    return combined

def get_or_add_db(db_file):
    TRACE("attempting to open database file %s" % db_file)
    if not os.path.exists(db_file):
        TRACE('%s INFO: db %s does not exist; creating it...' % (ME, db_file))
        try:
            conn = sqlite3.connect(db_file)
            conn.row_factory = sqlite3.Row
            cur = conn.cursor()
            if os.path.exists(create_db_sql_script):
                f = open(create_db_sql_script, 'r')
                sql = f.read()
            else:
                TRACE('%s ERROR: db creation sql script %s does not exist.  '
                      'Exiting abnormally\n'
                      % (ME, create_db_sql_script))
                sys.exit(2)
            cur.executescript(sql)
        except:
            TRACE('%s Exception %s: failed to create database.  '
                    'Exiting abnormally\n'
                    % (ME, str(sys.exc_info()))
                 )
        if not os.path.exists(db):
            TRACE('%s ERROR: failed to create database %s.  '
                    'Exiting abnormally\n'
                    % (ME, db_file))
    else:
        conn = sqlite3.connect(db_file)
        cur = conn.cursor()

    return conn

def create_table_if_missing(conn, name):
    cur = conn.cursor()
    sql1 = ("SELECT name FROM sqlite_master WHERE type='table' AND name='%s';"
           % name)
    cur.execute(sql1)
    row = cur.fetchone()
    if row == None:
        TRACE("No table named '%s' found.  Creating..." % name)
        sql2 = ("CREATE TABLE %s (id INTEGER PRIMARY KEY, name VARCHAR(255));"
            % name)
        TRACE("rowcount = %d, sql2=%s" % (cur.rowcount, sql2))
        cur.execute(sql2)
        conn.commit()
        cur.execute(sql1)
        row = cur.fetchone()
        if row == None:
            TRACE("ERROR: attempt to create table '%s' was unsuccessful" % name)
            return None
        TRACE("created table '%s'" % name)
    else:
        if VERBOSE:
            TRACE("table '%s' already exists" % name)
    return True

def dict_from_row(row):
    return dict(zip(row.keys(), row)) 

def add_field_if_missing(conn, table, field):
    cur = conn.cursor()
    # no match found; alter the table to add this record
    #sqlAddcol = ("ALTER TABLE %s ADD COLUMN '%s';" % (table, field))
    sqlAddcol = ("ALTER TABLE {tn} ADD COLUMN '{cn}' {ct}" \
        .format(tn=table, cn=field.name, ct=field.db_type))
    try:
        # TRACE("sqlAddcol(%s): '%s'" % (table, sqlAddcol))
        cur.execute(sqlAddcol)
        conn.commit()
    except:
        pass
        #TRACE("exception %s adding column %s to table %s"
        #      % (str(sys.exc_info), str(field), table))
    return None

def filter_nonprintable(text):
    # Use translate to remove all non-printable characters
    return text.translate({ord(character):'_' for character in nonprintable})

def lookup_value(properties, property_list):
    '''
    @param properties - a dict of all known properties about the media file
    @param tags - a dict containing the db field name as key and a list of
                  properties as the value
    '''
    for prop in property_list:
        try:
            if properties[prop] != None:
                if type(properties[prop]) == str:
                    return filter_nonprintable(properties[prop])
                return properties[prop]
        except KeyError:
            continue
    return None

def keyfield_list_to_value_str(list1):
    _str = ""
    for item in list1:
        if len(_str) > 0: _str += ", "
        if type(item) == Field:
            _str += "%s='%s'" % (item.name, str(item.value))
        else:
            _str += str(item)
    return _str

# TODO: this should specify fields that must
#       1) either be unique
#       2) or must be updated
#       3) or a remove-duplicates loop/query/query-set is performed
def get_or_add_record(conn,
                      properties,
                      description,
                      table,
                      keyfields,
                      update_fields = None):
    '''
    @brief get or create an album record and return the id
    @param keyfields and array of Field objects
    '''

    if len(table) < 1:
        TRACE("ERROR: the table must be specified")

    if VERBOSE:
        for kf in keyfields:
            TRACE("table = %s, keyfield: %s" % (table, kf.to_string()))

    # Create SQL-compatible strings for querying and adding records
    # This requires the following SQL substrings:
    #    - a list of field names (always starting with id)
    #    - a list of matching values
    #    - a list of values to insert into a new record
    paramstr = ""
    matchstr = ""
    valuestr = ""
    for keyfield in keyfields:
        if len(paramstr) > 0: paramstr += ", "
        paramstr += keyfield.name
        value = lookup_value(properties, keyfield.property_list)
        if value == None:
            keyfield.value = ""
        else:
            keyfield.value = value
        TRACE("%s keyfield: %s = '%s'" 
              % (table, keyfield.name, str(keyfield.value)))
        if len(valuestr) > 0: valuestr += ", "
        if len(matchstr) > 0: matchstr += " AND "
        valuestr += "'%s'" % str(keyfield.value).replace("'", "''")
        # add the value as a string, plus escape any single quotes that will
        # screw up our query
        matchstr += ("%s = '%s'"
                     % (keyfield.name, str(keyfield.value).replace("'", "''")))

    '''
    TRACE("paramstr: %s" % paramstr)
    TRACE("valuestr: %s" % valuestr)
    TRACE("matchstr: %s" % matchstr)
    '''

    if len(paramstr) < 1:
        TRACE("ERROR adding %s record: no parameters found in keyfields"
              % description)
        return None

    if len(matchstr) < 1:
        TRACE("ERROR: no values have been provided to match %s" % table)
        return None

    ALLOW_NULL_FIELDS = True
    if not ALLOW_NULL_FIELDS:
        if len(valuestr) < 1:
            TRACE("ERROR: no values have been provided to insert into %s"
                  % table)
            return None

    sql_query = ""
    if len(matchstr) > 1:
        sql_query = ("SELECT id, %s FROM %s WHERE %s;"
                    % (paramstr, table, matchstr))
    TRACE("sql_query = %s" % sql_query)

    conn.row_factory = sqlite3.Row
    cur = conn.cursor()
    row = None
    try:
        cur.execute(sql_query)
        row = cur.fetchone()
    except:
        TRACE("exception %s calling %s" % (str(sys.exc_info), sql_query))
    if row == None:
        TRACE("no values in table '%s' matching '%s' found.  Add record."
              % (table, keyfield_list_to_value_str(keyfields)))
        create_table_if_missing(conn, table)
        for field in keyfields:
            add_field_if_missing(conn, table, field)
        sql_insert = ("INSERT INTO %s (%s) VALUES (%s);"
                    % (table, paramstr, valuestr))
        TRACE("Updating field with sql %s" % sql_insert)
        cur.execute(sql_insert)
        conn.commit()
        cur.execute(sql_query)      # verify creation
        row = cur.fetchone()
        if row == None:
            TRACE("ERROR: failed to add %s record to table '%s'"
                  % (str(keyfields), table))
            return None
        TRACE("added '%s' to table '%s' (id = %d)"
              % (keyfield_list_to_value_str(keyfields), table, row['id']))
    else:
        row2 = cur.fetchone()
        if row2 != None:
            TRACE("WARNING: Multiple matches found to '%s' found in '%s': "
                  % (str(keyfields), table))
            TRACE("  %s" % str(dict_from_row(row)))
            while row2 != None:
                TRACE("  %s" % str(dict_from_row(row2)))
                row = row2
                row2 = cur.fetchone()
            TRACE("Returning last: %s" % str(dict_from_row(row)))

    if update_fields != None:
        TRACE("update fields in table '%s', row %d (%s)"
              % (table, row['id'], str(dict_from_row(row))))

        # insure that all 'update' fields exist in the table and add them to
        # the param string for the query
        for field in update_fields:
            add_field_if_missing(conn, table, field)
            if len(paramstr) > 0: paramstr += ", "
            paramstr += field.name

        sql_query = ("SELECT id, %s FROM %s WHERE id = %d;"
                    % (paramstr, table, row['id']))
        row2 = None
        try:
            cur.execute(sql_query)
            row2 = cur.fetchone()
        except:
            TRACE("exception %s calling %s" % (str(sys.exc_info), sql_query))
        if row2 == None:
            TRACE("ERROR: failed to add %s record to table '%s'"
                  % (str(keyfields), table))
            return row

        row = row2
        match = True
        updatestr = ""
        for field in update_fields:
            value = lookup_value(properties, field.property_list)
            if value == None:
                field.value = ''
            else:
                field.value = value
            TRACE("%s: %s = '%s'" % (table, field.name, str(field.value)))
            if str(field.value) == str(row[field.name]):
                continue
            try:
                i = int(float(field.value))
                if i != 0 and i == int(row[field.name]):
                    continue
            except:
                pass
            TRACE("field %s: '%s' (type %s) does not match current value of "
                    "'%s' (type %s)"
                    % (field.name, field.value, str(type(field.value)),
                        row[field.name], str(type(row[field.name]))))
            match = False
            if len(updatestr) > 0: updatestr += ", "
            # getting an error with rounded brackets in comment values
            # TODO: update string as literal in DB *without* replacing or escaping!
            updatestr += "%s='%s'" % (field.name, field.value)
        if match:
            TRACE("All fields in %s are up-to-date" % table)
        else:
            TRACE("updatestr = %s" % updatestr)
            sqlUpdate = ("UPDATE %s SET %s WHERE id = '%s'"
                % (table, updatestr, row['id']))
            TRACE("update table %s with query:\n   %s" % (table, sqlUpdate))
            #import pdb
            #pdb.set_trace()
            cur.execute(sqlUpdate)
            conn.commit()
            cur.execute(sql_query)      # ...and verify creation
            row = cur.fetchone()
            if row == None:
                TRACE("ERROR: row '%s' in table '%s' is missing after update"
                    % (str(keyfields), table))
                return None
    if row == None:
        TRACE("Failed to add '%s' to table %s"
              % (str(keyfields), table))
    return row

def get_or_add_album(conn, properties):
    '''
    @brief get or create an album record and return the id

    Current design of the albums table:
        CREATE TABLE "albums" (
        id INTEGER PRIMARY KEY ,
        "name" varchar(255) NOT NULL,
        "artist" int(11) DEFAULT NULL,
        "image" int(11) DEFAULT NULL 
        );

    Matching property names:
        properties['TALB']
        properties['exif_Album']
    '''
    cur = conn.cursor()
    description = 'album'
    table = 'albums'
    keyfields = []
    keyfields.append(Field(name="name",
                           property_list = ['TALB', 'exif_Album'],
                           db_type = "varchar(255)"))
    row = get_or_add_record(conn = conn,
                            properties = properties,
                            description = description,
                            table = table,
                            keyfields = keyfields)
    try:
        return row['id']
    except:
        TRACE("Exception %s returning record from %s matching %s"
              % (str(sys.exc_info()), table, str(keyfields)))
        traceback.print_exc()
        return None

def get_or_add_artist(conn, properties):
    '''
    @brief get or create an album record and return the id

    Current design of the albums table:
        CREATE TABLE "artists" (
            id INTEGER PRIMARY KEY ,
            "name" varchar(255) NOT NULL
        );

    Matching property names:
        properties['TPE1']
        properties['exif_Artist']
    '''
    description = 'artist'
    table = 'artists'
    keyfields = []
    f = (Field(name="name",
         property_list = ['TPE1', 'exif_Artist'],
         db_type = "varchar(255)"))
    keyfields.append(f)
    row = get_or_add_record(conn = conn,
                            properties = properties,
                            description = description,
                            table = table,
                            keyfields = keyfields)
    try:
        return row['id']
    except:
        TRACE("Exception %s returning record from %s matching %s"
              % (str(sys.exc_info()), table, str(keyfields)))
        traceback.print_exc()
        return None

def get_or_add_directory(conn, properties):
    '''
    CREATE TABLE "directories" (
        id INTEGER PRIMARY KEY,
        "deviceid" int(11) DEFAULT NULL,
        "dir" varchar(1000) NOT NULL,
        "changedate" int(11) DEFAULT NULL
    );
    '''

    h = md5(properties['dir'].encode('ascii', 'replace')).hexdigest()
    TRACE("dir_hashcache: dir = %s, key = %s" % (properties['dir'], str(h)))
    if h in dir_hashcache.keys():
        TRACE("cache hit on directory '%s': id = %d"
              % (properties['dir'], dir_hashcache[h]))
        return dir_hashcache[h]

    description = 'directory'
    table = 'directories'
    keyfields = []
    keyfields.append(Field(name="dir",
                           property_list = ['realpath',
                                            'dir',
                                            'exif_Directory'],
                           db_type = "VARCHAR(1000)"))
    keyfields.append(Field(name="deviceid",
                           property_list = ['device_id'],
                           db_type = "INT(11)"))
    update_fields = []
    update_fields.append(Field(name="changedate",
                           property_list = ['dir_ctime'],
                           db_type = "INT(11)"))
    row = get_or_add_record(conn = conn,
                            properties = properties,
                            description = description,
                            table = table,
                            keyfields = keyfields,
                            update_fields = update_fields)
    try:
        # update the cache
        dir_hashcache[h] = row['id']
        TRACE("dir_hashcache: key %s: id = %d, size = %d"
              % (str(h), row['id'], len(dir_hashcache.keys())))
        return row['id']
    except:
        TRACE("Exception %s returning record from %s matching %s"
              % (str(sys.exc_info()), table, str(keyfields)))
        traceback.print_exc()
        return None

def get_or_add_device(conn, properties):
    '''
    search_fields = ["device",
                     "device_UUID",
                     "device_PARTUUID",
                     "device_LABEL",
                     "mountpoint"]
    # find criteria (in order): UUID, PART_ID, mountpoint, device
    # update fields: lastmountpoint,  'device_TYPE': 'ext4'

    # Find or add devices in this table:
    CREATE TABLE "devices" (
        id INTEGER PRIMARY KEY,
        "type" varchar(255) DEFAULT NULL,
        "label" varchar(255) DEFAULT NULL,
        "lastmountpoint" varchar(255) DEFAULT NULL,
        "uuid" varchar(255) DEFAULT NULL,
        "servername" varchar(80) DEFAULT NULL,
        "sharename" varchar(240) DEFAULT NULL 
    );

    # Supplementary tables to devices:
    CREATE TABLE "directories" (
        id INTEGER PRIMARY KEY,
        "deviceid" int(11) DEFAULT NULL,
        "dir" varchar(1000) NOT NULL,
        "changedate" int(11) DEFAULT NULL
    );
    CREATE TABLE "urls" (
        id INTEGER PRIMARY KEY,
        "deviceid" int(11) DEFAULT NULL,
        "rpath" varchar(324) NOT NULL,
        "directory" int(11) DEFAULT NULL,
        "uniqueid" varchar(128) DEFAULT NULL
    );
    CREATE TABLE storage_devices (
        id INTEGER PRIMARY KEY,
        name VARCHAR(256),
        label VARCHAR(64),
        uuid VARCHAR(64),
        type VARCHAR(12),
        partuuid VARCHAR(64)
    );

    # Sample data:
    'device_UUID': '6afb1a3d-093d-4155-9246-ee173b21d9cb'
    'device_PARTUUID': '00058b0e-06'
    'device_PTTYPE': 'dos'

    '''

    h = md5(properties['dir'].encode('ascii', 'replace')).hexdigest()
    if h in dev_hashcache.keys():
        TRACE("cache hit on directory '%s': id = %d"
              % (properties['dir'], dev_hashcache[h]))
        return dev_hashcache[h]

    try:
        if not 'mountpoint' in properties.keys() \
        or properties['mountpoint'] == None \
        or len(properties['mountpoint']) < 1:
            TRACE("get_or_add_device ERROR: cannot add a device that does not "
                "have a valid 'mountpoint' property!")
            return 0
    except:
        TRACE("get_or_add_device ERROR: exception '%s' verifying the "
              "'mountpoint' property!" % str(sys.exc_info()))
        return 0

    if not 'device_LABEL' in properties.keys():
        properties['device_LABEL'] = ''
    if not 'device_TYPE' in properties.keys():
        properties['device_TYPE'] = ''

    '''
    TRACE("find device based on the following properties: \n%s"
          % str(properties).replace(",", "\n"))
    '''
    conn.row_factory = sqlite3.Row
    cur = conn.cursor()
    if 'device_UUID' in properties \
    and properties['device_UUID'] != None \
    and len(properties['device_UUID']) > 0:
        sql = ("SELECT id, type, label, lastmountpoint, uuid, servername, "
            "       sharename "
            "FROM devices "
            "WHERE uuid = '%s'"
            % properties['device_UUID'])
        cur.execute(sql)
        match = None
        for row in cur:
            if match == None:
                TRACE("uuid/device_UUID match found: %s" % str(tuple(row)))
                match = row
            else:
                TRACE("WARNING: "
                    "duplicate match for uuid in devices table found: %s"
                    % str(tuple(row)))
                '''
                TODO: determine which one to select
                We either have:
                  - a duplicate record with a correct mountpoint
                    - this may have been previously mounted at a different
                      location; take this record
                  - a duplicate record with an incorrect mountpoint
                    - if the original is also incorrect take this
                  - a duplicate record with a duplicate mountpoint
                    - this is a non-fatal db error, unless there is an
                      excessive number of these duplicate records
                We have a match on the first mountpoint; don't even
                consider using a later row
                '''
                if match['lastmountpoint'] == properties['mountpoint'] \
                and  row['lastmountpoint'] != properties['mountpoint']:
                    # the previous match is clearly the best choice
                    continue
                if row['lastmountpoint'] == match['lastmountpoint']:
                    TRACE("WARNING: multiple device records match on both "
                          "uuid/device_UUID and mountpoint")
                match = row

    if match != None:
        #TRACE("type(match) = %s" % str(type(match)))
        TRACE("match['lastmountpoint'] = %s" % match['lastmountpoint'])
        # check if we need to update the mountpoint
        if match['lastmountpoint'] != properties['mountpoint']:
            sql = ("UPDATE devices SET lastmountpoint = '%s' WHERE id = %d"
                   % (properties['mountpoint'], match['id']))
            cur.execute(sql)
            conn.commit()
        try:
            if len(properties['device_TYPE']) > 0 \
            and match['type'] != properties['device_TYPE']:
                sql = ("UPDATE devices SET type = '%s' WHERE id = %d"
                    % (properties['device_TYPE'], match['id']))
                cur.execute(sql)
                conn.commit()
        except:
            TRACE("Exception %s updating devices.type" % sys.exc_info())
        # update the cache
        dev_hashcache[h] = match['id']
        return match['id']

    if 'device_PARTUUID' in properties \
    and properties['device_PARTUUID'] != None \
    and len(properties['device_PARTUUID']) > 0:
        # try to match the db uuid with the device_PARTUUID
        sql = ("SELECT id, type, label, lastmountpoint, uuid, servername, "
            "       sharename "
            "FROM devices "
            "WHERE uuid = '%s'"
            % properties['device_PARTUUID'])
        cur.execute(sql)
        for row in cur:
            if match == None:
                TRACE("uuid/device_PARTUUID match found: %s" % str(tuple(row)))
                match = row
            else:
                TRACE("WARNING: duplicate match for uuid/device_PARTUUID "
                      "in devices table found: %s"
                      % str(tuple(row)))
                if match['lastmountpoint'] == properties['mountpoint'] \
                and  row['lastmountpoint'] != properties['mountpoint']:
                    # the previous match is clearly the best choice
                    continue
                if row['lastmountpoint'] == match['lastmountpoint']:
                    TRACE("WARNING: multiple device records match on both "
                          "uuid/device_PARTUUID and mountpoint")
                match = row

    if match != None:
        # check if we need to update the mountpoint
        if match['lastmountpoint'] != properties['mountpoint']:
            sql = ("UPDATE devices SET lastmountpoint = '%s' WHERE id = %d"
                   % (properties['mountpoint'], match['id']))
            cur.execute(sql)
            conn.commit()
        try:
            if len(properties['device_TYPE']) > 0 \
            and match['type'] != properties['device_TYPE']:
                sql = ("UPDATE devices SET type = '%s' WHERE id = %d"
                    % (properties['device_TYPE'], match['id']))
                cur.execute(sql)
                conn.commit()
        except:
            TRACE("Exception %s updating devices.type" % sys.exc_info())
        # update the cache
        dev_hashcache[h] = match['id']
        return match['id']

    '''
    No uuid fields match; if we have a non-null uuid field, we should
    create a new record.
    If our uuid fields are all null and/or zero-length, maybe we just
    can't (or haven't taken the time) to read this.  In this case, just
    see if we can match on the mountpoint.
    '''
    sql = ("SELECT * FROM devices WHERE lastmountpoint = '%s'"
        % properties['mountpoint'])
    cur.execute(sql)
    for row in cur:
        if match == None:
            TRACE("mountpoint match found: %s" % str(tuple(row)))
            match = row
        else:
            TRACE("WARNING: duplicate match for mountpoint (%s) "
                    "in devices table found: %s"
                    % (
                        str(tuple(match)),
                        str(tuple(row))
                    ))
            match = row

    if 'device_UUID' in properties \
    and properties['device_UUID'] != None \
    and len(properties['device_UUID']) > 0:
        uuid = properties['device_UUID']
    elif 'device_PARTUUID' in properties \
    and properties['device_PARTUUID'] != None \
    and len(properties['device_PARTUUID']) > 0:
        uuid = properties['device_PARTUUID']
    else:
        uuid = ""

    if match != None:
        # @TODO: update any non-null fields that we have
        if match['uuid'] != uuid and len(uuid) > 0:
            sql = ("UPDATE devices SET uuid = '%s' WHERE id = %d"
                   % (uuid, match['id']))
            cur.execute(sql)
            conn.commit()
        try:
            if len(properties['device_TYPE']) > 0 \
            and match['type'] != properties['device_TYPE']:
                sql = ("UPDATE devices SET type = '%s' WHERE id = %d"
                    % (properties['device_TYPE'], match['id']))
                cur.execute(sql)
                conn.commit()
        except:
            TRACE("Exception %s updating devices.type" % sys.exc_info())
        # update the cache
        dev_hashcache[h] = match['id']
        return match['id']

    if not 'device_LABEL' in properties.keys():
        properties['device_LABEL'] = ''
    # match is still None; add a new record
    sql = ("INSERT INTO devices ("
               "uuid,"
               "type,"
               "label,"
               "lastmountpoint"
           ") VALUES ('" +
               uuid + "', '" +
               properties['device_TYPE'] + "', '" +
               properties['device_LABEL'] + "', '" +
               properties['mountpoint'] + "'" +
           ");")
    TRACE("sql: %s" % sql)
    cur.execute(sql)
    conn.commit()
    sql = ("SELECT * FROM devices "
           "WHERE uuid = '%s' "
           "AND lastmountpoint = '%s'"
        % (uuid, properties['mountpoint']))
    cur.execute(sql)
    for row in cur:
        # update the cache
        dev_hashcache[h] = match['id']
        return row['id']

    return None

def get_or_add_url(conn, properties):
    '''
    @brief get or create an album record and return the id

    Current design of the albums table:
        CREATE TABLE "urls" (
            id INTEGER PRIMARY KEY ,
            "deviceid" int(11) DEFAULT NULL,
            "rpath" varchar(324) NOT NULL,
            "directory" int(11) DEFAULT NULL,
            "uniqueid" varchar(128) DEFAULT NULL 
        );

    Matching property names:
        properties['filename']
    '''
    description = 'url'
    table = 'urls'
    keyfields = []
    keyfields.append(Field(name="rpath",
                           db_type="varchar(1000)",
                           property_list=['filename']))
    keyfields.append(Field(name="directory",
                           db_type="INT(11)",
                           property_list=['directory_id']))
    keyfields.append(Field(name="deviceid",
                           db_type="INT(11)",
                           property_list=['device_id']))
    row = get_or_add_record(conn = conn,
                            properties = properties,
                            description = description,
                            table = table,
                            keyfields = keyfields)
    if row == None:
        TRACE("ERROR: no record returned when getting/adding url")
        return None
    try:
        return row['id']
    except:
        TRACE("Exception %s returning record from %s matching %s"
              % (str(sys.exc_info()), table, str(keyfields)))
        traceback.print_exc()
        return None

def get_or_add_genre(conn, properties):
    '''
    @brief get or create a genre record
    '''
    description = 'Genre'
    table = 'genres'
    keyfields = []
    keyfields.append(Field(name="name",
                           db_type="varchar(255)",
                           property_list=['TCON', 'exif_Genre']))
    row = get_or_add_record(conn = conn,
                             properties = properties,
                             description = description,
                             table = table,
                             keyfields = keyfields)
    try:
        return row['id']
    except:
        TRACE("Exception %s returning record from %s matching %s"
              % (str(sys.exc_info()), table, str(keyfields)))

def get_or_add_year(conn, properties):
    '''
    @brief get or create a year record
    '''
    description = 'Year'
    table = 'years'
    keyfields = []
    f = (Field(name="name",
               db_type="varchar(255)",
               property_list=['YEAR', 'TDRC', 'exif_Year']))
    keyfields.append(f)
    row = get_or_add_record(conn = conn,
                            properties = properties,
                            description = description,
                            table = table,
                            keyfields = keyfields)
    try:
        return row['id']
    except:
        TRACE("Exception %s returning record from %s matching %s"
              % (str(sys.exc_info()), table, str(keyfields)))
        traceback.print_exc()
        return None

def get_or_add_composer(conn, properties):
    '''
    @brief get or create a year record
    '''
    description = 'Composer'
    table = 'composers'
    keyfields = []
    keyfields.append(Field(name="name",
                           db_type="varchar(255)",
                           property_list=['TCOM']))
    row = get_or_add_record(conn = conn,
                            properties = properties,
                            description = description,
                            table = table,
                            keyfields = keyfields)
    try:
        return row['id']
    except:
        TRACE("Exception %s returning record from %s matching %s"
              % (str(sys.exc_info()), table, str(keyfields)))
        traceback.print_exc()
        return None

def get_or_add_track(conn, properties):
    '''
    @brief get or create a track record

    # Key Fields (unique values cause unique records)
    "url" int(11) DEFAULT NULL,
    "artist" int(11) DEFAULT NULL,
    "album" int(11) DEFAULT NULL,
    "composer" int(11) DEFAULT NULL,
    "title" varchar(255) DEFAULT NULL,
    "bitrate" int(11) DEFAULT NULL,
    "length" int(11) DEFAULT NULL,
    "samplerate" int(11) DEFAULT NULL,
    "filesize" int(11) DEFAULT NULL,
    "filetype" int(11) DEFAULT NULL,

    # Non-Key Fields (values are updated with the current data when non-null,
    # non-zero-length-string differences are found
    "genre" int(11) DEFAULT NULL,
    "composer" int(11) DEFAULT NULL,
    "year" int(11) DEFAULT NULL,
    "comment" text COLLATE BINARY,
    "tracknumber" int(11) DEFAULT NULL,
    "discnumber" int(11) DEFAULT NULL,
    "bpm" float DEFAULT NULL,
    "createdate" int(11) DEFAULT NULL,
    "modifydate" int(11) DEFAULT NULL,
    "albumgain" float DEFAULT NULL,
    "albumpeakgain" float DEFAULT NULL,
    "trackgain" float DEFAULT NULL,
    "trackpeakgain" float DEFAULT NULL 
    '''

    # some sanity-checking
    try:
        if int(properties['url_id']) < 1:
            TRACE("ERROR: can't add a track with no valid url property")
            return None
    except:
        TRACE("ERROR: exception %s verifying url_id of track"
              % str(sys.exc_info()))
        return None

    description = 'track'
    table = 'tracks'
    keyfields = []
    keyfields.append(Field(name="url",
                           property_list=['url_id'],
                           db_type="INT(11)"))
    keyfields.append(Field(name="artist",
                           property_list=['artist_id'],
                           db_type="INT(11)"))
    keyfields.append(Field(name="album",
                           property_list=['album_id'],
                           db_type="INT(11)"))
    keyfields.append(Field(name="title",
                           property_list=['TIT2', 'exif_Title'],
                           db_type="VARCHAR(255)"))
    keyfields.append(Field(name="length",
                           property_list=['TLEN'],
                           db_type="INT(11)"))
    keyfields.append(Field(name="bitrate",
                           property_list=[
                               'exif_Lame_Bitrate',
                               'exif_Audio_Bitrate'],
                           db_type="INT(11)"))
    keyfields.append(Field(name="samplerate",
                           property_list=['exif_Sample_Rate'],
                           db_type="INT(11)"))
    keyfields.append(Field(name="filesize",
                           property_list=['file_size'],
                           db_type="INT(11)"))

    update_fields = []
    '''
    "genre" int(11) DEFAULT NULL,
    "composer" int(11) DEFAULT NULL,
    "year" int(11) DEFAULT NULL,
    "comment" text COLLATE BINARY,
    "tracknumber" int(11) DEFAULT NULL,
    "discnumber" int(11) DEFAULT NULL,
    "bpm" float DEFAULT NULL,
    "createdate" int(11) DEFAULT NULL,
    "modifydate" int(11) DEFAULT NULL,
    "albumgain" float DEFAULT NULL,
    "albumpeakgain" float DEFAULT NULL,
    "trackgain" float DEFAULT NULL,
    "trackpeakgain" float DEFAULT NULL 
    '''
    update_fields.append(Field(name="year",
                               property_list = ['year_id'],
                               db_type = "INT(11)"))
    update_fields.append(Field(name="genre",
                               property_list = ['genre_id'],
                               db_type = "INT(11)"))
    update_fields.append(Field(name="composer",
                               property_list=['composer_id'],
                               db_type="INT(11)"))
    update_fields.append(Field(name="tracknumber",
                               property_list=['TRCK', 'exif_Track'],
                               db_type="INT(11)"))
    update_fields.append(Field(name="createdate",
                               property_list=['file_ctime'],
                               db_type="INT(11)"))
    update_fields.append(Field(name="modifydate",
                               property_list=['file_mtime'],
                               db_type="INT(11)"))
    update_fields.append(Field(name="comment",
                               property_list=['COMM', 'exif_Comment'],
                               db_type="INT(11)"))

    row = get_or_add_record(conn = conn,
                            properties = properties,
                            description = description,
                            table = table,
                            keyfields = keyfields,
                            update_fields = update_fields)
    try:
        return row['id']
    except:
        TRACE("Exception %s returning record from %s matching %s"
              % (str(sys.exc_info()), table, str(keyfields)))
        traceback.print_exc()
        return None

def update_database(db, properties):

    conn = get_or_add_db(db)
    if not os.path.exists(db):
        TRACE("ERROR: db %s does not exist; creation failed" % db)
        return 4

    if conn == None:
        TRACE("ERROR: failed to connect to the database %s" % db)
        return 5

    # TODO: check for write access; maybe add, verify, and delete a record

    for prop in sorted(properties.keys()):
        val = str(properties[prop])
        if prop[0:5] == "file_" and prop[-4:] == "time":
            TRACE("%40s: %s (%s)"
                % (prop, val, datetime.fromtimestamp(float(val))))
        else:
            TRACE("%40s: %s" % (prop, val))

    artist_id = get_or_add_artist(conn, properties)
    if artist_id != None:
        properties['artist_id'] = artist_id
    TRACE("get_or_add_artist: artist_id=%s" % str(artist_id))

    album_id = get_or_add_album(conn, properties)
    if album_id != None:
        properties['album_id'] = album_id
    TRACE("get_or_add_album: album_id=%s" % str(album_id))

    device_id = get_or_add_device(conn, properties)
    if device_id != None:
        properties['device_id'] = device_id
    TRACE("get_or_add_device: device_id=%s" % str(device_id))

    directory_id = get_or_add_directory(conn, properties)
    if directory_id != None:
        properties['directory_id'] = directory_id
    TRACE("get_or_add_directory: directory_id=%s" % str(directory_id))

    url_id = get_or_add_url(conn, properties)
    if url_id != None:
        properties['url_id'] = url_id
    TRACE("get_or_add_url: url_id=%s" % str(url_id))

    genre_id = get_or_add_genre(conn, properties)
    if genre_id != None:
        properties['genre_id'] = genre_id
    TRACE("get_or_add_genre: genre_id=%s" % str(genre_id))

    year_id = get_or_add_year(conn, properties)
    if year_id != None:
        properties['year_id'] = year_id
    TRACE("get_or_add_year: year_id=%s" % str(year_id))

    composer_id = get_or_add_composer(conn, properties)
    if composer_id != None:
        properties['composer_id'] = composer_id
    TRACE("get_or_add_composer: composer_id=%s" % str(composer_id))

    track_id = get_or_add_track(conn, properties)
    if track_id != None:
        properties['track_id'] = track_id
    TRACE("get_or_add_track: track_id=%s" % str(track_id))


    # TODO: consider adding playlist as an option
    return 0


############################################
# Main

# TODO: optionally pass deviceID into this function, since looking up the 
#       device is quite slow (~800ms)

if len(sys.argv) <= 2:
    sys.stderr.write("ERROR: this script requires at least 2 arguments\n")
    sys.stderr.write("USAGE:\n   %s\n" % USAGE)
    sys.exit(2)

TRACE('enter %s' % str(sys.argv))

DB = sys.argv[1]
for i in range(2, len(sys.argv)):
    media_file = sys.argv[i]
    if not os.path.exists(media_file):
        TRACE("%s ERROR: media_file '%s' does not exist;  Exiting abnormally"
            % (ME, DB))
        sys.exit(3)
    result = 1
    properties = get_media_file_properties(media_file)
    if properties != None:
        result = update_database(DB, properties)

sys.exit(result)
