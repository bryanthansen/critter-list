#!/bin/bash

# initial 1-liner:
# diff <(sqlite3 critters.db.5 .dump) <(sqlite3 .critters.db.20170124_205749 .dump) | less

#diff <(sqlite3 critters.db.5 .dump) <(sqlite3 .critters.db.20170124_205749 .dump)

diff <(sqlite3 "$1" .dump) <(sqlite3 "$2" .dump) | colordiff
