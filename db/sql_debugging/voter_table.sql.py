#!/usr/bin/python3.4
# Bryant Hansen

import sqlite3

playlistId = 2
voterId = 1

dbConnection = sqlite3.connect('/var/www/localhost/critter_list/wsgi/db/critters.db')
cur = dbConnection.cursor()
sql = (
    "SELECT "
        "tracks.id AS id, "
        "ltrim(urls.rpath, '.') AS filename, "
        "votes.voterId, "
        "votes.vote, "
        "tracks.title AS title, "
        "artists.name AS artist, "
        "albums.name AS album "
    "FROM playlist_tracks "
    "LEFT JOIN tracks ON tracks.id = playlist_tracks.track_num "
    "LEFT JOIN urls ON tracks.url = urls.id "
    "LEFT JOIN votes ON tracks.id = votes.trackId "
    "LEFT JOIN artists ON tracks.artist = artists.id "
    "LEFT JOIN albums ON tracks.album = albums.id "
    "WHERE playlist_tracks.playlist_id = '%d' AND votes.voterId = '%d' "
    "ORDER BY random() "
    "LIMIT 1000 " %
    (voterId, playlistId)
)
print("sql = '%s" % sql)
cur.execute(sql)
col_list = list(list(zip(*cur.description))[0])
print("col_list = '%s'" % str(col_list))

# This transforms a cur.description object from an sql query to a dict.
# The cur.description object is a 2-dimensional array of which we only want the first element of each row
col_dict = dict([(list(list(zip(*cur.description))[0])[n], n) for n in range(len(cur.description))])
print("col_list = '%s'" % str(col_dict))

rows = cur.fetchall()
for row in rows:
    print("row: %s" % str(row))
    for i in range(len(col_list)):
        print("\t%s = %s" % (col_list[i], row[i]))
