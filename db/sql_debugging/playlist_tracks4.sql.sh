#!/bin/sh

playlistid=2

echo "playlistid=${playlistid}" >&2

sqlite3 -header /var/www/localhost/critter_list/wsgi/db/critters.db \
"SELECT (COUNT(CASE WHEN vote = '1' THEN vote END)   - COUNT(CASE WHEN vote = '0' THEN vote END) ) AS score,
COUNT(CASE when vote = '1' THEN vote END) AS yaes,
COUNT(CASE when vote = '0' THEN vote END) AS nahs,
tracks.title AS title,
artists.name AS artist,
albums.name AS album,
ltrim(urls.rpath, '.') AS filename,
IFNULL(MAX(playhistory.playedOn), 'never') AS last_played,
CAST(IFNULL(     strftime('%s','now')  -  strftime('%s', SUBSTR(MAX(playhistory.playedOn), 1, 19)) , 1000000000) AS Integer) AS time_since_played,
tracks.id AS trackId,
strftime('%s','now') AS now,
playlist_tracks.playlist_id AS playlist_id 
FROM playlist_tracks 
JOIN tracks ON tracks.id = playlist_tracks.track_num 
LEFT JOIN votes ON votes.trackId = tracks.id 
LEFT JOIN voters ON votes.voterId = voters.id 
LEFT JOIN urls ON tracks.url = urls.id 
LEFT JOIN artists ON tracks.artist = artists.id 
LEFT JOIN albums ON tracks.album = albums.id 
LEFT JOIN playhistory ON playhistory.trackid = tracks.id 
WHERE playlist_id = '2' 
GROUP BY tracks.id, playhistory.playedOn 
ORDER BY score DESC, yaes 
LIMIT 10 ;"
