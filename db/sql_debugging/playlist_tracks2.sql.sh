#!/bin/sh

playlistid=2

echo "playlistid=${playlistid}" >&2

sqlite3 -header /var/www/localhost/critter_list/wsgi/db/critters.db \
"SELECT
    tracks.title AS title, 
    artists.name AS artist, 
    albums.name AS album, 
    ltrim(urls.rpath, '.') AS filename,
    Hist.last_played AS last_played, 
    Hist.time_since_played AS time_since_played 
    FROM tracks
    LEFT JOIN playlist_tracks ON tracks.id = playlist_tracks.track_num
    LEFT JOIN urls ON tracks.url = urls.id 
    LEFT JOIN artists ON tracks.artist = artists.id 
    LEFT JOIN albums ON tracks.album = albums.id 
    LEFT JOIN (
          SELECT playhistory.trackid AS id, IFNULL(MAX(playhistory.playedon), 'never') AS last_played, 
          IFNULL(strftime('%s','now')  -  strftime('%s', SUBSTR(MAX(playhistory.playedOn), 1, 19)) , 1000000000) AS time_since_played 
          FROM  playlist_tracks 
          LEFT JOIN playhistory ON playhistory.trackid = playlist_tracks.track_num 
          WHERE playlist_tracks.playlist_id = '${playlistid}' 
          GROUP BY playhistory.trackid
    ) AS Hist ON Hist.id = tracks.id 
    WHERE playlist_tracks.playlist_id = '${playlistid}' 
    GROUP BY tracks.id 
    LIMIT 10 ;
"

