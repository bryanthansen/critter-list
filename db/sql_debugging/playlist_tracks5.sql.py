#!/usr/bin/python3.4
# Bryant Hansen

import sqlite3

dbConnection = sqlite3.connect('/var/www/localhost/critter_list/wsgi/db/critters.db')
cur = dbConnection.cursor()

playlistId = 2

sql = (
    "SELECT "
    "(COUNT(CASE WHEN vote = '1' THEN vote END) - COUNT(CASE WHEN vote = '0' THEN vote END)) AS score, "  # 1
    "COUNT(CASE when vote = '1' THEN vote END) AS yaes, " # 2
    "COUNT(CASE when vote = '0' THEN vote END) AS nahs, " # 3
    "tracks.id AS trackId, " # 4
    "tracks.title AS title, " # 5
    "artists.name AS artist, " # 6
    "albums.name AS album, " # 7
    "ltrim(urls.rpath, '.') AS filename, " # 8
    "Hist.last_played AS last_played, " # 9
    "Hist.time_since_played AS time_since_played, " # 10
    "strftime('%%s','now') AS now, " # 11
    "playlist_tracks.playlist_id AS playlist_id " # 12
    "FROM playlist_tracks "
    "LEFT JOIN tracks ON tracks.id = playlist_tracks.track_num "
    "LEFT JOIN votes ON votes.trackId = tracks.id "
    "LEFT JOIN voters ON votes.voterId = voters.id "
    "LEFT JOIN urls ON tracks.url = urls.id "
    "LEFT JOIN artists ON tracks.artist = artists.id "
    "LEFT JOIN albums ON tracks.album = albums.id "
    "LEFT JOIN (SELECT playhistory.trackid AS id, IFNULL(MAX(playhistory.playedon), 'never') AS last_played, "
    "IFNULL(strftime('%%s','now')  -  strftime('%%s', SUBSTR(MAX(playhistory.playedOn), 1, 19)), 1000000000) AS time_since_played "
    "FROM playlist_tracks "
    "LEFT JOIN playhistory ON playhistory.trackid = playlist_tracks.track_num "
    "WHERE playlist_tracks.playlist_id = '%d' "
    "GROUP BY playhistory.trackid ) AS Hist ON Hist.id = tracks.id "
    "WHERE playlist_tracks.playlist_id = '%d' "
    "GROUP BY tracks.id ORDER BY score DESC, yaes DESC "
    "LIMIT 1000"
    % (playlistId, playlistId)
)
print("sql = '%s" % sql)

cur.execute(sql)

rows = cur.fetchall()
for row in rows:
    print("row: %s" % str(row))

for col in range(len(rows[0])):
    print("str(cur.description[%d]) = %s" % (col, str(cur.description[col])))
    print("str(cur.description[%d][0]) = %s" % (col, str(cur.description[col][0])))

#print(cur.description)
desc = cur.description
unzipped = zip(*desc)
mapped = map(lambda x: x[0], desc)

#print("unzipped = %s" % str(unzipped[0]))
#print("mapped = %s" % str(mapped))

print("desc = '%s'" % str(cur.description[0]))

col_list = list(list(zip(*cur.description))[0])
print("col_list = '%s'" % str(col_list))

def list2dict(col_list):
    print(str(list(zip(col_list, range(len(col_list))))))
    return list(zip(col_list, range(len(col_list))))

#col_dict = list2dict(col_list)
#print("\n\ncol_dict = '%s'\n" % str(col_dict))

verbose = False
def desc2dict(description):
    print("desc2dict(description = '%s')" % str(description))
    col_list = list(list(zip(*description))[0])
    print("col_list = '%s'" % str(col_list))
    col_desc_dict = dict(zip(col_list, range(len(col_list))))
    print(col_desc_dict)
    return col_desc_dict

col_dict = desc2dict(cur.description)
print("\ntype(col_dict: %s, col_dict = '%s'"
      % (str(type(col_dict)), str(col_dict)))

print("test values: col_dict['score'](type: %s) = '%s'"
      % (str(type(col_dict['score'])), str(col_dict['score'])))
for row in rows:
    for field in col_dict.keys():
        print("row[col_dict['%s']]: %s" % (field, row[col_dict[field]]))

for field in col_dict.keys():
    print("rows[0][col_dict['%s']]: %s" % (field, rows[0][col_dict[field]]))
for field in col_dict.keys():
    print("rows[len(rows)-1][col_dict['%s']]: %s" % (field, rows[len(rows)-1][col_dict[field]]))

