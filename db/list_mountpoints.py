#!/usr/bin/python3 -u
# Bryant Hansen

import sys
import os
from trace import TRACE

ME = sys.argv[0]
USAGE = "%s" % ME
VERBOSE = True
VERBOSE = False

def list_mountpoints():
    "Get the device mounted at pathname"
    # uses "/proc/mounts"
    device = ""
    mountpoints = []
    try:
        with open("/proc/mounts", "r") as ifp:
            for line in ifp:
                if not line.startswith('/dev/'):
                    continue
                fields = line.rstrip('\n').split()
                # note that line above assumes that
                # no mount points contain whitespace
                mountpoints.append(fields[1])
                TRACE("%s" % fields[1])
    except EnvironmentError:
        pass
    return mountpoints

SUCCESS = 0
if __name__ == '__main__':
    TRACE('enter %s' % str(sys.argv))
    mountpoints = list_mountpoints()
    TRACE("mountpoints = %s" % mountpoints)
    for mountpoint in mountpoints:
        print(mountpoint)
    sys.exit(SUCCESS)

