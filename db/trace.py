# -- coding: utf-8 --
#!/usr/bin/python3 -u

"""
trace.py - general purpose logging wrapper

@author: Bryant Hansen <github1@bryanthansen.net>
@license: GPLv3
"""

import sys
from datetime import datetime
import logging
default_level = logging.DEBUG
from inspect import stack
from collections import deque
import config

try:
    from io import StringIO
except ImportError:
    from cStringIO import StringIO


class Singleton:

    _instance = None
    _debuglog = None
    _logger = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(Singleton, cls).__new__(
                                cls, *args, **kwargs)
        return cls._instance

    def initialize_logger_broken(self):
        logger = logging.getLogger()
        logger.setLevel(logging.DEBUG)
        # create console handler and set level to debug
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        # create formatter
        formatter = logging.Formatter(
            "%(asctime)s: critter-track: %(name)s: %(levelname)s: %(message)s"
        )
        # add formatter to ch
        ch.setFormatter(formatter)
        if logger.handlers:
            return self._logger
        else:
            # add ch to logger
            logger.addHandler(ch)
            TRACE("critter_list logger initialized")
        return logger

    def initialize_logger(self):
        #logging.basicConfig(filename=config.LOGFILE,  level = default_level)
        logging.basicConfig(level = default_level)

    def __init__(self):
        if self._debuglog == None:
            self._debuglog = StringIO()
            self.initialize_logger()

    def trace(self, msg,  level = default_level):
        m = "%s%s" % (datetime.now().strftime("%Y%m%d_%H%M%S.%f")[0:-3], msg)
        self._debuglog.write("%s\n" % m)
        logging.log(level, m)


def TRACE(msg, level = default_level):
    # get the name of the calling function: str(stack()[2][3])
    calling_function = ""
    try:
        calling_function = stack()[1][3]
    except:
        pass
    if type(level) != type(logging.DEBUG):
        Singleton().trace(
            "ERROR: invalid param type passed to TRACE: %s; should be %s; "
            "calling_function: %s"
            % (
                str(type(level)),
                str(type(logging.DEBUG)),
                str(calling_function)
            ),
            logging.ERROR
        )
        level = logging.ERROR
    m = ":%s: %s" % (calling_function, msg)
    Singleton().trace(m, level)


