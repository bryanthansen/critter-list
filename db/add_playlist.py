#!/usr/bin/python3 -u
# Bryant Hansen

import sys
import os
import sqlite3
import critterdb_lib
from trace import TRACE

ME = os.path.basename(sys.argv[0])
USAGE = "%s" % ME
VERBOSE = True

USAGE = "%s  DB_FILE  PLAYLIST_NAME  [  PLAYLIST_DESCRIPTION  ]" % ME


def get_and_validate_args(argv):
    if len(argv) <= 2:
        sys.stderr.write("ERROR: this script requires at least 2 arguments\n")
        sys.stderr.write("USAGE:\n   %s\n" % USAGE)
        sys.exit(2)

    db_file = argv[1]
    if not os.path.exists(db_file):
        TRACE('%s ERROR: database %s does not exist.  Exiting abnormally\n'
                % (ME, db_file))
        sys.exit(3)

    playlist_name = argv[2]
    if len(playlist_name) < 1:
        TRACE('%s ERROR: playlist name cannot be a zero-length string.  '
              'Exiting abnormally\n'
                % (ME, db_file))
        sys.exit(4)

    if len(argv) > 3:
        description = argv[3]
    else:
        description = ""

    return db_file, playlist_name, description


SUCCESS = 0
if __name__ == '__main__':
    TRACE('enter %s' % str(sys.argv))

    db_file, playlist_name, description = get_and_validate_args(sys.argv)
    if db_file == None:
        TRACE("Failed to get db_file from command args")
        sys.exit(2)
    conn = critterdb_lib.connect_to_db(db_file)
    if conn == None:
        TRACE("Failed to open connection to database %s" % str(db_file))
        sys.exit(3)
    playlists = critterdb_lib.add_playlist(conn, playlist_name, description)
    if playlists == None:
        TRACE("Failed to find any playlists in db_file %s"
              % (str(db_file)))
        sys.exit(3)
    for playlist in playlists:
        print("%s\n" % str(playlist))

    sys.exit(SUCCESS)

