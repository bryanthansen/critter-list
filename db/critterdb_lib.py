#!/usr/bin/python3 -u
# Bryant Hansen

import sys
import os
import sqlite3
from trace import TRACE
from datetime import datetime
from list_mountpoints import list_mountpoints

ME = os.path.basename(sys.argv[0])


def connect_to_db(db_file):
    # TRACE("attempting to open database file %s" % db_file)
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except:
        TRACE('Exception %s: failed to create database.  '
              'Exiting abnormally\n'
              % (str(sys.exc_info()))
             )
        sys.exit(5)
    return conn

def get_playlist(conn, playlist_name, description = ""):
    sql = ("SELECT id from playlists "
           "WHERE playlists.name = '%s'" % playlist_name)
    # TRACE("sql = %s" % sql)
    conn.row_factory = sqlite3.Row
    cur = conn.cursor()
    cur.execute(sql)
    rows = cur.fetchall()
    if len(rows) < 1:
        TRACE("no playlist named '%s' found in the db" % playlist_name)
        return None
    if len(rows) > 1:
        TRACE("%d playlists named %s found in the db.  returning last"
              % (len(rows), playlist_name))
        return rows[len(rows) - 1]
    try:
        TRACE("playlist '%s' has id %s" % (playlist_name, str(rows[0]['id'])))
    except:
        TRACE("exception printing playlist info for playlist '%s'"
              % (playlist_name))
    return rows[0]

def add_playlist(conn, playlist_name, description = ""):
    cur = conn.cursor()
    playlist_record = get_playlist(conn, playlist_name, description)
    if playlist_record == None:
        if len(description) < 1:
            description = ("playlist added by %s on %s"
                           % (ME, datetime.now().strftime("%Y%m%d_%H%M%S")))
        sql = ("INSERT INTO playlists (name, description) VALUES ('%s', '%s')"
               % (playlist_name, description))
        TRACE("sql = %s" % str(sql))
        cur.execute(sql)
        conn.commit()
        playlist_record = get_playlist(conn, playlist_name)
    else:
        TRACE("playlist %s already exists in db" % playlist_name)
    return playlist_record

def list_playlists(conn):
    conn.row_factory = sqlite3.Row
    cur = conn.cursor()
    sql = ("SELECT playlists.*, COUNT(playlist_tracks.id) AS num_tracks "
           "from playlists LEFT JOIN playlist_tracks "
           "ON playlists.id == playlist_tracks.playlist_id "
           "GROUP BY playlists.id")
    TRACE("sql = %s" % sql)
    cur.execute(sql)
    rows = cur.fetchall()
    return rows

def add_to_playlist(conn, playlist_id, track_id, url = ""):
    TRACE("adding track id %d to playlist id %d" % (track_id, playlist_id))
    sql = ("INSERT INTO playlist_tracks "
           "(playlist_id, track_num, url) "
           "VALUES (%d, %d, '%s')"
           % (playlist_id, track_id, url)
          )
    TRACE("sql = %s" % sql)
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit();

def remove_from_playlist(conn, playlist_id, track_id, url = ""):
    TRACE("removing track id %d from playlist id %d" % (track_id, playlist_id))
    sql = ("DELETE FROM playlist_tracks "
           "WHERE playlist_id == %d AND track_num == %d"
           % (playlist_id, track_id)
          )
    TRACE("sql = %s" % sql)
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit();

def dump_playlist_tracks(conn, playlist_id):
    sql = ("SELECT playlist_tracks.*, tracks.* "
           "FROM playlist_tracks LEFT JOIN tracks "
           "ON playlist_tracks.track_num == tracks.id "
           "WHERE playlist_tracks.playlist_id = %d" % playlist_id)
    TRACE("sql = %s" % sql)
    conn.row_factory = sqlite3.Row
    cur = conn.cursor()
    cur.execute(sql)
    rows = cur.fetchall()
    return rows

def dump_playlist_tracks2(conn, playlist_id):
    # subquery version
    #  real    0m0.958s
    #  user    0m0.910s
    #  sys     0m0.036s
    sql = (
        "SELECT "
            "tracks.id AS trackid, "
            "tracks.title AS title, "
            "artists.name AS artist, "
            "albums.name AS album, "
            "urls.rpath AS url, "
            "COUNT(playhistory.trackid) AS numplays, "
            "( "
            "    COUNT(CASE WHEN vote = '1' THEN vote END) "
            "  - COUNT(CASE WHEN vote = '0' THEN vote END) "
            ") AS score, "
            "COUNT(CASE when vote = '1' THEN vote END) AS yaes, "
            "COUNT(CASE when vote = '0' THEN vote END) AS nahs "
        "FROM tracks "
        "INNER JOIN playlist_tracks ON tracks.id = playlist_tracks.track_num "
        "LEFT JOIN votes ON votes.trackId = tracks.id "
        "LEFT JOIN urls ON tracks.url = urls.id "
        "LEFT JOIN artists ON tracks.artist = artists.id "
        "LEFT JOIN albums ON tracks.album = albums.id "
        "LEFT JOIN playhistory ON playhistory.trackid = votes.trackId "
        "WHERE playlist_tracks.playlist_id = '%d' "
        "GROUP BY tracks.id, playhistory.trackid "
        "ORDER BY score DESC, yaes "
        "LIMIT 500 "
        ";"
        % playlist_id
    )
    # subquery version
    #  real    0m1.395s
    #  user    0m1.366s
    #  sys     0m0.024s
    sql = (
        "SELECT "
            "tracks.id, "
            "tracks.title AS title, "
            "artists.name AS artist, "
            "albums.name AS album, "
            "urls.rpath AS url, "
            "ltrim(urls.rpath, '.') AS filename, "
            "( "
            "    COUNT(CASE WHEN vote = '1' THEN vote END) "
            "  - COUNT(CASE WHEN vote = '0' THEN vote END) "
            ") AS score, "
            "COUNT(CASE when vote = '1' THEN vote END) AS yaes, "
            "COUNT(CASE when vote = '0' THEN vote END) AS nahs, "
            "Hist.last_played, "
            "Hist.time_since_played "
        "FROM tracks "
        "INNER JOIN playlist_tracks ON tracks.id = playlist_tracks.track_num "
        "LEFT JOIN votes ON votes.trackId = tracks.id "
        "LEFT JOIN voters ON votes.voterId = voters.id "
        "LEFT JOIN urls ON tracks.url = urls.id "
        "LEFT JOIN artists ON tracks.artist = artists.id "
        "LEFT JOIN albums ON tracks.album = albums.id "
        "LEFT JOIN ("
            "SELECT tracks.id, "
            "IFNULL(MAX(playhistory.playedon), 'never') AS last_played, "
            "IFNULL( "
            "    strftime('%%s','now') "
            " -  strftime('%%s', SUBSTR(MAX(playhistory.playedOn), 1, 19)) "
            ", 1000000000"
            ") "
            "AS time_since_played "
            "FROM tracks "
            "INNER JOIN playlist_tracks ON tracks.id = playlist_tracks.track_num "
            "LEFT JOIN playhistory ON playhistory.trackid = tracks.id "
            "WHERE playlist_tracks.playlist_id = '%s' "
            "GROUP BY tracks.id "
        ") AS Hist "
        "ON Hist.id = tracks.id "
        "WHERE playlist_tracks.playlist_id = '%s' "
        "GROUP BY tracks.id "
        "ORDER BY score DESC, yaes DESC "
        "LIMIT 10 "
        ";"
        % (str(playlist_id), str(playlist_id))
    )
    TRACE("sql = %s" % sql)
    conn.row_factory = sqlite3.Row
    cur = conn.cursor()
    cur.execute(sql)
    rows = cur.fetchall()
    return rows

def get_next_N_tracks(conn, playlist_id, num_tracks):
    rowsout = {}
    sql = (
        "SELECT "
            "tracks.id, "
            "tracks.title AS title, "
            "artists.name AS artist, "
            "albums.name AS album, "
            "urls.rpath AS url, "
            "ltrim(urls.rpath, '.') AS filename, "
            "( "
            "    COUNT(CASE WHEN vote = '1' THEN vote END) "
            "  - COUNT(CASE WHEN vote = '0' THEN vote END) "
            ") AS score, "
            "COUNT(CASE when vote = '1' THEN vote END) AS yaes, "
            "COUNT(CASE when vote = '0' THEN vote END) AS nahs, "
            "Hist.last_played, "
            "Hist.time_since_played "
        "FROM tracks "
        "INNER JOIN playlist_tracks ON tracks.id = playlist_tracks.track_num "
        "LEFT JOIN votes ON votes.trackId = tracks.id "
        "LEFT JOIN voters ON votes.voterId = voters.id "
        "LEFT JOIN urls ON tracks.url = urls.id "
        "LEFT JOIN artists ON tracks.artist = artists.id "
        "LEFT JOIN albums ON tracks.album = albums.id "
        "LEFT JOIN ("
            "SELECT tracks.id, "
            "IFNULL(MAX(playhistory.playedon), 'never') AS last_played, "
            "IFNULL( "
            "    strftime('%%s','now') "
            " -  strftime('%%s', SUBSTR(MAX(playhistory.playedOn), 1, 19)) "
            ", 1000000000"
            ") "
            "AS time_since_played "
            "FROM tracks "
            "INNER JOIN playlist_tracks ON tracks.id = playlist_tracks.track_num "
            "LEFT JOIN playhistory ON playhistory.trackid = tracks.id "
            "WHERE playlist_tracks.playlist_id = '%s' "
            "GROUP BY tracks.id "
        ") AS Hist "
        "ON Hist.id = tracks.id "
        "WHERE playlist_tracks.playlist_id = '%s' "
        "GROUP BY tracks.id "
        "ORDER BY score DESC, yaes DESC "
        "LIMIT 1000 "
        ";"
        % (str(playlist_id), str(playlist_id))
    )
    TRACE("sql = %s" % sql)
    conn.row_factory = sqlite3.Row
    cur = conn.cursor()
    cur.execute(sql)
    row = cur.fetchone()
    while row:
        if not os.path.exists(filename):
            TRACE("get_next_N_tracks: file '%s' is not accessible.  "
                  "Not adding to list.\n" % filename)
        else:
            now = str(row[10])
            TRACE(
                "get_next_N_tracks: time_since_played = %s, now = %s\n"
                % (str(time_since_played), now)
            )
            if int(time_since_played) < constants.MIN_REPEAT_TIME:
                TRACE(
                  "get_next_N_tracks: "
                  "file '%s' has been played more recently than %d seconds.  "
                  "Not adding to list.\n" % constants.MIN_REPEAT_TIME)
            else:
                rowsout.append(row)
        row = cur.fetchone()
        if not row:
            break
    return rows

def get_url_record(conn, media_url):
    # check for existence
    matchstr = "/%s" % os.path.basename(media_url)
    sql = ""
    if len(matchstr) > 1:
        sql = ("SELECT * FROM urls WHERE rpath like '%%%s';"
               % (matchstr))
    TRACE("sql = %s" % sql)

    conn.row_factory = sqlite3.Row
    cur = conn.cursor()
    row = None
    try:
        cur.execute(sql)
        rows = cur.fetchall()
    except:
        TRACE("exception %s calling %s" % (str(sys.exc_info), sql))
    if rows == None:
        TRACE("no values in table 'urls' matching '%s' found." % media_url)
        return None
    n = 0
    TRACE("%d rows returned matching %s" % (len(rows), matchstr))
    hit = None
    for row in rows:
        TRACE("row #%d: (type %s)" % (n, str(type(row))))
        for key in row.keys():
            TRACE("    %s = %s" % (str(key), str(row[key])))
        mountpoints = list_mountpoints()
        TRACE("list_mountpoints returned %s" % str(mountpoints))
        for mountpoint in mountpoints:
            # mountpoint = "/"
            db_rpath = row['rpath']
            u = "%s%s%s" % (mountpoint, os.sep, db_rpath)
            # is it in the root?
            if os.path.exists(u):
                # we have a winner!
                TRACE("path exists: %s" % (u))
                hit = row
                break
            else:
                TRACE("path does not exist: %s" % (u))
            n = n + 1
        if hit != None: break

    if hit == None:
        TRACE('%s ERROR: no db record returned for file %s.  '
              'Exiting abnormally\n'
              % (ME, media_url))
        sys.exit(6)

    return hit

def get_track(conn, url_id):
    sql = ("SELECT * FROM tracks WHERE url == %d"
                % (url_id))
    conn.row_factory = sqlite3.Row
    cur = conn.cursor()
    row = None
    try:
        TRACE("sql = %s" % sql)
        cur.execute(sql)
        rows = cur.fetchall()
    except:
        TRACE("exception %s calling %s" % (str(sys.exc_info), sql))

    if len(rows) < 1:
        TRACE("no track matching URL ID %d found in the db" % url_id)
        return None
    if len(rows) > 1:
        TRACE("multiple tracks matching URL ID %d found in the db.  "
              "returning last")
        return rows[len(rows) - 1]
    TRACE("get_track returning record 0, type %s" % (str(type(rows[0]))))
    return rows[0]
