# -- coding: utf-8 --
#!/usr/bin/python3 -u

"""
config.py - global configuration information

@author: Bryant Hansen <github1@bryanthansen.net>
@license: GPLv3
"""

from datetime import datetime
LOGDIR = './'
LOGFILE = "%s/%s.log" % (LOGDIR, datetime.now().strftime("%Y%m%d_%H%M%S"))
