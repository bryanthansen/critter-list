PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE "admin" (
  "component" varchar(255) DEFAULT NULL,   "version" int(11) DEFAULT NULL
);
CREATE TABLE "albums" (
  id INTEGER PRIMARY KEY ,   "name" varchar(255) NOT NULL,   "artist" int(11) DEFAULT NULL,   "image" int(11) DEFAULT NULL 
);
CREATE TABLE "amazon" (
  "asin" varchar(20) DEFAULT NULL,   "locale" varchar(2) DEFAULT NULL,   "filename" varchar(33) DEFAULT NULL,   "refetchdate" int(11) DEFAULT NULL
);
CREATE TABLE "artists" (
  id INTEGER PRIMARY KEY ,   "name" varchar(255) NOT NULL 
);
CREATE TABLE "bookmark_groups" (
  id INTEGER PRIMARY KEY ,   "parent_id" int(11) DEFAULT NULL,   "name" varchar(255) DEFAULT NULL,   "description" varchar(255) DEFAULT NULL,   "custom" varchar(255) DEFAULT NULL 
);
CREATE TABLE "bookmarks" (
  id INTEGER PRIMARY KEY ,   "parent_id" int(11) DEFAULT NULL,   "name" varchar(255) DEFAULT NULL,   "url" varchar(1000) DEFAULT NULL,   "description" varchar(1000) DEFAULT NULL,   "custom" varchar(255) DEFAULT NULL 
);
CREATE TABLE "composers" (
  id INTEGER PRIMARY KEY ,   "name" varchar(255) NOT NULL 
);
CREATE TABLE "devices" (
  id INTEGER PRIMARY KEY ,   "type" varchar(255) DEFAULT NULL,   "label" varchar(255) DEFAULT NULL,   "lastmountpoint" varchar(255) DEFAULT NULL,   "uuid" varchar(255) DEFAULT NULL,   "servername" varchar(80) DEFAULT NULL,   "sharename" varchar(240) DEFAULT NULL 
);
CREATE TABLE "directories" (
  id INTEGER PRIMARY KEY ,   "deviceid" int(11) DEFAULT NULL,   "dir" varchar(1000) NOT NULL,   "changedate" int(11) DEFAULT NULL 
);
CREATE TABLE "genres" (
  id INTEGER PRIMARY KEY ,   "name" varchar(255) NOT NULL 
);
CREATE TABLE "images" (
  id INTEGER PRIMARY KEY ,   "path" varchar(255) NOT NULL 
);
CREATE TABLE "labels" (
  id INTEGER PRIMARY KEY ,   "label" varchar(255) DEFAULT NULL 
);
CREATE TABLE "lyrics" (
  id INTEGER PRIMARY KEY ,   "url" varchar(324) DEFAULT NULL,   "lyrics" text COLLATE BINARY 
);
CREATE TABLE "playlist_groups" (
  id INTEGER PRIMARY KEY ,   "parent_id" int(11) DEFAULT NULL,   "name" varchar(255) DEFAULT NULL,   "description" varchar(255) DEFAULT NULL 
);
CREATE TABLE "playlist_tracks" (
  id INTEGER PRIMARY KEY ,   "playlist_id" int(11) DEFAULT NULL,   "track_num" int(11) DEFAULT NULL,   "url" varchar(1000) DEFAULT NULL,   "title" varchar(255) DEFAULT NULL,   "album" varchar(255) DEFAULT NULL,   "artist" varchar(255) DEFAULT NULL,   "length" int(11) DEFAULT NULL,   "uniqueid" varchar(128) DEFAULT NULL 
);
CREATE TABLE "playlists" (
  id INTEGER PRIMARY KEY ,   "parent_id" int(11) DEFAULT NULL,   "name" varchar(255) DEFAULT NULL,   "description" varchar(255) DEFAULT NULL,   "urlid" varchar(1000) DEFAULT NULL 
);
CREATE TABLE "podcastchannels" (
  id INTEGER PRIMARY KEY ,   "url" text COLLATE BINARY,   "title" text COLLATE BINARY,   "weblink" text COLLATE BINARY,   "image" text COLLATE BINARY,   "description" text COLLATE BINARY,   "copyright" varchar(255) DEFAULT NULL,   "directory" varchar(255) DEFAULT NULL,   "labels" varchar(255) DEFAULT NULL,   "subscribedate" varchar(255) DEFAULT NULL,   "autoscan" tinyint(1) DEFAULT NULL,   "fetchtype" int(11) DEFAULT NULL,   "haspurge" tinyint(1) DEFAULT NULL,   "purgecount" int(11) DEFAULT NULL,   "writetags" tinyint(1) DEFAULT NULL,   "filenamelayout" varchar(1024) DEFAULT NULL 
);
CREATE TABLE "podcastepisodes" (
  id INTEGER PRIMARY KEY ,   "url" text COLLATE BINARY,   "channel" int(11) DEFAULT NULL,   "localurl" text COLLATE BINARY,   "guid" varchar(1000) DEFAULT NULL,   "title" text COLLATE BINARY,   "subtitle" text COLLATE BINARY,   "sequenctext"  DEFAULT NULL,   "description" text COLLATE BINARY,   "mimetype" varchar(255) DEFAULT NULL,   "pubdate" varchar(255) DEFAULT NULL,   "duration" int(11) DEFAULT NULL,   "filesize" int(11) DEFAULT NULL,   "isnew" tinyint(1) DEFAULT NULL 
);
CREATE TABLE "statistics" (
  id INTEGER PRIMARY KEY ,   "url" int(11) NOT NULL,   "createdate" int(11) DEFAULT NULL,   "accessdate" int(11) DEFAULT NULL,   "score" float DEFAULT NULL,   "rating" int(11) NOT NULL DEFAULT '0',   "playcount" int(11) NOT NULL DEFAULT '0',   "deleted" tinyint(1) NOT NULL DEFAULT '0' 
);
CREATE TABLE "statistics_permanent" (
  "url" varchar(324) NOT NULL,   "firstplayed" datetime DEFAULT NULL,   "lastplayed" datetime DEFAULT NULL,   "score" float DEFAULT NULL,   "rating" int(11) DEFAULT '0',   "playcount" int(11) DEFAULT NULL
);
CREATE TABLE "statistics_tag" (
  "name" varchar(108) DEFAULT NULL,   "artist" varchar(108) DEFAULT NULL,   "album" varchar(108) DEFAULT NULL,   "firstplayed" datetime DEFAULT NULL,   "lastplayed" datetime DEFAULT NULL,   "score" float DEFAULT NULL,   "rating" int(11) DEFAULT '0',   "playcount" int(11) DEFAULT NULL
);
CREATE TABLE "tracks" (
  id INTEGER PRIMARY KEY ,   "url" int(11) DEFAULT NULL,   "artist" int(11) DEFAULT NULL,   "album" int(11) DEFAULT NULL,   "genre" int(11) DEFAULT NULL,   "composer" int(11) DEFAULT NULL,   "year" int(11) DEFAULT NULL,   "title" varchar(255) DEFAULT NULL,   "comment" text COLLATE BINARY,   "tracknumber" int(11) DEFAULT NULL,   "discnumber" int(11) DEFAULT NULL,   "bitrate" int(11) DEFAULT NULL,   "length" int(11) DEFAULT NULL,   "samplerate" int(11) DEFAULT NULL,   "filesize" int(11) DEFAULT NULL,   "filetype" int(11) DEFAULT NULL,   "bpm" float DEFAULT NULL,   "createdate" int(11) DEFAULT NULL,   "modifydate" int(11) DEFAULT NULL,   "albumgain" float DEFAULT NULL,   "albumpeakgain" float DEFAULT NULL,   "trackgain" float DEFAULT NULL,   "trackpeakgain" float DEFAULT NULL 
);
CREATE TABLE "urls" (
  id INTEGER PRIMARY KEY ,   "deviceid" int(11) DEFAULT NULL,   "rpath" varchar(324) NOT NULL,   "directory" int(11) DEFAULT NULL,   "uniqueid" varchar(128) DEFAULT NULL 
);
CREATE TABLE "urls_labels" (
  "url" int(11) DEFAULT NULL,   "label" int(11) DEFAULT NULL
);
CREATE TABLE "years" (
  id INTEGER PRIMARY KEY ,   "name" varchar(255) NOT NULL 
);
CREATE TABLE storage_devices (id INTEGER PRIMARY KEY, name VARCHAR(256), label VARCHAR(64), uuid VARCHAR(64), type VARCHAR(12), partuuid VARCHAR(64));
CREATE TABLE files (id INTEGER PRIMARY KEY, storage_device INTEGER, path VARCHAR(256), filename VARCHAR(1024));
CREATE TABLE playlist (id INTEGER PRIMARY KEY, trackId INTEGER, play_order INTEGER, yaes INTEGER, nahs INTEGER);
CREATE TABLE voters (id INTEGER PRIMARY KEY, name VARCHAR(256), status INTEGER);
CREATE TABLE votes (id INTEGER PRIMARY KEY, trackId INTEGER, voterId INTEGER, vote INTEGER, votedon DATETIME);
CREATE TABLE sessions (id INTEGER PRIMARY KEY, name VARCHAR(256), voterId INTEGER, ip VARCHAR(20), mac VARCHAR(30), starttime DATETIME, endtime DATETIME, status INTEGER);
CREATE TABLE playhistory (id INTEGER PRIMARY KEY, trackId INTEGER, playedon DATETIME, result INTEGER, comment VARCHAR(1024));
CREATE TABLE playresults (id INTEGER PRIMARY KEY, description VARCHAR(256));
CREATE INDEX "statistics_tag_stats_tag_name_artist_album" ON "statistics_tag" ("name","artist","album");
CREATE INDEX "urls_labels_urlslabels_url" ON "urls_labels" ("url");
CREATE INDEX "urls_labels_urlslabels_label" ON "urls_labels" ("label");
CREATE INDEX "genres_genres_name" ON "genres" ("name");
CREATE INDEX "devices_devices_uuid" ON "devices" ("uuid");
CREATE INDEX "devices_devices_type" ON "devices" ("type");
CREATE INDEX "devices_devices_rshare" ON "devices" ("servername","sharename");
CREATE INDEX "podcastepisodes_url_podepisode" ON "podcastepisodes" ("url");
CREATE INDEX "podcastepisodes_localurl_podepisode" ON "podcastepisodes" ("localurl");
CREATE INDEX "amazon_amazon_date" ON "amazon" ("refetchdate");
CREATE INDEX "albums_albums_name_artist" ON "albums" ("name","artist");
CREATE INDEX "albums_albums_name" ON "albums" ("name");
CREATE INDEX "albums_albums_artist" ON "albums" ("artist");
CREATE INDEX "albums_albums_image" ON "albums" ("image");
CREATE INDEX "statistics_permanent_stats_perm_url" ON "statistics_permanent" ("url");
CREATE INDEX "images_images_name" ON "images" ("path");
CREATE INDEX "podcastchannels_url_podchannel" ON "podcastchannels" ("url");
CREATE INDEX "composers_composers_name" ON "composers" ("name");
CREATE INDEX "urls_uniqueid" ON "urls" ("uniqueid");
CREATE INDEX "urls_urls_id_rpath" ON "urls" ("deviceid","rpath");
CREATE INDEX "urls_urls_uniqueid" ON "urls" ("uniqueid");
CREATE INDEX "urls_urls_directory" ON "urls" ("directory");
CREATE INDEX "playlists_parent_playlist" ON "playlists" ("parent_id");
CREATE INDEX "labels_labels_label" ON "labels" ("label");
CREATE INDEX "statistics_statistics_url" ON "statistics" ("url");
CREATE INDEX "statistics_statistics_createdate" ON "statistics" ("createdate");
CREATE INDEX "statistics_statistics_accessdate" ON "statistics" ("accessdate");
CREATE INDEX "statistics_statistics_score" ON "statistics" ("score");
CREATE INDEX "statistics_statistics_rating" ON "statistics" ("rating");
CREATE INDEX "statistics_statistics_playcount" ON "statistics" ("playcount");
CREATE INDEX "artists_artists_name" ON "artists" ("name");
CREATE INDEX "years_years_name" ON "years" ("name");
CREATE INDEX "tracks_tracks_url" ON "tracks" ("url");
CREATE INDEX "tracks_tracks_id" ON "tracks" ("id");
CREATE INDEX "tracks_tracks_artist" ON "tracks" ("artist");
CREATE INDEX "tracks_tracks_album" ON "tracks" ("album");
CREATE INDEX "tracks_tracks_genre" ON "tracks" ("genre");
CREATE INDEX "tracks_tracks_composer" ON "tracks" ("composer");
CREATE INDEX "tracks_tracks_year" ON "tracks" ("year");
CREATE INDEX "tracks_tracks_title" ON "tracks" ("title");
CREATE INDEX "tracks_tracks_discnumber" ON "tracks" ("discnumber");
CREATE INDEX "tracks_tracks_createdate" ON "tracks" ("createdate");
CREATE INDEX "tracks_tracks_length" ON "tracks" ("length");
CREATE INDEX "tracks_tracks_bitrate" ON "tracks" ("bitrate");
CREATE INDEX "tracks_tracks_filesize" ON "tracks" ("filesize");
CREATE INDEX "lyrics_lyrics_url" ON "lyrics" ("url");
CREATE INDEX "directories_directories_deviceid" ON "directories" ("deviceid");
CREATE INDEX "playlist_tracks_parent_playlist_tracks" ON "playlist_tracks" ("playlist_id");
CREATE INDEX "playlist_tracks_playlist_tracks_uniqueid" ON "playlist_tracks" ("uniqueid");
CREATE INDEX "playlist_groups_parent_podchannel" ON "playlist_groups" ("parent_id");
COMMIT;
