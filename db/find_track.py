#!/usr/bin/python3 -u
# Bryant Hansen

import sys
import os
import sqlite3

import critterdb_lib
from trace import TRACE

ME = sys.argv[0]
USAGE = "%s  DB_FILE  TRACK_URL" % ME
VERBOSE = True
VERBOSE = False

# TODO: make a list of mountpoints and check the media_url relative to the root
#       of every mountpoint

def get_and_validate_args(argv):
    if len(argv) <= 2:
        sys.stderr.write("ERROR: this script requires at least 2 arguments\n")
        sys.stderr.write("USAGE:\n   %s\n" % USAGE)
        sys.exit(2)

    db_file = argv[1]
    media_url = argv[2]

    if not os.path.exists(db_file):
        TRACE('%s ERROR: database %s does not exist.  Exiting abnormally\n'
                % (ME, db_file))
        sys.exit(3)

    if not os.path.exists(media_url):
        TRACE('%s ERROR: media file %s does not exist.  Exiting abnormally\n'
                % (ME, db_file))
        sys.exit(4)

    return db_file, media_url

def connect_to_db(db_file):
    TRACE("attempting to open database file %s" % db_file)
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except:
        TRACE('%s Exception %s: failed to create database.  '
                'Exiting abnormally\n'
                % (ME, str(sys.exc_info()))
                )
        sys.exit(5)
    return conn

SUCCESS = 0
if __name__ == '__main__':

    TRACE('enter %s' % str(sys.argv))

    db_file, media_url = get_and_validate_args(sys.argv)
    conn = connect_to_db(db_file)
    record = critterdb_lib.get_url_record(conn, media_url)

    #TRACE("record type %s returned for file %s in db %s: %s"
    #      % (str(type(record)), media_url, db_file, str(record)))

    print("%d\n" % record['id'])

    sys.exit(SUCCESS)

