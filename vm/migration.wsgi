# -- coding: utf-8 --
#!/usr/bin/python3.3 -u

"""
critter.wsgi - a dynamic voting system

@author: Bryant Hansen
@license: GPLv3
"""

from __future__ import unicode_literals

import sqlite3
import sys
import os
import time
from datetime import datetime
import re
from random import randint, choice
import codecs
#from io import StringIO

try:
     from io import StringIO
except ImportError:
     from cStringIO import StringIO

import subprocess
from cgi import parse_qs, escape
from traceback import format_exc, print_exc
from pprint import pformat

DIR='/var/www/localhost/htdocs/mod_wsgi'

default_url = "player.wsgi"
debuglog = StringIO()

default_sessionId = 123456789
default_voterId = 0
default_playlistId = 1


#class SessionContext:
#    sessionId
#    voterId
#    voterName
#    playlistId
#    envdict
#    pass

def TRACE(msg):
    sys.stderr.write("TRACE: msg is type " + str(type(msg)))
    if isinstance(msg, str):
        msg = unicode(msg)
    debuglog.write("%s: %s\n" % (datetime.now().strftime("%H:%M:%S.%f")[0:-3], msg))
    #sys.stderr.write(msg)

def dump_todo():
    text = str(
        "<div id='TODO'>\n"
        "<h3>TODO</h3>\n"
        "<ul>\n"
        "<li>Finish enabling vote delete\n"
        "<li>Efficient live updates of the tally - do only partial updates\n"
        "<li>Add session functionality; replace ?voterId=NNN url with ?sessionId=NNN\n"
        "<li>Expiration features.  Remove votes from tally that are associated with expired sessions\n"
        "<li>Safe, secure deployment system in network appliance (make DVD & USB stick versions)\n"
        "<li>Amarok integration - automate conversion and finish linking queries to this\n"
        "<li>Turn on and off debug messages from the command line\n"
        "<li>Multiple playlists, and a selector for which controls the local audio system\n"
        "<li>mplayer &amp; mpd controls\n"
        "<li>Abstraction layer for supporting mod_python and mod_wsgi simultaneously\n"
        "<li>Get ID3 tags - parse ID3 tags and display the info instead of filename\n"
        "<li>Create separate Voting and Viewing pages\n"
        "<li>Login screen\n"
        "<li>Separate screens: Voter Client, Admin, Dev, ???\n"
        "<li>Tab bar on top to access various views\n"
        "<li>Test on Mobile phones\n"
        "<li>Clear votes\n"
        "</ul>\n"
        "</div>\n"
    )
    return text

def get_voter(cur, voterId):
    cur.execute(
        "SELECT id, name FROM voters "
        "WHERE id = '" + str(voterId)  + "'"
    )
    rows = cur.fetchall()
    if len(rows) >= 1:
        return rows[0][1]
    else:
        return ""

def add_voter(con, votername):
    TRACE("adding voter " + votername + " to db")
    cur = con.cursor()
    cur.execute(
        "INSERT INTO voters (name) VALUES ('" + votername + "');"
    )
    con.commit()












def application(environ, start_response):

    TRACE("enter application")

    status = '200 OK' 
    output = dump_todo()
    response_headers = [('Content-type', 'text/plain'),
                        ('Content-Length', str(len(output)))]
    start_response(status, response_headers)

    return [output]

