#!/bin/bash
# BHa

vmname=gentoo_vm_baseline_20120927b_20140213

#VBoxManage modifyvm "VM name" --natpf1 "guestssh,tcp,127.0.0.1,2222,,22"

host_port="10022"
guest_port="22"
VBoxManage modifyvm $vmname --natpf1 delete "guestssh"
VBoxManage modifyvm $vmname --natpf1 "guestssh,tcp,0.0.0.0,${host_port},,${guest_port}"

host_port="10080"
guest_port="80"
VBoxManage modifyvm $vmname --natpf1 delete "guesthttp"
VBoxManage modifyvm $vmname --natpf1 "guesthttp,tcp,0.0.0.0,${host_port},,${guest_port}"

host_port="10443"
guest_port="443"
VBoxManage modifyvm $vmname --natpf1 delete "guesthttps"
VBoxManage modifyvm $vmname --natpf1 "guesthttps,tcp,0.0.0.0,${host_port},,${guest_port}"

#VBoxManage getextradata $vmname enumerate
