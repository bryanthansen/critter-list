#!/bin/bash
# Bryant Hansen

DIR=/data/os/virtualbox/gentoo_vm_baseline_20120927b_20140213
LOGFILE=/data/os/virtualbox/gentoo_vm_baseline_20120927b_20140213_$(date "+%Y%m%d_%H%M%S").log
VMNAME=gentoo_vm_baseline_20120927b_20140213

touch $LOGFILE
nohup VBoxHeadless -s $VMNAME >> $LOGFILE 2>&1 & sleep 1
tail --follow $LOGFILE $DIR/Logs/VBox.log'
