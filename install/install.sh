#!/bin/bash
# Bryant Hansen

# TODO Make ebuild

FILENAME_PREFORMATTER_REGEX="s/\#.*//;/^[ \t]*$/d;s/^[ \t]*//;s/[ \t]*$//"

echo "Current directory should be project root.  Current directory is '${PWD}'" >&2

unset src
LINKS_FILE=install/LINKS.lst
echo "# LINKS_FILE: $LINKS_FILE" >&2
cat $LINKS_FILE \
| sed "$FILENAME_PREFORMATTER_REGEX" \
| while read line ; do
  #echo "# $line" >&2
  if [[ ! "${line//\[*\]/}" ]] ; then
      src="${line#*\[}"
      src="${src%\]*}"
      echo "## (src=$src) " >&2
  else
      if [[ ! "$src" ]] ; then
          echo "ERROR: src not set!" >&2
          break
      fi
      if [[ ! -f "$src" ]] ; then
          echo "ERROR: src '$src' does not exist!" >&2
          break
      fi
      dst="${line#*\[}"
      dst="${dst%\]*}"
      echo "ln -s '${src}' '${dst}'"
  fi
done

unset dst
MANIFEST_FILE=install/MANIFEST.lst
echo "# MANIFEST_FILE $MANIFEST_FILE" >&2
cat $MANIFEST_FILE \
| sed "$FILENAME_PREFORMATTER_REGEX" \
| while read line ; do
  # echo "# $line" >&2
  if [[ ! "${line//\[*\]/}" ]] ; then
      dst="${line#*\[}"
      dst="${dst%\]*}"
      echo "## (dst=$dst) " >&2
  else
      if [[ ! "$dst" ]] ; then
          echo "# ERROR: dst not set!" >&2
          break
      fi
      dstdir="$(dirname "$dst")"
      if [[ ! -d "$dstdir" ]] ; then
          echo "mkdir -p '$dstdir'"
      fi
      src="${line#*\[}"
      src="${src%\]*}"
      if [[ ! -f "$src" ]] ; then
          echo "# ERROR: src '$src' does not exist!" >&2
          break
      fi
      b="$(basename "$src")"
      if [[ -f "${dst}/${b}" ]] ; then
          echo "# ${dst}/${b} already exists" >&2
      else
          echo "cp -a '${src}' '${dst}'"
      fi
  fi
done
