#!/bin/bash
# Bryant Hansen

FILENAME_PREFORMATTER_REGEX="s/\#.*//;/^[ \t]*$/d;s/^[ \t]*//;s/[ \t]*$//"

echo "Current directory should be project root.  Current directory is '${PWD}'" >&2

unset src
LINKS_FILE=install/LINKS.lst
echo "# LINKS_FILE: $LINKS_FILE" >&2
cat $LINKS_FILE \
| sed "$FILENAME_PREFORMATTER_REGEX" \
| while read line ; do
  if [[ "${line//\[*\]/}" ]] ; then
      dst="${line#*\[}"
      dst="${dst%\]*}"
      if [[ -L "$dst" ]] ; then
          echo "rm '${dst}'"
      fi
  fi
done

unset dst
MANIFEST_FILE=install/MANIFEST.lst
echo "# MANIFEST_FILE $MANIFEST_FILE" >&2
cat $MANIFEST_FILE \
| sed "$FILENAME_PREFORMATTER_REGEX" \
| while read line ; do
  if [[ ! "${line//\[*\]/}" ]] ; then
      dst="${line#*\[}"
      dst="${dst%\]*}"
  else
      if [[ ! "$dst" ]] ; then
          echo "# ERROR: dst not set!" >&2
          break
      fi
      dstdir="$(dirname "$dst")"
      if [[ ! -d "$dstdir" ]] ; then
          echo "mkdir -p '$dstdir'"
      fi
      src="${line#*\[}"
      src="${src%\]*}"
      if [[ ! -f "$src" ]] ; then
          echo "# ERROR: src '$src' does not exist!" >&2
          break
      fi
      b="$(basename "$src")"
      dstfile="${dst}/${b}"
      if [[ ! -f "${dstfile}" ]] ; then
          echo "# ${dstfile} does not exist" >&2
          continue
      fi
      if ! diff "$src" "$dstfile" ; then
          echo "# The source '${src}' and the destination '${dstfile}' differ.  Do not delete." >&2
          continue
      fi
      echo "rm '$dstfile'"
  fi
done
