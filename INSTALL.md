# Make options:

make install - install the application

make uninstall - uninstall the application

make test - run tests

make install-template - install a new template; requires an argument

make save-template - save the template; requires an argument

make list-templates
