'''

@author Bryant Hansen <sourcecode@bryanthansen.net>

@brief a set of queries used in various parts of the app

These are stored here and organized since it represents a lot of code,
in a unique language, which was previously intermixed with the python

'''
import os
import sqlite3
from time import strftime, gmtime

from critter_list.lib.trace import TRACE
import critter_list.constants as constants


def desc2dict(description):
  """
  convert the cursor().description data structure to a dict that performs
  a reverse lookup of the field name to field index
  In this way, if the order of fields in the query changes, the code
  is auto-adjusted
  It makes the code easier to read and possibly more-efficient
  TODO: performance testing in large datasets
  compare to row_factory
  """
  #print("desc2dict(description = '%s')" % str(description))
  col_list = list(list(zip(*description))[0])
  #print("col_list = '%s'" % str(col_list))
  col_desc_dict = dict(zip(col_list, range(len(col_list))))
  #print(col_desc_dict)
  return col_desc_dict

def lookup_votername(cur, voterId):
  cur.execute(
    "SELECT id, name FROM voters "
    "WHERE id = '" + str(voterId)  + "'"
  )
  rows = cur.fetchall()
  if len(rows) >= 1:
    return rows[0]['name']
  else:
    TRACE("lookup_votername: no voter matching id %d" % voterId)
    return ""

def lookup_voterId(cur, voterName):
  cur.execute(
    "SELECT id, name FROM voters "
    "WHERE name = '" + voterName + "'"
  )
  rows = cur.fetchall()
  TRACE("lookup_voterId: called fetchall, returned rows type=%s, val='%s'"
        % (str(type(rows)), str(rows)))
  if len(rows) < 1:
    TRACE("lookup_voterId: no voter matching name '%s'" % voterName)
    return 0
  elif len(rows) > 1:
    TRACE("lookup_voterId: WARNING: "
          "multiple voters matching name '%s': '%s'.  Returning first."
          % (str(voterName), str(rows)))
    return rows[0]['id']
  else:
    return rows[0]['id']

def add_voter(con, voterName):
  TRACE("adding voter " + voterName + " to db")
  cur = con.cursor()
  cur.execute(
    "INSERT INTO voters (name) VALUES ('" + voterName + "');"
  )
  con.commit()

def voters_active_session_query(cur, voterId):
  cur.execute(
      "SELECT "
          "sessions.voterId, voters.name, playlists.id, playlists.name, "
          "sessions.ip, sessions.mac, sessions.starttime, sessions.endtime "
      "FROM sessions "
      "LEFT JOIN voters ON voters.id = sessions.voterId "
      "LEFT JOIN playlists ON playlists.id = sessions.playlist_id "
      "WHERE sessions.voterId = %d "
      "AND starttime IS NOT NULL "
      "AND endtime = ''"
      % voterId
  )
  if len(rows) < 1:
      TRACE("No active session for voter %d" % voterId)
      return None
  if len(rows) > 1:
      TRACE("WARNING: multiple active sessions (%d) found for voter %d"
            % (len(rows), voterId))
      return None
  return cur.fetchall()


def get_session(cur, sessionId):
  cur.execute(
      "SELECT "
          "sessions.id AS sessionId, "
          "sessions.voterId, "
          "voters.name AS voterName, "
          "playlists.id AS playlistId, "
          "playlists.name AS playlistName, "
          "sessions.ip AS ip, "
          "sessions.mac AS mac, "
          "sessions.starttime AS starttime, "
          "sessions.endtime AS endtime "
      "FROM sessions "
      "LEFT JOIN voters ON voters.id = sessions.voterId "
      "LEFT JOIN playlists ON playlists.id = sessions.playlist_id "
      "WHERE sessions.id = %d "
      "AND starttime IS NOT NULL "
      "AND endtime = ''"
      % sessionId
  )
  row = cur.fetchone()
  return row

def new_session(con, voterId, playlistId, ip, mac):
  TRACE(''.join([
            "new_session: voterId=", str(voterId), ", "
            "playlistId = ", str(playlistId), ", "
            "ip=", str(ip), ", "
            "mac=", str(mac)
  ]))
  con.row_factory = sqlite3.Row
  cur = con.cursor()
  starttime = strftime("%Y-%m-%dT%H:%M:%S")
  cur.execute(
      "INSERT INTO sessions ("
          "voterId,"
          "playlist_id,"
          "ip,"
          "mac,"
          "starttime,"
          "endtime"
      ") VALUES ('%d', '%d', '%s', '%s', '%s', '%s');"
      % (voterId, playlistId, 'ip', 'mac', starttime, '')
  )
  con.commit()
  sessionId = cur.lastrowid
  #TRACE("new_session: type(lastrowid) = '%s'" % str(type(cur.lastrowid)))
  TRACE("new_session: type(cur.lastrowid) = '%s'" % str(type(cur.lastrowid)))
  TRACE("new_session: "
        "session id created and accessed via cur.lastrowid = %d"
        % sessionId)
  return get_session(cur, sessionId)

def get_playlist_name(cur, playlistId):
  cur.execute(
      "SELECT "
      "playlists.name "
      "FROM tracks "
      "INNER JOIN playlist_tracks ON tracks.id = playlist_tracks.track_num "
      "LEFT JOIN playlists ON playlist_tracks.playlist_id = playlists.id "
      "WHERE playlists.id = " + str(playlistId)
  )
  rows = cur.fetchall()
  if len(rows) >= 1:
      return str(rows[1][0])
  else:
      return ""

def create_player_playlist(con, voterId):
  TRACE("INSERT INTO config (param, value, voterId, updatedOn) "
        "VALUES ('player_playlist', '%s', '%d', '%s');"
        % (str(playlist_id), voterId, timestr))
  cur.execute("INSERT INTO config (param, value) "
              "VALUES ('player_playlist', '%s');"
              % str(playlist_id))
  pid = cur.lastrowid
  con.commit()
  return pid

def get_player_playlist(cur):
  sql = "SELECT param, value FROM config WHERE param = 'player_playlist'"
  cur.execute(sql)
  rows = cur.fetchall()
  if len(rows) >= 1:
      # return str(rows[1][0])
      str_param = str(rows[len(rows)-1][1])
      int_param = int(str_param)
      TRACE("player_playlist = %d" % int_param)
      return int_param
  else:
      return config.default_playlistId

def set_player_playlist(con, playlistId, voterId):
  cur = con.cursor()
  current_playlist_id = get_player_playlist(cur)
  TRACE("setting player playlist to %d; current is %d"
        % (playlistId, current_playlist_id))
  if playlistId == current_playlist_id:
      return playlistId

  # update the database
  # - current track status
  # sqlite has no zoneinfo handling; all times are in GMT/UTC
  # limited time formats are supported by sqlite; select one that's
  # ISO 8601-compatible (see: http://www.w3.org/TR/NOTE-datetime)
  timestr = strftime("%Y-%m-%dT%H:%M:%S", gmtime())

  if current_playlist_id > 0:
      sql = ''.join([
          "UPDATE config "
          "SET "
              "value = '", str(playlistId), "', "
              "voterId = ", str(voterId), ", "
              "updatedOn = '", timestr, "' "
          "WHERE param = 'player_playlist';"
      ])
      cur.execute(sql)
      con.commit()
  return get_player_playlist(cur)

def sessions_query():
  return (
            "SELECT "
                "id, name, voterId, ip, mac, starttime, "
                "endtime, status, playlist_id "
            "FROM sessions "
            "ORDER BY id "
            "DESC LIMIT 5"
          )

def playlists_select_query():
  return "SELECT id, name FROM playlists"

def winners_query (playlistId, numrecords):
  '''
  major query: this is where the magic happens
  this retreives the tally, including the score/ranking of every song
  '''
  recordLimit = ""
  if (numrecords > 0):
      recordLimit = "LIMIT " + str(numrecords)

  return ''.join([
      "SELECT "
      "( "
      "    COUNT(CASE WHEN vote = '1' THEN vote END) "
      "  - COUNT(CASE WHEN vote = '0' THEN vote END) "
      ") AS score, "
      "COUNT(CASE when vote = '1' THEN vote END) AS yaes, "
      "COUNT(CASE when vote = '0' THEN vote END) AS nays, "
      "tracks.title AS title, "
      "artists.name AS artist, "
      "albums.name AS album, "
      "ltrim(urls.rpath, '.') AS filename "
      "FROM votes "
      "JOIN tracks ON votes.trackId = tracks.id "
      "LEFT JOIN playlist_tracks ON tracks.id = playlist_tracks.track_num "
      "JOIN voters ON votes.voterId = voters.id "
      "LEFT JOIN urls ON tracks.url = urls.id "
      "LEFT JOIN artists ON tracks.artist = artists.id "
      "LEFT JOIN albums ON tracks.album = albums.id "
      "WHERE playlist_tracks.playlist_id = ", str(playlistId), " "
      "GROUP BY tracks.id "
      "ORDER BY score DESC, yaes DESC ",
      recordLimit, ";"
  ])

def losers_query (playlistId, numrecords):
      # major query: this is where the magic happens
      # this retreives the tally, including the score/ranking of every song

  recordLimit = ""
  if (numrecords > 0):
      recordLimit = "LIMIT " + str(numrecords)

  return ''.join([
      "SELECT "
          "( "
          "    COUNT(CASE WHEN vote = '1' THEN vote END) "
          "  - COUNT(CASE WHEN vote = '0' THEN vote END) "
          ") AS score, "
          "COUNT(CASE when vote = '1' THEN vote END) AS yaes, "
          "COUNT(CASE when vote = '0' THEN vote END) AS nays, "
          "tracks.title AS title, "
          "artists.name AS artist, "
          "albums.name AS album, "
          "ltrim(urls.rpath, '.') AS filename "
      "FROM votes "
      "JOIN tracks ON votes.trackId = tracks.id "
      "LEFT JOIN playlist_tracks ON tracks.id = playlist_tracks.track_num "
      "JOIN voters ON votes.voterId = voters.id "
      "LEFT JOIN urls ON tracks.url = urls.id "
      "LEFT JOIN artists ON tracks.artist = artists.id "
      "LEFT JOIN albums ON tracks.album = albums.id "
      "WHERE playlist_tracks.playlist_id = '", str(playlistId), "' "
      "GROUP BY tracks.id "
      "ORDER BY score, nays ",
      recordLimit, ";"
  ])

def votes_query(playlistId):
  return ''.join([
      "SELECT "
          "votes.id AS id, "
          "votes.vote, "
          "tracks.title AS title, "
          "artists.name AS artist, "
          "albums.name AS album, "
          "voters.name, "
          "votes.votedon, "
          "ltrim(urls.rpath, '.') AS filename "
      "FROM votes "
      "LEFT JOIN tracks ON votes.trackId = tracks.id "
      "LEFT JOIN playlist_tracks ON tracks.id = playlist_tracks.track_num "
      "JOIN voters ON votes.voterId = voters.id "
      "LEFT JOIN urls ON tracks.url = urls.id "
      "LEFT JOIN artists ON tracks.artist = artists.id "
      "LEFT JOIN albums ON tracks.album = albums.id "
      "WHERE playlist_tracks.playlist_id = ",
          str(playlistId), " "
      "ORDER BY votes.votedon DESC "
      "LIMIT 500;"
  ])

def tally_query(playlistId):
  return ''.join([
      "SELECT "
          "( "
          "    COUNT(CASE WHEN vote = '1' THEN vote END) "
          "  - COUNT(CASE WHEN vote = '0' THEN vote END) "
          ") AS score, "
          "COUNT(CASE when vote = '1' THEN vote END) AS yaes, "
          "COUNT(CASE when vote = '0' THEN vote END) AS nays, "
          "tracks.title AS title, "
          "artists.name AS artist, "
          "albums.name AS album, "
          "ltrim(urls.rpath, '.') AS filename "
      "FROM votes "
      "JOIN tracks ON votes.trackId = tracks.id "
      "LEFT JOIN playlist_tracks ON tracks.id = playlist_tracks.track_num "
      "JOIN voters ON votes.voterId = voters.id "
      "LEFT JOIN urls ON tracks.url = urls.id "
      "LEFT JOIN artists ON tracks.artist = artists.id "
      "LEFT JOIN albums ON tracks.album = albums.id "
      "WHERE playlist_tracks.playlist_id = ", str(playlistId), " "
      "GROUP BY tracks.id "
      "ORDER BY score DESC, yaes DESC "
      "LIMIT 500;"
  ])

def voters_query(cur):
  # alternative idea?
  # cur.execute("SELECT"
  #                 id, name, voterId, ip, mac, "
  #                 starttime, endtime, status, playlist_id "
  #              FROM sessions "
  #              ORDER BY id "
  #              DESC LIMIT 5"
  #            )
  querystr = ''.join([
      "SELECT "
        "voters.id AS voterId, "
        "voters.name AS voterName, "
        "votes, sessions, "
        "started "
      "FROM voters "
      "LEFT JOIN ("
        "SELECT "
        "count(id) AS sessions, "
        "voterId AS sessionVoterId, "
        "max(starttime) as started "
        "FROM sessions "
        "WHERE endtime like '' "
        "GROUP BY sessionVoterId"
      ") ON voters.id == sessionVoterId "
      "LEFT JOIN ("
        "SELECT votes.voterId, count(id) AS votes "
        "FROM votes "
        "GROUP BY votes.voterId"
      ") ON voters.id = voterId;"
  ])
  TRACE("voters_query: querystr(type = %s): '%s'"
        % (type(querystr), str(querystr)))
  cur.execute(querystr)
  rows = cur.fetchall()
  TRACE("voters_query: returning %d records with the following fields: %s"
        % (len(rows), rows[0].keys()))
  return rows

def playlist_query(cur, playlistId):
  # major query: this is where the magic happens
  # this retreives the tally, including the score/ranking of every song
  # Note: the playlistHistory join corrupts the COUNT
  # This may need to be a sub-query
  try:
      sql = (
          "SELECT "
              "( "
              "    COUNT(CASE WHEN vote = '1' THEN vote END) "
              "  - COUNT(CASE WHEN vote = '0' THEN vote END) "
              ") AS score, "
              "COUNT(CASE when vote = '1' THEN vote END) AS yaes, "
              "COUNT(CASE when vote = '0' THEN vote END) AS nays, "
              "tracks.title AS title, "
              "artists.name AS artist, "
              "albums.name AS album, "
              "ltrim(urls.rpath, '.') AS filename, "
              "Hist.last_played AS last_played, "
              "Hist.time_since_played AS time_since_played "
          "FROM playlist_tracks "
          "JOIN tracks ON tracks.id = playlist_tracks.track_num "
          "LEFT JOIN votes ON votes.trackId = tracks.id "
          "LEFT JOIN voters ON votes.voterId = voters.id "
          "LEFT JOIN urls ON tracks.url = urls.id "
          "LEFT JOIN artists ON tracks.artist = artists.id "
          "LEFT JOIN albums ON tracks.album = albums.id "
          "LEFT JOIN ("
              "SELECT playhistory.trackid AS id, "
              "IFNULL(MAX(playhistory.playedon), 'never') AS last_played, "
              "IFNULL( "
              "    strftime('%%s','now') "
              " -  strftime('%%s', SUBSTR(MAX(playhistory.playedOn), 1, 19)) "
              ", 1000000000"
              ") "
              "AS time_since_played "
              "FROM  playlist_tracks "
              "LEFT JOIN playhistory "
                  "ON playhistory.trackid = playlist_tracks.track_num "
              "WHERE playlist_tracks.playlist_id = '%s' "
              "GROUP BY playhistory.trackid "
          ") AS Hist "
          "ON Hist.id = tracks.id "
          "WHERE "
          "   playlist_tracks.playlist_id = '%s' "
          "GROUP BY tracks.id "
          "ORDER BY score DESC, yaes DESC, random() "
          "LIMIT 10 "
          ";"
          % (str(playlistId), str(playlistId))
      )
      """
          "AND "
          "   time_since_played > '18000 "
          "AND "
          "   CAST(time_since_played as integer) > 18000 "
              ", strftime('%%s','now') AS now,"
              ", strftime('%%s', SUBSTR(MAX(playhistory.playedOn), 1, 19))
                    AS last_played_seconds "
      """
      TRACE("sql: %s" % sql)
      cur.execute(sql)
  except:
      e = sys.exc_info()[0]
      TRACE("Exception running %s: %s" % (str(sys.argv[0]), str(e)))
      print_exc(file=env['wsgi.errors'])
      return str(sys.exc_info()[0]) + "\n" + format_exc(10)


def vote_tracks_query(voterId, playlistId):
  return ''.join([
      "SELECT "
          "tracks.id AS id, "
          "ltrim(urls.rpath, '.') AS filename, "
          "votes.voterId, "
          "votes.vote, "
          "tracks.title AS title, "
          "artists.name AS artist, "
          "albums.name AS album "
      "FROM playlist_tracks "
      "LEFT JOIN tracks ON tracks.id = playlist_tracks.track_num "
      "LEFT JOIN urls ON tracks.url = urls.id "
      "LEFT JOIN votes "
          "ON tracks.id = votes.trackId AND votes.voterId = ", str(voterId), " "
      "LEFT JOIN artists ON tracks.artist = artists.id "
      "LEFT JOIN albums ON tracks.album = albums.id "
      "WHERE playlist_tracks.playlist_id = ", str(playlistId), " "
      "ORDER BY random() "
      "LIMIT 500;"
  ])

def get_query_string(query_name, fargv):
  TRACE("get_query_string: %s %s" % (query_name, str(fargv)))

  if query_name == 'player_playlist_table':
      # major query: this is where the magic happens
      # this retreives the tally, including the score/ranking of every song
      # Note: the playlistHistory join corrupts the COUNT
      # This may need to be a sub-query
      # TODO: validate params
      playlistId = fargv[0]
      return (
          "SELECT "
              "( "
              "    COUNT(CASE WHEN vote = '1' THEN vote END) "
              "  - COUNT(CASE WHEN vote = '0' THEN vote END) "
              ") AS score, "
              "COUNT(CASE when vote = '1' THEN vote END) AS yaes, "
              "COUNT(CASE when vote = '0' THEN vote END) AS nays, "
              "tracks.title AS title, "
              "artists.name AS artist, "
              "albums.name AS album, "
              "ltrim(urls.rpath, '.') AS filename, "
              "Hist.last_played AS last_played, "
              "Hist.time_since_played AS time_since_played "
          "FROM playlist_tracks "
          "JOIN tracks ON tracks.id = playlist_tracks.track_num "
          "LEFT JOIN votes ON votes.trackId = tracks.id "
          "LEFT JOIN voters ON votes.voterId = voters.id "
          "LEFT JOIN urls ON tracks.url = urls.id "
          "LEFT JOIN artists ON tracks.artist = artists.id "
          "LEFT JOIN albums ON tracks.album = albums.id "
          "LEFT JOIN ("
              "SELECT playhistory.trackid AS id, "
              "IFNULL(MAX(playhistory.playedon), 'never') AS last_played, "
              "IFNULL( "
              "    strftime('%%s','now') "
              " -  strftime('%%s', SUBSTR(MAX(playhistory.playedOn), 1, 19)) "
              ", 1000000000"
              ") "
              "AS time_since_played "
              "FROM  playlist_tracks "
              "LEFT JOIN playhistory "
                  "ON playhistory.trackid = playlist_tracks.track_num "
              "WHERE playlist_tracks.playlist_id = '%s' "
              "GROUP BY playhistory.trackid "
          ") AS Hist "
          "ON Hist.id = tracks.id "
          "WHERE playlist_tracks.playlist_id = '%s' "
          "GROUP BY tracks.id "
          "ORDER BY score DESC, yaes DESC, random() "
          "LIMIT 10 "
          ";"
          % (str(playlistId), str(playlistId))
      )
  else:
      TRACE("unknown query: %s" % str(query_name))
      return ("unknown query: %s" % str(query_name))


def get_next_track(cur, playlistId):
  TRACE("enter get_next_track(playlistId = %d)" % playlistId)
  # major query: this is where the magic happens
  # this retreives the tally, including the score/ranking of every song
  cur.execute(
      "SELECT "
          "( "
          "    COUNT(CASE WHEN vote = '1' THEN vote END) "
          "  - COUNT(CASE WHEN vote = '0' THEN vote END) "
          ") AS score, "
          "COUNT(CASE when vote = '1' THEN vote END) AS yaes, "
          "COUNT(CASE when vote = '0' THEN vote END) AS nays, "
          "tracks.title AS title, "
          "artists.name AS artist, "
          "albums.name AS album, "
          "ltrim(urls.rpath, '.') AS filename, "
          "IFNULL(MAX(playhistory.playedOn), 'never') AS last_played, "
          "CAST("
          "IFNULL( "
          "    strftime('%%s','now') "
          " -  strftime('%%s', SUBSTR(MAX(playhistory.playedOn), 1, 19)) "
          ", 1000000000"
          ") "
          "AS Integer) "
          "AS time_since_played, "
          "tracks.id AS trackId, "
          "strftime('%%s','now') AS now, "
          "playlist_tracks.playlist_id AS playlist_id "
      "FROM playlist_tracks "
      "JOIN tracks ON tracks.id = playlist_tracks.track_num "
      "LEFT JOIN votes ON votes.trackId = tracks.id "
      "LEFT JOIN voters ON votes.voterId = voters.id "
      "LEFT JOIN urls ON tracks.url = urls.id "
      "LEFT JOIN artists ON tracks.artist = artists.id "
      "LEFT JOIN albums ON tracks.album = albums.id "
      "LEFT JOIN playhistory ON playhistory.trackid = tracks.id "
      "WHERE playlist_id = '%d' "
      "GROUP BY tracks.id, playhistory.playedOn "
      "ORDER BY score DESC, yaes "
      "LIMIT 1000 "
      ";"
      # TODO: add random sorting for use in the case of tiebreakers
      % playlistId
  )
  row = cur.fetchone()
  if not row:
      TRACE("get_next_track: No Tracks Found\n")
  else:
      while row:
          time_since_played = str(row[8])
          now = str(row[10])
          TRACE(
              "get_next_track: time_since_played = %s, now = %s\n"
              % (row['time_since_played'], now)
          )
          if int(time_since_played) > constants.MIN_REPEAT_TIME:
              next_track_dict = {
                  'score':              row['score'],
                  'filename':           row['filename'],
                  'yaes':               row['yaes'],
                  'nays':               row['nays'],
                  'title':              row['title'],
                  'artist':             row['artist'],
                  'album':              row['album'],
                  'last_played':        row['last_played'],
                  'time_since_played':  row['time_since_played'],
                  'trackId':            row['trackId']
              }
              TRACE("get_next_track: "
                    "returning %s, now = %s, last_played = %s"
                    % (row['filename'], now, row['last_played'])
              )
              break
          else:
              TRACE("get_next_track: "
                    "%s was played within the last %d seconds."
                    "  Trying next..."
                    % (row['filename'], constants.MIN_REPEAT_TIME)
              )
          row = cur.fetchone()
          if not row:
              TRACE("get_next_track: end of records")

  TRACE("next_track_dict: %s" % str(next_track_dict))
  return next_track_dict

def get_next_N_tracks(cur, playlistId, numtracks):
  TRACE("enter get_next_N_tracks(playlistId = %d, numtracks = %d)"
        % (playlistId, numtracks))
  tracks = []
  # major query: this is where the magic happens
  # this retreives the tally, including the score/ranking of every song
  sql = (
    "SELECT "
      "("
          "COUNT(CASE WHEN vote = '1' THEN vote END) "
          "- COUNT(CASE WHEN vote = '0' THEN vote END)"
      ") AS score, "
      "COUNT(CASE when vote = '1' THEN vote END) AS yaes, "
      "COUNT(CASE when vote = '0' THEN vote END) AS nays, "
      "tracks.id AS trackId, "
      "tracks.title AS title, "
      "artists.name AS artist, "
      "albums.name AS album, "
      "ltrim(urls.rpath, '.') AS filename, "
      "Hist.last_played AS last_played, "
      "Hist.time_since_played AS time_since_played, "
      "strftime('%%s','now') AS now, "
      "playlist_tracks.playlist_id AS playlist_id "
      "FROM playlist_tracks "
      "LEFT JOIN tracks ON tracks.id = playlist_tracks.track_num "
      "LEFT JOIN votes ON votes.trackId = tracks.id "
      "LEFT JOIN voters ON votes.voterId = voters.id "
      "LEFT JOIN urls ON tracks.url = urls.id "
      "LEFT JOIN artists ON tracks.artist = artists.id "
      "LEFT JOIN albums ON tracks.album = albums.id "
      "LEFT JOIN ("
        "SELECT "
          "playhistory.trackid AS id, "
          "IFNULL(MAX(playhistory.playedon), 'never') AS last_played, "
          "IFNULL("
              "strftime('%%s','now') "
              "- "
              "strftime('%%s', SUBSTR(MAX(playhistory.playedOn), 1, 19)"
            "), "
            "1000000000"
          ") AS time_since_played "
        "FROM playlist_tracks "
        "LEFT JOIN playhistory "
            "ON playhistory.trackid = playlist_tracks.track_num "
        "WHERE playlist_tracks.playlist_id = '%d' "
        "GROUP BY playhistory.trackid "
      ") AS Hist ON Hist.id = tracks.id "
      "WHERE playlist_tracks.playlist_id = '%d' "
      "GROUP BY tracks.id ORDER BY score DESC, yaes DESC "
      "LIMIT 1000"
    % (playlistId, playlistId)
  )
  TRACE("get_next_N_tracks: sql = '%s'" % sql)
  cur.execute(sql)
  row = cur.fetchone()
  if not row:
      TRACE("get_next_N_tracks: No Tracks Found\n")
  else:
    while row:
      time_since_played = str(row['time_since_played'])
      if not os.path.exists(row['filename']):
        TRACE("get_next_N_tracks: file '%s' is not accessible.  "
              "Not adding to list.\n" % row['filename'])
      else:
        TRACE("get_next_N_tracks: time_since_played = %s, now = %s\n"
              % (str(row['time_since_played']), str(row['now'])))
        if not row['time_since_played'] is None:
          if int(row['time_since_played']) > constants.MIN_REPEAT_TIME:
            tracks.append(row)
            TRACE("get_next_N_tracks: added "
                  "%s to the track list., now = %s, last_played = %s"
                  % (row['filename'], row['now'], row['last_played'])
            )
            if len(tracks) >= numtracks:
                break
          else:
            TRACE("get_next_N_tracks: "
                  "%s was played within the last %d seconds."
                  "  Trying next..."
                  % (row['filename'], constants.MIN_REPEAT_TIME)
            )
        else:
          TRACE("get_next_N_tracks: time_since_played is NULL.  Append track.")
          tracks.append(row)
      row = cur.fetchone()
    if not row:
      TRACE("get_next_N_tracks: end of records")
  TRACE("get_next_N_tracks: returning tracks: '%s'" % str(tracks))
  return tracks

