# -- coding: utf-8 --
#!/usr/bin/python3 -u

"""
@brief this file is used to maintain the session of a user
it may be expanded in the future to save and restore sessions somehow

@author: Bryant Hansen <github1@bryanthansen.net>
@license: GPLv3
"""

import sys
import os
import sqlite3
import logging
from time import strftime, gmtime
from multiprocessing.connection import Client

from traceback import format_exc, print_exc
from cgi import parse_qs, escape

import pyplayer.lib.pyplayerc as pyplayerc
import critter_list.config
import pyplayer.config
import critter_list.lib.sql as sql

# TODO: consider replacing this directly with logger/logging
from critter_list.lib.trace import TRACE

sys.path.insert(0, "/projects/critter_list/mod_wsgi/critter_list/lib")

'''
## http://stackoverflow.com/questions/3425512/how-do-i-get-the-same-functionality-as-cs-function-in-python#3425585

def startLog( func ):
    def loggedFunc( *args, **kwargs ):
        print( "starting {0} with {1}".format( func.__name__, args[ 0 ] ) )
        return func( *args, **kwargs )
    return loggedFunc

@startLog
def theFunc( ):
    print( "running theFunc" )

theFunc( )
# starting theFunc
# running theFunc

def theFunc( ):
    print( "running theFunc" )
theFunc = startLog( theFunc )

### can be done via logger/logging?
'''

def startLog( func ):
    def loggedFunc( *args, **kwargs ):
        TRACE( "starting {0} with {1}".format( func.__name__, args[ 0 ] ) )
        return func( *args, **kwargs )
    return loggedFunc

def get_int_param_from_url(urldict, paramName):
    if not paramName in urldict.keys():
        return 0
    vid = urldict[paramName]
    if isinstance(vid, str):
        TRACE(
            "get_int_param_from_url: string '%s' specified as '%s'" 
            % (vid, paramName)
            )
        if len(vid) < 1:
            return 0
        try:
            ivid = int(vid)
        except:
            ivid = 0
        TRACE(
           "get_int_param_from_url: returning %d for paramName %s" 
           % (ivid, paramName)
        )
        return ivid
    elif not isinstance(vid, int):
        TRACE(
            "get_int_param_from_url: non-integer '" + str(vid) + 
            "' of type " + str(type(vid)) + 
            " specified as " + paramName)
        return 0
    elif vid > 0:
        TRACE(
            "get_int_param_from_url: %s specified as %d of type %s"
            % (paramName, vid, str(type(vid)))
        )
        return vid
    else:
        return 0

# TODO: review this; the last writing of this, years ago, looks quite experimental
def get_login_from_env(env):
    loginname = ""
    try:
        a = env['wsgi.input'].read()
        TRACE("get_login_from_env: a='%s', type(a) = %s" % (a, str(type(a))))

        # TODO: handle utf-8 - error generated here
        # http://www.w3schools.com/tags/ref_urlencode.asp
        # parse_qs is cgi.parse_qs
        #b = unicodedata.normalize('NFD', a)
        #b.encode('ascii', 'ignore').decode('ascii')
        #TRACE("get_login_from_env: b='%s'" % b)
        #cmb_chrs = dict.fromkeys(c1 for c1 in range(sys.maxunicode) if unicodedata.combining(chr(c1)))
        #b = unicodedata.normalize('NFD', a)
        #b = b.translate(cmb_chrs)
        #b = unicodedata.normalize('NFD', a).translate(cmb_chrs)

        b = a.decode('utf-8')
        TRACE("get_login_from_env: b = a.decode('utf-8') = '%s'" % b)
        c = parse_qs(b)
        TRACE("get_login_from_env: c = parse_qs(b) = '%s'" % c)
        d = ""
        try:
            d = c['loginname'][0]
            TRACE("get_login_from_env: d = c['loginname'][0] ='%s'" % d)
        except:
            TRACE("get_login_from_env: exception reading loginname: %s"
                  % str(sys.exc_info()[0]))
            pass

        #e = d[0]
        #TRACE("get_login_from_env: e='%s', type(e)=%s" % (e, str(type(e))))

        loginname = d
    #except KeyError:
    #    TRACE("get_login_from_env: KeyError exception referencing dict element loginname")
    except:
        TRACE("get_login_from_env: exception referencing dict element loginname")
        e = sys.exc_info()[0]
        print_exc(file=env['wsgi.errors'])
        TRACE("sys.exec_info()[0]: " + str(sys.exc_info()[0]))
        TRACE("format_exc(10): " + format_exc(10))
        if len(loginname) <= 0:
            loginname = "exception"

    # Always escape user input to avoid script injection
    # TODO: figure how how to insert this prior to parsing
    loginname = escape(loginname)

    if len(loginname) > 0:
        TRACE("loginname: %s" % repr(loginname))
    else:
        TRACE("loginname not specified")
    return loginname


def get_or_add_voter(con, environ, voterName):
    TRACE("get_or_add_voter: voterName = %s" % voterName)
    cur = con.cursor()
    voterId = 0
    if len(voterName) > 0:
        TRACE("login name specified in environment: %s" % voterName)
        voterId = sql.lookup_voterId(cur, voterName)
        if voterId > 0:
            TRACE("voterId for %s is found as %d" % (voterName, voterId))
            vn = sql.lookup_votername(cur, voterId)
            if vn != voterName:
                TRACE(
                    "PROGRAM ERROR: DB did not return the same login name "
                    "as was just created.  created = %s, returned = %s"
                    % (voterName, vn)
                )
        else:
            TRACE("no voterId found for voter %s" % voterName)
            sql.add_voter(con, voterName)
            voterId = sql.lookup_voterId(cur, voterName)
            if voterId <= 0:
                TRACE(
                 "get_or_add_voter ERROR: failed to create voter record for %s"
                 % voterName
                )

    sVoterId = get_int_param_from_url(environ, 'voterId')
    if sVoterId > 0:
        voterId = sVoterId
        TRACE("voterId indicated in URL: %d" % voterId)

        vn = sql.lookup_votername(cur, voterId)
        if len(voterName) > 0 and vn != voterName:
            TRACE(
                "PROGRAM ERROR: voterId specified in URL as %d"
                " This is associated with voter %s"
                " but voterName %s"
                " specified"
                % (sVoterId, vn, voterName)
            )
        voterName = sql.lookup_votername(cur, voterId)
        if len(voterName) > 0:
            TRACE("voter %d exists in db as %s" % (voterId, voterName))
        else:
            TRACE("voter not found in voter db for id %d" % voterId)
            voterId = 0

    if len(voterName) <= 0:
        sql.add_voter(con, voterName)

    voterId = sql.lookup_voterId(cur, voterName)
    if voterId > 0:
        TRACE("get_or_add_voter: voterId for " + voterName + 
              " is found as " + str(voterId))
    return voterId, voterName


class SessionContext:

    def __init__(
        self,
        environ,
        url
    ):
        self.environ = environ
        self.url = url
        self.proxy = None
        self.voterId = 0
        self.voterName = ""
        self.show_debug = False
        self.admin = False
        self.cur = None
        self.urldict = {}

        # TODO detect ip and mac here
        TRACE("sessionContext.__init__: TODO: record IP and MAC here")
        self.ip = None
        self.mac = None

        TRACE("attempting to open database file %s"
              % critter_list.config.DB_FILE)
        self.con = None
        try:
            self.con = sqlite3.connect(critter_list.config.DB_FILE)
        except:
            TRACE("Exception %s opening database file" % str(sys.exc_info()))
            print_exc()
            # @TODO set/return some kind of status

        TRACE("accessing the database...")
        self.cur = None
        try:
            #self.con.text_factory = sqlite3.OptimizedUnicode
            self.con.row_factory = sqlite3.Row
            self.cur = self.con.cursor()
        except:
            TRACE("Exception %s accessing the database" % str(sys.exc_info()))
            print_exc()
            # @TODO set/return some kind of status
        if self.cur == None:
            raise "Fatal: failed to get database cursor"

        self.urldict = {}
        # safely parse the urldict here and only here
        urldict = parse_qs(environ['QUERY_STRING'])
        for urlkey, urlvalue in urldict.items():
            TRACE("sessionContext.__init__: "
                  "urldict.key(type %s)='%s', urldict.val[0](type %s)='%s'"
                  % (str(type(urlkey)), str(urlkey),
                     str(type(urlvalue[0])), str(urlvalue[0])
                  )
            )
            key = escape(urlkey)
            value = escape(urlvalue[0])
            self.urldict[key] = value
        urldict = None

        # loginname may come from a POST request (from the login page)
        # indicating the name.
        # voterId may come from url line ?voterId=NNN
        # If both arrive, verify that there is not a conflict.
        # If the sessionId arrives in the URL, then lookup the voterId in the
        # db
        loginname = get_login_from_env(environ)

        urlVoterId = get_int_param_from_url(self.urldict, 'voterId')
        TRACE("urlVoterId = %s" % str(urlVoterId))
        if len(loginname) > 0:
            self.voterId, self.voterName = get_or_add_voter(self.con,
                                                          self.environ,
                                                          loginname)
            if urlVoterId > 0 and urlVoterId != self.voterId:
                TRACE("application ERROR: voterId = %d"
                      " for voterName '%s', urlVoterId = '%d'"
                      ".  No error handler.  proceeding with voterId %d"
                      % (self.voterId,  self.voterName , urlVoterId, self.voterId)
                )
                self.voterId = 0
            TRACE(
                "application: voterId = %d, voter lookup: %s" 
                % (self.voterId, self.voterName)
            )
        elif urlVoterId > 0:
            voterName = sql.lookup_votername(self.cur, urlVoterId)
            if len(voterName) < 1:
                TRACE(
                    "application ERROR: no voter found for voterId %d" 
                    % urlVoterId
                )
                self.voterId = 0
            else:
                self.voterId = urlVoterId
                TRACE(
                    "application: voterId = %d, voter lookup: %s" 
                    % (self.voterId, self.voterName)
                )
        else:
            TRACE(
                "application: no login name specified and no login ID found.  "
                "urlVoterId = %d" 
                % urlVoterId
            )


        # TODO: determine how to fetch admin privileges
        # This is where to do it...after we have fetch and validated the voter
        self.admin = True





        if critter_list.config.defaultClientWidth > 0:
            clientWidth = critter_list.config.defaultClientWidth
        else:
            clientWidth = 1024
        if critter_list.config.defaultClientWidth > 0:
            clientHeight = critter_list.config.defaultClientHeight
        else:
            clientHeight = 768
        width = get_int_param_from_url(self.urldict, 'width')
        if width > 0:
            clientWidth = width
            TRACE("width set on command line as %d" % width)

        height = get_int_param_from_url(self.urldict, 'height')
        if height > 0:
            clientHeight = height
            TRACE("height set on command line as %d" % height)

        self.playlistId = critter_list.config.default_playlistId
        pid = get_int_param_from_url(self.urldict, 'playlistId')
        if isinstance(pid, str):
            pid = 0
            TRACE("playlist ID not specified in URL")
        if pid > 0:
            TRACE("application: overriding playlistId %d "
                  "with url-specified %d"
                  % (self.playlistId, pid)
            )
            self.playlistId = pid

        sid = get_int_param_from_url(self.urldict, 'sessionId')
        # TODO: why do I not know the type here?
        if isinstance(sid, str):
            sid = 0
        if sid > 0:
            TRACE("application: continuing session %d "
                  % (sid)
            )
            self.sessionId = sid
        else:
            self.sessionId = critter_list.config.default_sessionId

        sdict = None
        if self.sessionId == 0:
            if self.voterId == 0:
                raise "can't create a new session for a null voter"
            # sdict = startLog(self.get_voters_active_session(self.cur, voterId))
            sdict = self.get_voters_active_session(self.cur, self.voterId)
            if sdict == None:
                TRACE("no active sessions for voter id %d.  sql.new_session"
                      % voterId)
                sdict = sql.new_session(self.con,
                                        self.voterId,
                                        self.playlistId,
                                        self.ip,
                                        self.mac)
        else:
            self.con.row_factory = sqlite3.Row
            self.cur = self.con.cursor()
            sdict = self.get_session(self.cur, self.sessionId)
            if sdict == None:
                TRACE("sql.new_session")
                sdict = sql.new_session(self.con,
                                        self.voterId,
                                        self.playlistId,
                                        self.ip,
                                        self.mac)
            if sdict == None:
                raise "failed to get a session or create one"

        if sdict == None:
            return None

        TRACE("type(sdict) = %s" % str(type(sdict)))
        TRACE("str(sdict) = %s" % str(sdict))
        TRACE("keys(sdict) = %s" % str(sdict.keys()))
        self.record = sdict
        self.sessionId = sdict['sessionId']
        self.voterId = sdict['voterId']
        self.voterName = sdict['voterName']
        self.playlistName = sdict['playlistName']

        # TODO: get selected submenus here; decide on data structure; use a pickled dict?

        if self.playlistId == 0:
            self.playlistId = sdict['playlistId']

        TRACE(
            ''.join(["# TODO: adjust narrow based on "
              "clientHeight(", str(clientWidth), ") and "
              "clientWidth(", str(clientHeight), ")"
        ]))
        self.clientWidth = clientWidth
        self.clientHeight = clientHeight
        self.narrow = False



        if not os.path.exists(pyplayer.config.pidfile):
            TRACE('pyplayerd pidfile %s not found; '
                  'it does not appear to be running.  '
                  '(tip: execute "path-to/pyplayerd start")'
                  % pyplayer.config.pidfile)
            '''
            raise RuntimeError(
                    'pyplayerd pidfile %s not found; '
                    'it does not appear to be running.  '
                    '(tip: execute "path-to/pyplayerd start")'
                    % pyplayer.config.pidfile)
            '''

        # TODO: check for any 'pyplayerd.py start' process running
        #       ...or is this not necessary, since this is the server and it
        #       could be on a different host than the client?
        else:
            try:
                TRACE("connecting to server %s on port %s"
                    % (pyplayer.config.SERVER, pyplayer.config.PORT))
                c = Client((pyplayer.config.SERVER, pyplayer.config.PORT),
                            authkey=pyplayer.config.authkey)
            except socket_error as serr:
                if serr.errno != errno.ECONNREFUSED:
                    # Not the error we are looking for, re-raise
                    print("Connection Refused on %s:%d.  Is the daemon running?"
                        % (pyplayer.config.SERVER, pyplayer.config.PORT))
                    sys.exit(2)
            except:
                e = sys.exc_info()
                TRACE("exception connecting to server; exception info: %s"
                      % str(e))
                print("*** print_exc:")
                traceback.print_exc()
                sys.exit(3)

            self.proxy = pyplayerc.RPCProxy(c)


    def get_session(self, cur, sessionId):
        TRACE("get_session: fetching session matching id %d" % sessionId)
        row = sql.get_session(cur, sessionId)
        if row is None:
            TRACE("get_session: no session found")
            return None
        TRACE("get_session: fetched session; type(row) = %s" % type(row))
        return row


    def get_voters_active_session(self, cur, voterId):
        TRACE("looking for active session for voter %d" % voterId)
        rows = voters_active_session_query(cur, voterId)
        # TODO: can this be replaced by a dictionary object?
        return rows[len(rows)-1]
