# -- coding: utf-8 --
#!/usr/bin/python3 -u

"""
critter.py - the main library for the critter list, a dynamic voting system
for playlists

@author: Bryant Hansen <github1@bryanthansen.net>
@license: GPLv3
"""

from __future__ import unicode_literals

import sys
import os

from random import randint, choice
from cgi import escape
from traceback import print_exc
from pprint import pformat

import critter_list.lib.sql as sql
from critter_list.lib.trace import TRACE


def dump_todo():
    return todo.text

def show_env(env, start_response):
    start_response('200 OK', [('Content-Type', 'text/plain')])
    return [
        '%s\r\n\r\nClient Address: %s\r\n'
        % (pformat(env), get_client_address(env))
    ]

def get_base_filename_from_env(env, name):
    base_url = ""
    try:
        # default script will be based on name; fallback to HTTP_REFERER
        base_url = env[name].split('?')[0]
    except:
        pass
    base_url = os.path.basename(base_url)
    TRACE("get_base_filename_from_env(%s): returning %s" % (name, base_url))
    return base_url


"""
Figure out who is logged in, so that they don't get a duplicate voter record
and multiple votes

@return: voter name as a string
"""
def discover_voter(env, urldict, con):
    """
    This function must determine some way of finding out if we know this voter
    It currently returns the name, but it should return a valid ID
    """

    voterId = 0
    voterName = ""
    ip = "ip not available"
    mac = "mac not available"

    try:
        ip = get_client_address(env)
        if str(ip) == "::1" or str(ip) == "127.0.0.1" or str(ip) == "localhost":
            mac="01:02:03:04:05:06"
        if (len(ip) > 0):
                arp = tuple(open('/proc/net/arp', 'r'))
                for l in arp:
                    a = l.split()
                    if a[0] == ip:
                        mac = a[3]
                        break
    except:
        e = sys.exc_info()[0]
        TRACE("Exception fetching voter IP & MAC")
        sys.stderr.write(
            "<div id='ERROR'>\n"
            "<p>Exception in critter.py / vote() - failed to parse vote</p>\n"
            "</div>\n"
            "%s\n"
            % str(e)
        )

    TRACE(" voter ip: %s" % str(ip))
    TRACE(" voter mac: %s" % str(mac))

    # Returns a dictionary containing lists as values.
    votestr = urldict.get('voterId', [''])[0] # Returns the first voterId value.
    #hobbies = d.get('hobbies', []) # Returns a list of hobbies.

    # Always escape user input to avoid script injection
    voterId = escape(votestr)
    if voterId:
        TRACE("voterId indicated in URL: %d" % voterId)
        voterName = sql.lookup_votername(con.cursor(), voterId)
        if len(voterName) > 0:
            TRACE("voter %d exists in db as %s" % (voterId, voterName))
        else:
            TRACE("voter not found in voter db for id %s" % voterId)
            voterId = 0

    # TODO: try to get this from MAC address, IP address, cookie or even
    #       sessionId?

    return voterId, voterName



'''
    TODO connect callback from pyplayer here
'''
def track_complete():
    # attempt to load the queue first?
    _playerqueue = None
    try:
        _playerqueue = proxy.dump_queue()
    except:
        print("track_complete: exception calling proxy.dump_queue()")
        print_exc()

    if _playerqueue.count('\n') < 2:
        # auto-adding to queue
        
        
        # TODO: do the query
        
        
        TRACE("track_complete: TODO add next URL(s) to queue %s" % (url))
        #try:
        #    url = get next url
        #    result = sessionContext.proxy.add_url_to_queue(url)
        #except:
        #    print("track_complete: exception adding urls to the queue")
        #    print_exc()

    TRACE(" **** TRACK COMPLETE ****")
