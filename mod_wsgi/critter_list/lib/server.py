# -- coding: utf-8 --
#!/usr/bin/python3 -u

"""
@brief this file is used to maintain the session of a user
it may be expanded in the future to save and restore sessions somehow

@author: Bryant Hansen <github1@bryanthansen.net>
@license: GPLv3
"""

import sys
import logging

import critter_list.lib.sql as sql

# TODO: consider replacing this directly with logger/logging
from critter_list.lib.trace import TRACE


class ServerContext:
    '''
    @TODO determine a good way of caching this (making it persistent), if that's not done already
    '''

    def __init__(
        self,
        voterId,
        urldict,
        con
    ):
        cur = con.cursor()
        self.playerPlaylistId = sql.get_player_playlist(cur)
        self.playerPlaylistName = sql.get_playlist_name(cur, self.playerPlaylistId)

    def set_player_playlist(con, playlistId, voterId):
        self.playerPlaylistId = sql.set_player_playlist(
                                      con,
                                      playlistId,
                                      voterId)
        '''
        try:
            urlPlaylistId = int(urldict['playlistId'])
        except:
            urlPlaylistId = 0
        self.playerPlaylistId = urlPlaylistId
        if self.playerPlaylistId < 1:
            newpid = sql.create_player_playlist(con, voterId)
            sql.set_player_playlist(dbConnection, playlistId, voterId)
            TRACE(
                "application: WARNING: failed to get the player playlist ID.  "
                "setting a new one to %d (%s)."
                % (self.playerPlaylistId)
            )
        elif urlPlaylistId > 0 and urlPlaylistId != self.playerPlaylistId:
            TRACE(
                "application: changing player playlist setting from %s to %s"
                % (self.playerPlaylistId, urlPlaylistId)
            )
            sql.set_player_playlist(con, urlPlaylistId, voterId)
            self.playerPlaylistId = sql.get_player_playlist(cur)
        self.playerPlaylistName = sql.get_playlist_name(cur, self.playerPlaylistId)
        '''
