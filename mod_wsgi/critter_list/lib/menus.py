# -- coding: utf-8 --
#!/usr/bin/python3 -u

"""
@brief the menus - file separated due to massive size of the main

@author: Bryant Hansen <github1@bryanthansen.net>
@license: GPLv3
"""

import critter_list.config as config
from critter_list.lib.trace import TRACE
try:
    from io import StringIO
except ImportError:
    from cStringIO import StringIO

menu_sortorder = ['ADMIN', 'PLAYER', 'VOTE', 'CONFIG', 'STATS', 'DOCS']
submenu_map = {
    "ADMIN":  [ "VOTERS", "VOTES", "PLAYLISTS", "PARAMS", "SESSION" ],
    "PLAYER": [ "PLAYER", "PODCASTS", "GONG",  "VISUALS" ],
    "VOTE":   [ "LIST", "SWIPE" ],
    "CONFIG": [ "PLAYLIST", "ADD TRACKS", "OTHER PLAYLISTS" ],
    "DOCS":   [ "DOCS", "CONCEPT" ],
    "STATS":  [ "STATS", "TALLY", "ALL VOTES" ]
}
pages = {
    'ADMIN'              : "voters.wsgi",
    'ADD TRACKS'         : "add_track.wsgi",
    'CONCEPT'            : "concept.wsgi",
    'CONTROL'            : "control.wsgi",
    'CONFIG'             : "config.wsgi",
    'CRITTER'            : "critter.wsgi",
    'DOCS'               : "docs.wsgi",
    'GONG'               : "gong.wsgi",
    'PLAYER'             : "player.wsgi",
    'LIST'               : "player_playlist.wsgi",
    'OTHER PLAYLISTS'    : "other_playlists.wsgi",
    'PARAMS'             : "admin_params.wsgi",
    'PLAYLIST'           : "config_playlist.wsgi",
    'PLAYLISTS'          : "admin_playlists.wsgi",
    'PODCASTS'           : "podcast.wsgi",
    'SESSION'            : "session_context.wsgi",
    'SESSIONS'           : "sessions.wsgi",
    'STATS'              : "stats.wsgi",
    'SWIPE'              : "swipe.wsgi",
    'TALLY'              : "tally.wsgi",
    'TRACKS'             : "tracks.wsgi",
    'VISUALS'            : "visuals.wsgi",
    'VOTE'               : "player_playlist.wsgi",
    'VOTERS'             : "admin_voters.wsgi",
    'VOTES'              : "admin_votes.wsgi",
    'ALL VOTES'          : "votes.wsgi"
}

# TODO: this should be a user-parameter that is saved and restored in the DB
selected_submenus = {
    "ADMIN":  "VOTERS",
    "PLAYER": "PLAYER",
    "VOTE":   "LIST",
    "CONFIG": "PLAYLIST",
    "DOCS":   "DOCS",
    "STATS":  "STATS"
}
'''
    'VOTE'            : "vote.wsgi",
'''

class MenuItem:
    # TODO: some controls should only be available for an admin
    def __init__(self, name, selection, sessionId):
        self.name = name
        self.selection = selection
        self.url = ''
        if pages[name]:
            self.url = ''.join([
                            config.wsgi_path,
                            "/", pages[name],
                            "?sessionId=", str(sessionId)
                          ])
    def to_html(self):
        menuitem_html = ""
        if not self.selection == None:
            itemclass="menuitem_unsel"
            if self.name == self.selection:
                itemclass="menuitem_sel"
            menuitem_button = ''.join([
                "<button class='", itemclass, "'>",
                self.name,
                "</button>\n"
            ])
            if len(self.url) > 0:
                menuitem_html = ''.join([
                    "<a class='", itemclass, "' href='", self.url, "'>",
                    menuitem_button,
                    "</a>\n"
                ])
            else:
                menuitem_html = menuitem_button
        return menuitem_html

def dump_main_menu(sessionId, selection, narrow = False):
    TRACE("dump_main_menu: sessionId = %d, selection = '%s'" % (sessionId, selection))
    req = StringIO()
    # if there is a selected submenu, override the link for this with with the that selected submenu page
    for menu_name in menu_sortorder:
        # set the page url for the menu to the page of the selected submenu item
        if selected_submenus[menu_name]:
            if pages[menu_name] != pages[selected_submenus[menu_name]]:
                TRACE(
                    "dump_main_menu: "
                    "changing main menu link for page '%s' from '%s' "
                    "to selected submenu link for submenu '%s': '%s'"
                    % (menu_name,
                       pages[menu_name],
                       selected_submenus[menu_name],
                       pages[selected_submenus[menu_name]]
                    )
                )
                pages[menu_name] = pages[selected_submenus[menu_name]]
        selected_submenu_index = 0
        selected_submenu = submenu_map[menu_name][selected_submenu_index]
        pages[menu_name] = pages[selected_submenu]
        req.write(MenuItem(menu_name, selection, sessionId).to_html())
    # sanity-check that our sort menu is not missing anything
    for menu_name in submenu_map.keys():
        if not menu_name in menu_sortorder:
            TRACE("WARNING: main menu '%s' not found in the menu sortorder"
                  % menu_name)
            # tack it on to the end, since we want to show the submenus anyway
            req.write(MenuItem(menu_name, selection, sessionId).to_html())
    return req.getvalue()

def dump_menu_delimiter(sessionId, narrow = False):
    if narrow: return "</tr>\n<tr>\n"
    else:      return ""

def dump_submenu(sessionId, parent_name, selection):
    if submenu_map[parent_name] == None:
        return ''
    req = StringIO()
    for submenu_name in submenu_map[parent_name]:
        req.write(MenuItem(submenu_name, selection, sessionId).to_html())
    return req.getvalue()

def lookup_submenu_index(submenu, submenu_name):
    # TODO: consider how to use a dict instead of a function for a faster lookup with less code
    for i in range(len(submenu)):
        if submenu[i] == submenu_name:
            # found
            return i
    # not found
    return -1

def dump_menu(sessionId, selection, narrow = False):
    default_main_menu = "VOTE"
    main_menu = default_main_menu
    submenu_name = None
    submenu_index = -1
    if selection in submenu_map.keys():
        main_menu = selection
        if submenu_map[main_menu] == None:
            # The main menu has no submenu items
            TRACE("selected menuitem '%s' has no submenus" % selection)
        else:
            # The main menu has a submenu; default to the first element
            submenu_index = 0
            try:
                if selected_submenus[main_menu]:
                    submenu_name = selected_submenus[main_menu]
                    submenu_index = lookup_submenu_index(
                                        submenu_map[main_menu],
                                        submenu_name)
                    if submenu_index < 0:
                        TRACE(
                            "WARNING: selected submenu '%s' not found "
                            "in main menu '%s'.  "
                            "Defaulting to '%s'"
                            % (
                                submenu_name,
                                main_menu,
                                submenu_map[main_menu][submenu_index]
                            )
                        )
                        submenu_index = 0
            except:
                TRACE("Exception getting submenu for main menu %s" % main_menu)
            submenu_name = submenu_map[main_menu][submenu_index]
            TRACE("menu %s, submenu[%d]: '%s'"
                  % (selection, submenu_index, submenu_name))
    else:
        # the selection is not present in the main menu
        # find a matching submenu item
        for menuitem in submenu_map.keys():
            if not submenu_map[menuitem] == None:
                if not selection in submenu_map[menuitem]:
                    continue
                main_menu = menuitem
                submenu_name = selection
                TRACE("found submenu '%s' in main menu '%s'"
                      % (submenu_name, main_menu))
                break
    TRACE("selection = '%s', main_menu = '%s', "
          "submenu_name = '%s'"
          % (selection, main_menu, str(submenu_name)))
    return '\n'.join([
        "<table class='menu'>\n<tr>"
        "<td class='menu'>\n",
        dump_main_menu(sessionId, main_menu),
        "</td>\n",
        dump_menu_delimiter(sessionId),
        "<td class='submenu'>\n",
        dump_submenu(sessionId, main_menu, submenu_name),
        "</td>\n"
        "</tr>\n</table>"
    ])

