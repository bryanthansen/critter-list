# -- coding: utf-8 --
#!/usr/bin/python3 -u

'''
ajax.py - server-side ajax request handlers for the Critter List

@author: Bryant Hansen <github1@bryanthansen.net>
@license: GPLv3
'''
from __future__ import unicode_literals

'''
import sys
import os
from time import strftime, gmtime

from cgi import parse_qs, escape

from critter_list.lib.trace import TRACE, get_debug
from traceback import format_exc, print_exc

import tables
#import critter_list.lib.tables as tables
'''

import sys
import os
import re
import subprocess
from time import strftime, gmtime
from datetime import datetime
from random import randint, choice

from critter_list.lib.trace import TRACE, get_debug

# append the local directory to the path
sys.path.append(os.path.dirname(__file__))

import critter_list.config as config
import critter_list.constants as constants
import critter_list.todo as todo
import critter_list.lib.menus as menus
import critter_list.lib.tables as tables
import critter_list.lib.sql as sql
from critter_list.lib.session import SessionContext
import critter_list.lib.session as session
from critter_list.lib.server import ServerContext

try:
    from io import StringIO
except ImportError:
    from cStringIO import StringIO

from cgi import parse_qs, escape
from traceback import format_exc, print_exc
from pprint import pformat


def validate_vote_url(voteStr, trackStr, sessionStr, playlistStr):
    """
    TODO: validate session and other params against DB
    """
    if int(sessionStr) <= 0:
        TRACE("ERROR: invalid session Id: %s" % str(sessionStr))
        return False
    if voteStr != "yae" and voteStr != "nay":
        TRACE("ERROR: invalid vote (must be 'yae' or 'nay'): %s" % voteStr)
        return False
    if int(trackStr) <= 0:
        TRACE("ERROR: invalid track Id: %s" % str(trackStr))
        return False
    if int(playlistStr) <= 0:
        TRACE("WARNING: invalid playlist Id: %s" % str(playlistStr))
        return False
    return True

def add_url_to_queue(sessionContext, url):
    response = "no response"
    TRACE("add_url_to_queue: adding URL to queue %s" % (url))
    try:
        response = sessionContext.proxy.add_url_to_queue(url)
    except:
        e = sys.exc_info()
        TRACE("exception connecting to server; exception info: %s" % str(e))
        response = str(e)
    str_result = str(response)
    TRACE("proxy.%s response: %s\n" % (sys.argv[0], str_result))
    str_result = str_result.replace("'", "")
    return str_result

def playurl(sessionContext, url):

    response = "no response"

    # attempt to load the queue first?
    TRACE("playurl: adding URL to queue %s" % (url))
    try:
        result = sessionContext.proxy.add_url_to_queue(url)
    except:
        print("exception adding urls to the queue")
        print_exc()

    TRACE("playing")
    try:
        response = sessionContext.proxy.play()
    except:
        e = sys.exc_info()
        TRACE("exception connecting to server; exception info: %s" % str(e))
        response = str(e)
        print("*** print_exc:")
        print_exc()
        sys.exit(4)

    str_result = str(response)
    TRACE("proxy.%s response: %s\n" % (sys.argv[0], str_result))
    str_result = str_result.replace("'", "")
    TRACE("%s\n" % (str_result))
    # TODO: update official state if we have a proper response
    if str_result == "playing":
        TRACE("PLAYING!  Update state!")
    return str_result

def send_play_command(sessionContext):
    TRACE("send_play_command")
    response = "no response"
    try:
        response = sessionContext.proxy.play()
    except:
        e = sys.exc_info()
        TRACE("exception connecting to server; exception info: %s" % str(e))
        response = str(e)
        print("*** print_exc:")
        print_exc()
        sys.exit(4)
    str_result = str(response)
    TRACE("proxy.%s response: %s\n" % (sys.argv[0], str_result))
    str_result = str_result.replace("'", "")
    if str_result == "playing":
        TRACE("PLAYING!  Update state!")
        # TODO: update state here
    return str_result

def send_seek_start_command(sessionContext):
    response = "no response"
    TRACE("seek_start")
    try:
        response = sessionContext.proxy.seek_start()
    except:
        e = sys.exc_info()
        TRACE("exception connecting to server; exception info: %s" % str(e))
        response = str(e)
        print("*** print_exc:")
        print_exc()
    str_result = str(response)
    TRACE("proxy.%s response: %s\n" % (sys.argv[0], str_result))
    str_result = str_result.replace("'", "")
    TRACE("%s\n" % (str_result))
    return str_result

def send_gong_command(sessionContext):
    response = "no response"
    TRACE("send_gong_command")
    try:
        response = sessionContext.proxy.gong()
    except:
        e = sys.exc_info()
        TRACE("exception connecting to server; exception info: %s" % str(e))
        response = str(e)
        print("*** print_exc:")
        print_exc()
    str_result = str(response)
    TRACE("proxy.%s response: %s\n" % (sys.argv[0], str_result))
    str_result = str_result.replace("'", "")
    TRACE("%s\n" % (str_result))
    return str_result

def send_seek_end_command(sessionContext):
    TRACE("send_seek_end_command")
    response = "no response"
    try:
        response = sessionContext.proxy.seek_end()
    except:
        e = sys.exc_info()
        TRACE("exception connecting to server; exception info: %s" % str(e))
        response = str(e)
        print("*** print_exc:")
        print_exc()
    str_result = str(response)
    TRACE("proxy.%s response: %s\n" % (sys.argv[0], str_result))
    str_result = str_result.replace("'", "")
    TRACE("%s\n" % (str_result))
    return str_result

def track_complete_callback():
    TRACE(" *** TRACK COMPLETE *** ")

def gong(sessionContext):
    response = "no response"
    TRACE("gong")
    try:
        response = sessionContext.proxy.gong()
    except:
        e = sys.exc_info()
        TRACE("exception connecting to server; exception info: %s" % str(e))
        response = str(e)
        print("*** print_exc:")
        print_exc()
        sys.exit(4)
    str_result = str(response)
    TRACE("proxy.%s response: %s\n" % (sys.argv[0], str_result))
    str_result = str_result.replace("'", "")
    TRACE("%s\n" % (str_result))
    return str_result

def do_play(sessionContext, serverContext):
  # get the current state (playing/paused/stopped)
  # get the track to be played
  # get the set up the command to play the track
  # check the status that it started ok
  # update the database
  # - previous track status
  # - current track status

  # get the track to be played
  tracks = sql.get_next_N_tracks(sessionContext.cur,
                                  serverContext.playerPlaylistId,
                                  5)
  TRACE("do_play: get_next_N_tracks returned %d tracks" % len(tracks))
  firsttrack = tracks[0]
  trackId = 0
  filename = ""
  try:
    trackId = int(firsttrack['trackId'])
  except:
    trackId = 0
  try:
    filename = firsttrack['filename']
  except:
    pass
  if trackId >= 1:
    TRACE("do_play: next track Id = %s" % str(trackId))

    # update the database
    # - current track status
    # sqlite has no zoneinfo handling; all times are in GMT/UTC
    # limited time formats are supported by sqlite; select one that's
    # ISO 8601-compatible (see: http://www.w3.org/TR/NOTE-datetime)
    timestr = strftime("%Y-%m-%dT%H:%M:%S", gmtime())

    # alternative methods:
    #   timestr = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S")
    #   timestr = datetime.now('GMT').strftime("%Y-%m-%dT%H:%M:%S")
    try:
      TRACE(
        "updating play history: "
        "track = %s, playedOn = %s" 
        % (str(trackId), timestr)
      )
      sessionContext.cur.execute(
        "INSERT INTO playhistory ("
          "trackId, "
          "playedOn"
          ") "
        "VALUES (%s, '%s');"
        % (str(trackId), timestr)
      )
      sessionContext.con.commit()
    except:
      e = sys.exc_info()[0]
      TRACE("Exception running %s: %s" % (str(sys.argv[0]), str(e)))
      print_exc(file=sessionContext.environ['wsgi.errors'])
      return str(sys.exc_info()[0]) + "\n" + format_exc(10)

    for track in tracks:
      try:
        TRACE(
            "adding '%s' to pyplayer queue..."
            % track['filename']
        )
        add_url_to_queue(sessionContext, track['filename'])
      except:
        e = sys.exc_info()[0]
        TRACE(
          "Exception running self.playurl(%s): %s"
          % (filename, str(e))
        )
        print_exc(file=sessionContext.environ['wsgi.errors'])
    try:
      return send_play_command(sessionContext)
    except:
      e = sys.exc_info()[0]
      TRACE(
        "Exception running self.send_play_command(%s): %s"
        % (filename, str(e))
      )
      print_exc(file=sessionContext.environ['wsgi.errors'])
      # TODO: change state here if playurl does not do it
      return str(sys.exc_info()[0]) + "\n" + format_exc(10)
  else:
    TRACE("do_play ERROR: get_next_track failed.  "
          "No track ID")
    return "do_play ERROR"

def do_player_command(sessionContext, serverContext):
    # if play
        # check state if already playing
        # get next track
    # update last played
    # start player
    # setup callback for finish
    # finish callback
        # update played-track status (finished ok)
        # find and play next track
    # TODO: consider launching mplayer in thread
        # bullet-proof code to limit the number of instances that can be
        # launched (at least 2 should be allowed for optional crossfade)
    # example command:
    # command=play&sessionId=123456789&voterId=2&width=2560&height=1265&page=%s/player.wsgi % config.wsgi_path
    # Always escape user input to avoid script injection

    TRACE(''.join([
        "do_player_command: "
        "command = '",           sessionContext.urldict['command'],      "', "
        "url = '",               sessionContext.url,                     "', "
        "sessionId = ",          str(sessionContext.sessionId),          ", "
        "voterId = ",            str(sessionContext.voterId),            ", "
        "env(QUERY_STRING) = '", sessionContext.environ['QUERY_STRING'], "', "
        "urldict = '",           str(sessionContext.urldict),            "'"
    ]))
    # this is the extra params on the URL after the question mark (?)
    # as is conventional, each param is delimited by the ampersand (&)
    # Returns a dictionary containing lists as values.

    if sessionContext.urldict['command'] == 'play':
        do_play(sessionContext, serverContext)
    elif sessionContext.urldict['command'] == 'seek_end':
        try:
            TRACE("do_player_command: attempting to seek_end via pyplayer...")
            send_seek_end_command(sessionContext)
        except:
            e = sys.exc_info()[0]
            TRACE("Exception running %s: %s" % (str(sys.argv[0]), str(e)))
            print_exc(file=sessionContext.environ['wsgi.errors'])
            return str(sys.exc_info()[0]) + "\n" + format_exc(10)
    else:
        TRACE("do_player_command ERROR: unknown command: %s.  "
              % str(sessionContext.urldict['command']))
    testurl = sessionContext.url.split(".")[0]
    TRACE(''.join([
        "do_player_command: AJAX response for url '", sessionContext.url, "'"
    ]))
    if testurl == "tracks":
        return tables.dump_tracks_table(sessionContext,
                                        serverContext.playerPlaylistId,
                                        serverContext.playerPlaylistName,
                                        controls = False)
    elif testurl == "critter":
        return tables.dump_vote_tables(sessionContext, True)
    elif testurl == "player":
        TRACE("do_player_command: player response: tables.dump_player_table")
        return tables.dump_player_table(sessionContext, serverContext)
    else:
        return tables.dump_vote_tables(sessionContext, True)


"""
AJAX request - do_vote
returns one or more html tables, but not an entire page
"""
def do_vote(sessionContext):
    voterId = sessionContext.voterId
    #urldict = parse_qs(sessionContext.environ['QUERY_STRING'])
    TRACE(''.join(["do_vote: env(QUERY_STRING) = '",
          sessionContext.environ['QUERY_STRING'],
          "', urldict = " + str(sessionContext.urldict)
    ]))
    # this is the extra params on the URL after the question mark (?)
    # as is conventional, each param is delimited by the ampersand (&)
    # Returns a dictionary containing lists as values.
    vote    = sessionContext.urldict.get('vote',    [''])
    # Always escape user input to avoid script injection
    vote    = escape(vote)

    trackId = sessionContext.urldict.get('trackId', [''])
    trackId = escape(trackId)

    sessionId = sessionContext.urldict.get('sessionId', [''])
    sessionId = escape(sessionId)

    if ((int(sessionId) < 1)
     or (int(sessionId) != int(sessionContext.sessionId))):
        TRACE(''.join([
            "AUTHORIZATION ERROR: sessionId specified as ", str(sessionId), " "
            "in the URL does not match detected sessionId of ",
            str(sessionContext.sessionId)
        ]))
    if vote == "yae" or vote == "nay":
        if vote == "yae":
            ivote = 1
        elif vote == "nay":
            ivote = 0
        else:
            TRACE("ERROR: critter.py: vote of %s not understood" % str(vote))
            ivote = -1

    # sanity check
    if validate_vote_url(
                vote,
                sessionContext.voterId,
                trackId,
                sessionContext.sessionId):
        TRACE("vote = " + str(sessionContext.urldict))
        cur = sessionContext.con.cursor()
        cur.execute(''.join([
            "SELECT id, trackId, voterId, vote "
            "FROM votes "
            "WHERE "
                "voterId = '", str(sessionContext.voterId), "' AND "
                "trackId = '", str(trackId), "';"
        ]))
        rows = cur.fetchall()
        if len(rows) < 1:
            # no records exist matching the voterId and the trackId
            try:
                sql = ''.join([
                    "INSERT INTO votes ("
                        "voterId, "
                        "trackId, "
                        "vote, "
                        "votedon"
                        ") "
                    "VALUES ('",
                        str(sessionContext.voterId), "', '",
                        str(trackId), "', '",
                        str(ivote), "', '",
                        strftime("%Y-%m-%dT%H:%M:%S"),
                    "');"
                ])
                TRACE("do_vote: sql = '%s'" % sql)
                cur.execute(sql)
            except:
                e = sys.exc_info()[0]
                TRACE("do_vote: Exception running %s: %s"
                      % (str(sys.argv[0]), str(e)))
                print_exc(file=sessionContext.environ['wsgi.errors'])
                return str(sys.exc_info()[0]) + "\n" + format_exc(10)
        else:
            db_vote = str(rows[0]['vote'])
            TRACE(
                "do_vote: str(vote) = %s, str(db_vote)=%s"
                % (str(vote), str(db_vote))
            )
            if str(ivote) == db_vote:
                sql = ''.join([
                  "DELETE FROM votes "
                      "WHERE "
                          "voterId = '", str(sessionContext.voterId), "' AND "
                          "trackId = '", str(trackId), "';"
                ])
                ivote = None
                TRACE(sql)
                cur.execute(sql)
            else:
                sql = ''.join([
                  "UPDATE votes "
                      "SET vote = '", str(ivote), "', "
                          "votedon = '", strftime("%Y-%m-%dT%H:%M:%S"), "' "
                  "WHERE "
                        "voterId = '", str(sessionContext.voterId), "' AND "
                        "trackId = '", str(trackId), "';"
                ])
                TRACE(sql)
                cur.execute(sql)

        sessionContext.con.commit()
        TRACE("sessionContext.url = %s" % sessionContext.url)
        if sessionContext.url == "tracks.wsgi":
            return tables.dump_tracks_table(sessionContext,
                                            sessionContext.playlistId,
                                            sessionContext.playlistName,
                                            controls = False,
                                            select_playlist_dropdown = True)
        elif sessionContext.url == "admin.wsgi":
            return tables.dump_tracks_table(sessionContext,
                                            sessionContext.playlistId,
                                            sessionContext.playlistName,
                                            controls = False,
                                            select_playlist_dropdown = True)
        elif sessionContext.url == "critter.wsgi":
            return tables.dump_tracks_table(sessionContext,
                                            sessionContext.playlistId,
                                            sessionContext.playlistName,
                                            controls = False,
                                            select_playlist_dropdown = True)
        elif sessionContext.url == "player_playlist.wsgi":
            vote_cell_id = "vote_%s_%s" % (str(trackId), str(voterId))
            TRACE("vote_cell_id = %s" % vote_cell_id)
            return tables.dump_vote_cell( sessionContext.sessionId,
                                          trackId,
                                          sessionContext.voterId,
                                          sessionContext.voterId,
                                          str(ivote),
                                          vote_cell_id)
        else:
            return tables.dump_tracks_table(sessionContext,
                                            sessionContext.playlistId,
                                            sessionContext.playlistName,
                                            controls = False,
                                            select_playlist_dropdown = True)
    else:
        TRACE("ERROR: validate_vote_url failed.  "
              "The vote appears to be invalid.  "
              "Not counting.")
        return ("ERROR: validate_vote_url failed.  "
                "The vote appears to be invalid.  "
                "Not counting.")

def do_delete_voter(sessionContext, deletee):
    deleter = sessionContext.voterId
    if deleter == deletee:
        TRACE("WARNING: you are committing suicide! ;)")
        deleter = 0
    sql = ("DELETE FROM voters "
                "WHERE "
                    "id = '" + str(deletee) + "';")
    TRACE(sql)
    sessionContext.con.cursor().execute(sql)
    sessionContext.con.commit()

    testurl = sessionContext.url.split(".")[0]
    TRACE("do_delete_voter: url = " + sessionContext.url)
    if testurl == "voters":
        return tables.dump_voters_table(sessionContext)

def do_disconnect_voter(sessionContext, disconnectee):
    if sessionContext.voterId == disconnectee:
        TRACE("WARNING: you are effectively logging out")
    # set the end time of all active sessions to now
    sql = (''.join([
            "UPDATE sessions "
            "SET endtime = '", strftime("%Y-%m-%dT%H:%M:%S", gmtime()), "' "
            "WHERE "
                "voterId == '", str(disconnectee), "' "
                "AND ( endtime LIKE '' OR endtime IS NULL );"
          ]))
    TRACE(sql)
    sessionContext.con.cursor().execute(sql)
    sessionContext.con.commit()
    testurl = sessionContext.url.split(".")[0]
    TRACE("do_disconnect_voter: url = " + sessionContext.url)
    if testurl == "voters":
        return tables.dump_voters_table(sessionContext)

def do_delete_vote(sessionContext, voteId):

    url = sessionContext.url
    sessionId = sessionContext.sessionId
    voterId = sessionContext.voterId
    playlistId = sessionContext.playlistId

    TRACE("do_delete_vote: "
        "url = " + str(url) + 
        ", sessionId = " + str(sessionContext.sessionId) + 
        ", voterId = " + str(voterId) + 
        ", voteId = " + str(voteId) + 
        ", playlistId = " + str(playlistId))
    sql = ("DELETE FROM votes " + 
                "WHERE "
                    "id = '" + str(voteId) + "';")
    TRACE(sql)
    sessionContext.con.cursor().execute(sql)
    sessionContext.con.commit()

    testurl = url.split(".")[0]
    TRACE("url = " + url)
    if testurl == "tracks":
        return tables.dump_tracks_table(sessionContext,
                                        sessionContext.playlistId,
                                        sessionContext.playlistName,
                                        controls = False)
    elif testurl == "critter":
        return tables.dump_vote_tables(sessionContext, True)
    else:
        return tables.dump_vote_tables(sessionContext, True)



def do_server_command(sessionContext, serverContext, page):
    if page == "player_command.wsgi":
        TRACE("%s is a ajax_server_commands page, "
              "compliant with the ajax.do_player_command interface"
              % page)
        output = do_player_command(sessionContext, serverContext)
    if page == "do_gong.wsgi":
        TRACE("do_server_command: GONG!!!")
        # TODO: send moving gif image!!!
        # ...or animated PNG?
        # ...or a javascript position movement?
        output = send_gong_command(sessionContext)
    elif page == "set_player_playlist.wsgi":
        urlPlaylistId = session.get_int_param_from_url(
                              sessionContext.urldict,
                              'playlistId')
        if urlPlaylistId > 0:
            TRACE("New playlist selected via URL: %d" % urlPlaylistId)
            server.set_player_playlist( sessionContext.con,
                                        sessionContext.playlistId,
                                        sessionContext.voterId)

        TRACE(''.join(["tables.dump_playlists_table: "
              "sessionContext.playlistId = ", str(sessionContext.playlistId), ", ", "serverContext.playerPlaylistId = ", str(serverContext.playerPlaylistId)
        ]))
        output = tables.dump_playlists_table(
                      sessionContext,
                      playlistId=serverContext.playerPlaylistId
                  )
    else:
        output = "<h4>do_server_command: page '%s' not found</h4>" % page
    return output

# obsolete?  (from do_admin_command below)
def do_delete_track(sessionContext, trackId):

    url = sessionContext.url
    sessionId = sessionContext.sessionId
    playlistId = sessionContext.playlistId
    deleter = sessionContext.voterId

    TRACE("do_delete_track: "
        "url = " + str(url) + 
        ", sessionId = " + str(sessionContext.sessionId) + 
        ", trackId = " + str(trackId) + 
        ", playlistId = " + str(playlistId))
    sql = ("DELETE FROM playlist_tracks " + 
                "WHERE "
                    "playlist_id = '" + str(playlistId) + 
                    "' AND track_num = '" + str(trackId) + "';")
    TRACE(sql)
    sessionContext.con.cursor().execute(sql)
    sessionContext.con.commit()

    testurl = url.split(".")[0]
    TRACE("AJAX response for url '" + url + "'")
    if testurl == "tracks":
        return tables.dump_tracks_table(sessionContext,
                                        SessionContext.playlistId,
                                        sessionContext.playlistName,
                                        controls = False)
    elif testurl == "critter":
        return tables.dump_vote_tables(sessionContext, True)
    else:
        return tables.dump_vote_tables(sessionContext, True)



def do_admin_command(sessionContext, serverContext, page):
    __FUNCTION__ = sys._getframe().f_code.co_name
    if not sessionContext.admin:
        TRACE("%s: Permission Denied.  "
              "You are not an administrator."
              % __FUNCTION__)
        output = "<h4>%s(%s): permission denied</h4>" % (__FUNCTION__, page)
        status = '403 Forbidden'
    else:
        if page == "delete_voter.wsgi" or page == "delete_voter.py":
            if 'deletee' in sessionContext.urldict.keys():
                deletee = sessionContext.urldict['deletee']
                if sessionContext.voterId == deletee:
                    TRACE("%s(%s): can't commit suicide" % (__FUNCTION__, page))
                    status = '401 Unauthorized'
                else:
                    output = do_delete_voter(sessionContext, deletee)
                    # TODO: verify operation here?
                    status = '200 OK'
            else:
                TRACE("do_admin_command ERROR: "
                      "deletee not specified in url '%s'"
                      % sessionContext.url)
                status = '400 Bad Request'
        elif page == "delete_track.wsgi" or page == "delete_track.py":
            if 'trackId' in sessionContext.urldict.keys():
                deletee = sessionContext.urldict['trackId']
                output = do_delete_track(sessionContext, trackId)
                status = '200 OK'
            else:
                TRACE("do_admin_command(%s) ERROR: "
                      "trackId not specified in url '%s'"
                      % (page, sessionContext.url))
                status = '400 Bad Request'
        elif page == "delete_vote.wsgi" or page == "delete_vote.py":
            if 'voteId' in sessionContext.urldict.keys():
                deletee = sessionContext.urldict['voteId']
                output = do_delete_vote(sessionContext, voteId)
                status = '200 OK'
            else:
                TRACE("do_admin_command(%s) ERROR: "
                      "voteId not specified in url '%s'"
                      % (page, sessionContext.url))
                status = '400 Bad Request'
        elif page == "disconnect_voter.wsgi":
            if 'voteId' in sessionContext.urldict.keys():
                deletee = sessionContext.urldict['voteId']
                output = do_delete_vote(sessionContext, voteId)
                status = '200 OK'
            else:
                TRACE("do_admin_command(%s) ERROR: "
                      "voteId not specified in url '%s'"
                      % (page, sessionContext.url))
                status = '400 Bad Request'
        else:
            TRACE(
                "%s was not found in the list of ajax admin commands; "
                "page = %s, sessionId = %d, voterId = %d, "
                % (
                    environ['REQUEST_URI'],
                    page,
                    sessionContext.sessionId,
                    sessionContext.voterId
                )
            )
            output = (
                "File Not Found: uri = %s, page = %s, sessionId = %d, voterId = %d"
                % (
                    sessionContext.environ['REQUEST_URI'],
                    page,
                    sessionContext.sessionId,
                    sessionContext.voterId
                )
            )
            status = '404 Page Not Found' 
        content_type = 'text/plain'
    return output
