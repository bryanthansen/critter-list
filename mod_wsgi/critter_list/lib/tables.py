# -- coding: utf-8 --
#!/usr/bin/python3 -u

"""
@brief the menus - file separated due to massive size of the main

@author: Bryant Hansen <github1@bryanthansen.net>
@license: GPLv3

@TODO: xml/xsl should be applied for efficiency

"""

#############################################################
# Imports

import sys
import os
import re
from datetime import datetime, timedelta
import sqlite3
from traceback import format_exc, print_exc

import critter_list.config as config
import critter_list.constants as constants
from critter_list.lib.trace import TRACE
from critter_list.lib.session import SessionContext
from critter_list.lib.server import ServerContext

from pyplayer.lib import mplayer_constants
from pyplayer.lib.mplayer_constants import States
import critter_list.lib.sql as sql
import critter_list.constants as constants

try:
    from io import StringIO
except ImportError:
    from cStringIO import StringIO


#############################################################
# Functions

def html_escape(text):
    """Produce entities within text."""
    html_escape_table = {
        "&": "&amp;",
        '"': "&quot;",
        "'": "&apos;",
        ">": "&gt;",
        "<": "&lt;",
    }
    return "".join(html_escape_table.get(c,c) for c in text)

def format_short_string(strin, maxlen = 48):
    s = strin
    if len(strin) < 1:
        return ""
    elif len(strin) > maxlen:
        delim = " ... "
        l = (maxlen - len(delim)) // 2
        s = strin[0:l] + delim + strin[-l:]
        # s += " title='" + strin + "'"
    return s

"""
Dump a table that shows all of the available playlists, the currently-selected 
playlist and allows to select an alternate playlist
"""
def dump_playlists_select(sessionContext, playlistId, playlistName):
    """
    Dump a select dropdown that shows all of the available playlists, the
    currently-selected playlist and allows to select an alternate playlist
    """
    if playlistId == 0:
        playlistId = sessionContext.playlistId
        playlistName = sessionContext.playlistName
    req = StringIO()
    req.write(
        "<select "
        "name='select_playlists_dropdown' "
        "id='select_playlists_dropdown' "
        "class='select_playlists_dropdown' "
        "onchange='location = this.value;'>\n"
    )
    querystr = sql.playlists_select_query()
    TRACE("dump_votes_table: querystr(type = %s): '%s'"
          % (type(querystr), str(querystr)))
    sessionContext.cur.execute(querystr)
    rows = sessionContext.cur.fetchall()
    if len(rows) < 1:
        req.write(
            "<option>\n"
                "No<br />playlists<br />have<br />been<br />registered\n"
            "</option>\n"
        )
    else:
        for row in rows:
            lPlaylistId = row[0]
            lPlaylistName = row[1]
            selectedPlaylistClass = ""
            if str(playlistId) == str(lPlaylistId):
                selectedPlaylistClass = "class='selectedPlaylistCell' selected='selected'"
            else:
                selectedPlaylistClass = "class='unSelectedPlaylistCell'"
            req.write(''.join([
                "<option ", selectedPlaylistClass,
                " value='", sessionContext.url,
                "?sessionId=", str(sessionContext.sessionId),
                "&playlistId=", str(lPlaylistId),
                "'>\n", lPlaylistName, "\n"
                "</option>\n"
            ]))
    req.write("</select>\n")
    return req.getvalue()

"""
Dump a table that shows all of the available playlists, the currently-selected 
playlist and allows to select an alternate playlist
"""
def dump_playlists_table(sessionContext, playlistId = 0):
  """
  Dump a table that shows all of the available playlists, the
  currently-selected playlist and allows to select an alternate playlist
  """
  if playlistId == 0:
      playlistId = sessionContext.playlistId
  req = StringIO()
  sessionContext.cur.execute("SELECT id, name FROM playlists")
  rows = sessionContext.cur.fetchall()
  if len(rows) < 1:
    return (
      "<table id='table_playlists' class='set_playlist_table'>\n"
        "<tr>\n"
          "<th>\n"
            "No<br />playlists<br />have<br />been<br />registered\n"
          "</th>\n"
        "</tr>\n"
      "</table>\n"
    )
  req.write(
    "<table id='table_playlists' class='set_playlist_table'>\n"
    "<tr>\n"
      "<th class='topheader' colspan='100%'>\n"
        "Select Current Playlist\n"
      "</th>\n"
    "</tr>\n"
    "<tr>\n"
  )
  for col in rows[0].keys():
    req.write(''.join(["<th>", col, "</th>\n"]))
  req.write("<th>tracks</th>\n")
  req.write("<th>votes</th>\n")
  if sessionContext.admin == True:
    req.write("<th>del</th>\n")
  req.write("</tr>\n")
  for row in rows:
    if str(playlistId) == str(row['id']):
      playlistClass = "selectedPlaylistCell"
    else:
      playlistClass = "unSelectedPlaylistCell"
    req.write(''.join([
      "<tr>\n"
        "<td class='id'>", str(row['id']), "</td>\n"
        "<td "
            "class='", playlistClass, "' "
            "onclick='set_player_playlist(",
              str(sessionContext.sessionId), ", ",
              str(row['id']),
            ")' ",
            ">\n",
            row['name'],
        "</td>\n"
        "<td>\n"
          "num tracks"
        "</td>\n"
        "<td>\n"
          "num votes"
        "</td>\n"
    ]))
    if sessionContext.admin == True:
      req.write(''.join([
        "<td class='delete-playlist'>\n"
          "<img "
            "class='delete-playlist' "
            "src='", config.template_path, "/close.button.png' "
            "onclick='delete_playlist(",
              str(sessionContext.sessionId), ", ",
              str(sessionContext.voterId), ", ",
              str(row['name']),
            ")' "
          "/>\n"
        "</td>\n"
      ]))
    req.write("</tr>\n")
  req.write("</table>\n")
  return req.getvalue()

def dump_player_playlist_table(sessionContext, serverContext):
    currentTrackUrl = ""
    req = StringIO()

    conn = sessionContext.con
    # playlistId = sessionContext.playlistId
    playlistId = serverContext.playerPlaylistId
    try:
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
    except:
        e = ("Exception getting cursor: " % str(sys.exc_info()))
        TRACE(e)
        return e
    try:
        fargv = [0]
        fargv[0] = serverContext.playerPlaylistId
        querystr = sql.get_query_string('player_playlist_table', fargv)
        TRACE(querystr)
        cur.execute(querystr)
    except:
        e = sys.exc_info()[0]
        TRACE("Exception running %s: %s" % (str(sys.argv[0]), str(e)))
        print_exc(file=environ['wsgi.errors'])
        return str(sys.exc_info()[0]) + "\n" + format_exc(10)
    rows = cur.fetchall()
    if len(rows) < 1:
        req.write(
            "<table id='table_player_playlist' class='player_playlist_table'>\n"
            "<tr><th class='playlist_header'>No playlists are available</th></tr>\n"
        )
    else:
        TRACE("dump_player_playlist_table: "
              "serverContext.playerPlaylistName = '%s'"
              % serverContext.playerPlaylistName)
        try:
            req.write(
                "<table id='table_player_playlist' class='player_playlist_table'>\n"
                "<tr><th colspan='100%%' class='playlist_header'>Playlist: %s</th></tr>\n"
                "<tr>\n" % serverContext.playerPlaylistName
            )
        except:
            TRACE("exception writing serverContext.playerPlaylistName: %s"
                  % str(sys.exc_info()))
            req.write(
                "<table id='table_player_playlist' class='player_playlist_table'>\n"
                "<tr><th colspan='100%' class='playlist_header'>Playlist: ERROR</th></tr>\n"
                "<tr>\n"
            )
        for col in range(len(rows[0])):
            heading = str(cur.description[col][0])
            heading = heading.replace('_', ' ')
            if sessionContext.narrow:
                if heading == 'title':
                    req.write(
                        "<th class='subheader'>\n"
                            "Title / Artist / Album / Filename\n"
                        "</th>\n"
                    )
                elif heading in [
                        'artist', 'yaes', 'nays', 'album', 'filename']:
                    pass
                else:
                    req.write(
                        "<th class='subheader'>%s</th>\n"
                        % (heading)
                    )
            else:
                req.write(
                    "<th class='subheader'>%s</th>\n"
                    % (heading)
                )
        req.write(
            "<th class='exists'>Exists</th>\n"
            "</tr>\n"
        )
        currentTrackUrl = str(rows[0][6])
        time_since_played = constants.INFINITY
        verbose = False
        for row in rows:
            try:
                time_since_played = constants.INFINITY
                try:
                    if row['time_since_played'] == None:
                      time_since_played = constants.INFINITY
                    else:
                      time_since_played = int(row['time_since_played'])
                    # TRACE("  time_since_played: %d" % time_since_played)
                except:
                    TRACE("failed to get time_since_played from db: %s"
                          % str(sys.exc_info()))
                if time_since_played >= constants.INFINITY:
                    time_since_played_string = 'never'
                else:
                    time_since_played_string = timedelta(
                                                seconds=int(time_since_played))

            except:
                TRACE("Exception %s getting playlist data from the DB "
                      % str(sys.exc_info()))

            now = ""
            last_played_seconds = ""

            if  int(time_since_played) < 0 \
            or (int(time_since_played) == 0 and time_since_played != "0"):
                TRACE(
                    "FAILED TO RETRIEVE time_since_played: %s for file %s"
                    % (time_since_played, str(row['filename']))
                )
            elif int(time_since_played) <= constants.MIN_REPEAT_TIME:
                TRACE(
                    "%s has been played %s seconds ago.  Skipping"
                    % (str(row['title']), time_since_played)
                )
                continue
            else:
              if verbose:
                TRACE(
                    "%s has been played %s seconds ago.  "
                    "That's greater than our limit of %d.  Adding to list."
                    % (str(row['title']),
                       time_since_played,
                       constants.MIN_REPEAT_TIME)
                )
            t = int(time_since_played)
            # TRACE("dump_playler_table: time since played = %d" % t)
            if t >= constants.INFINITY:
                time_since_played_string = 'infinity'
            else:
                time_since_played_string = timedelta(
                                            seconds=int(time_since_played))

            req.write("<tr>\n")
            title = format_short_string(str(row['title']))
            artist = format_short_string(str(row['artist']))
            album = format_short_string(str(row['album']))
            basename = format_short_string(os.path.basename(str(row['filename'])))
            for col in range(len(rows[0])):
                if sessionContext.narrow:
                    if str(cur.description[col][0]) == 'title':
                        req.write(
                            "<td class='title_narrow'>"
                                "<span class='title'>%s</span><br />\n"
                                "&nbsp;&nbsp;%s<br />\n"
                                "&nbsp;&nbsp;%s<br />\n"
                                "&nbsp;&nbsp;%s"
                            "</td>\n" %
                            (title, artist, album, basename)
                        )
                    elif str(cur.description[col][0]) == 'score':
                        req.write("<td class='score'><span class='score'>")
                        if len(str(row['score'])) < 1:
                            req.write("&nbsp;\n")
                        else:
                            req.write(
                                "%s </span><br /> %s yaes <br /> %s nays" 
                                % (str(row['score']),
                                   str(row['yeae']),
                                   str(row['nays'])
                            ))
                        req.write("</td>\n")

                    elif str(cur.description[col][0]) in [
                        'artist', 'album', 'yaes', 'nays', 'filename']:
                            pass
                    elif str(cur.description[col][0]) == 'last_played':
                        if str(row['last_played']) == None:
                            last_played = 'never'
                        if str(last_played) == 'None':
                            last_played = 'never'
                        if last_played != 'never':
                            last_played = str(row['last_played']).replace(
                                                        'T', '<br />', 1
                                                    ) + " GMT"
                        req.write(
                            "<td class='%s'>%s</td>"
                            % (str(cur.description[col][0]), last_played)
                        )
                    elif str(cur.description[col][0]) == 'time_since_played':
                        if time_since_played_string == 'never' \
                        or time_since_played_string == 'infinity':
                            time_since_played_string = "<span class='infinity'>&infin;</span>"
                        else:
                            time_since_played_string = \
                                        str(time_since_played_string) \
                                        + " seconds"
                            time_since_played_string = \
                                        time_since_played_string.replace(
                                            ':', ' hours<br />', 1
                                        )
                            # tip on how to do an efficient, single-pass
                            # multi-string replace:
                            # http://emilics.com/blog/article/multi_replace.html
                            rdict = {
                                    '01 hours<br />': '01 hour<br />',
                                                ':': ' minutes<br />',
                                '01 minutes<br />': '01 minute<br />',
                                '01 seconds<br />': '01 second<br />',
                                                ', ': '<br />',
                                        '<br />0': '<br />'
                            }
                            robj = re.compile('|'.join(rdict.keys()))
                            time_since_played_string = robj.sub (
                                lambda m: rdict[m.group(0)],
                                time_since_played_string
                            )
                        req.write(
                            "<td class='%s'>%s</td>"
                            % (
                                str(cur.description[col][0]),
                                time_since_played_string
                            )
                        )
                    else:
                        req.write(
                            "<td class='%s'>"
                            % str(cur.description[col][0])
                        )
                        s = str(row[col])
                        if len(s) < 1:
                            req.write("&nbsp;\n")
                        else:
                            req.write(s)
                        req.write("</td>\n")
                else:
                    req.write("<td class='%s'>" % cur.description[col][0])
                    if row[col] == None:
                        req.write("&nbsp;\n")
                    else:
                        s = str(row[col])
                        if str(cur.description[col][0]) == 'filename':
                            basename = os.path.basename(s)
                            basename = format_short_string(basename)
                            req.write(basename)
                        else:
                            req.write(s)
                    req.write("</td>\n")
            if (os.path.exists(str(row['filename']))):
                req.write("<td class='exists'>Y</td>\n")
            else:
                TRACE("WARNING: file '%s' does not exist in the filesystem" % filename)
                req.write("<td class='exists'>N</td>\n")
            req.write("</tr>\n")
    req.write("</table>\n")
    return req.getvalue()

def request_player_data(player_obj, param):
    resp = "failed to get " + param
    try:
        resp = str(getattr(player_obj, "get_" + param)())
    except:
        resp = "Exception calling proxy.get_%s()" % param
        TRACE("Exception %s calling proxy.get_%s()"
              % (str(sys.exc_info()), param)
        )
    return resp

class MediaButton:
    # TODO: controls should only be available for an admin
    def __init__(self, command, state, sessionContext):
        self.state = state
        self.css_class = "%s-%s" % (command, state)
        # NOTE: All filenames composed here must match names of actual files on the server.  Where/when/how to verify?  TODO
        self.button_set =     ''.join([
                                  config.template_path,
                                  "/buttons4/",
                                  state,
                                  ".svg"
                              ])
        self.js_function = ''.join([
                              command,"(",
                                  str(sessionContext.sessionId),
                                  ", ",
                                  str(sessionContext.voterId),
                                  ")"
                              ])
    def to_html(self):
        return ''.join([
                "<div class='pushbutton_", self.state, "'>\n"
                "  <img "
                "    class='",   self.css_class,   "' "
                "    src='",     self.button_set,  "' "
                "    onclick='", self.js_function, "' "
                "  />\n"
                "</div>\n"
        ])

def dump_player_table(sessionContext, serverContext):
    state = 0
    try:
        state = int(request_player_data(sessionContext.proxy, "state"))
        ps = mplayer_constants.States(state)
    except:
        ps = mplayer_constants.States(0)
        TRACE("Exception %s requesting or displaying player state"
              % str(sys.exc_info()))

    '''
    To enumerate all params that have responses:
    for param in mplayer_constants.Responses.keys():
    HOWEVER, these must be put in a list if we want control over the order
    '''
    '''
    TODO: get this as a dict in 1 request!
    "get_audio_bitrate"     : "ANS_AUDIO_BITRATE",
    "get_audio_codec"       : "ANS_AUDIO_CODEC",
    "get_audio_samples"     : "ANS_AUDIO_SAMPLES",
    "get_file_name"         : "ANS_FILENAME",
    "get_meta_album"        : "ANS_META_ALBUM",
    "get_meta_artist"       : "ANS_META_ARTIST",
    "get_meta_comment"      : "ANS_META_COMMENT",
    "get_meta_genre"        : "ANS_META_GENRE",
    "get_meta_title"        : "ANS_META_TITLE",
    "get_meta_track"        : "ANS_META_TRACK",
    "get_meta_year"         : "ANS_META_YEAR",
    "get_percent_pos"       : "ANS_PERCENT_POSITION",
    "get_sub_visibility"    : "",
    "get_time_length"       : "ANS_LENGTH",
    "get_time_pos"          : "ANS_TIME_POSITION",
    "get_vo_fullscreen"     : "",
    "get_video_bitrate"     : "ANS_VIDEO_BITRATE",
    "get_video_codec"       : "ANS_VIDEO_CODEC",
    "get_video_resolution"  : "ANS_VIDEO_RESOLUTION",
    "get_property volume"   : "ANS_volume",
    '''

    req = StringIO()
    req.write(
        "<table class='player_interface'>\n"
        "<tr>\n"
        "<td class='player_state'>\n"
        "Player State-of-Being: %s\n"
        "<table class='player_state'>\n"
        "<tr>\n"
        % str(ps)
    )

    if sessionContext.proxy == None:
        req.write(
            "<td>\n"
            "<strong>No Access to Player</strong>\n"
            "</td></tr></table>\n"
        )
    else:
        if (state == States.STOPPED) or (state == States.TRACK_COMPLETE) or (state == States.MPLAYER_NOT_RUNNING):
            req.write("<tr><th class='next_up' colspan='2'>Next Up:</th></tr>\n")
            track = sql.get_next_track(sessionContext.cur, serverContext.playerPlaylistId)
            datafound = False
            if len(str('title')) == 0 and len(str('artist')) == 0 and len(str('album')) == 0:
                try:
                    filename = str(track['filename'])
                    req.write("<tr><td class='next_up' colspan='2'>%s</td></tr>\n" % filename)
                except:
                    req.write("<tr><td class='next_up' colspan='2'>Exception getting next-up field 'filename' from server</td></tr>\n")
                    TRACE("Exception %s getting next_track data for field 'filename'" % (str(sys.exc_info())))
            for field in ['title', 'artist', 'album', 'score', 'yaes', 'nays', 'last_played' ]:
                try:
                    value = str(track[field])
                    if len(value) > 0:
                        req.write("<tr><td class='next_up' colspan='2'>%s: %s</td></tr>\n" % (field, value))
                except:
                    req.write("<tr><td class='next_up' colspan='2'>Exception getting next-up field '%s' from server</td></tr>\n" % field)
                    TRACE("Exception %s getting next_track data for field '%s'" % (str(sys.exc_info()), str(field)))
        else:
            params = [
                        "get_meta_title",
                        "get_meta_album",
                        "get_meta_artist",
                        "get_file_name",
                      ]
            for param in params:
                param = param.replace("get_", "")
                TRACE("requesting param %s" % param)
                try:
                    req.write("<tr><td class='player_playing' colspan='2'>%s</td></tr>\n"
                            % (request_player_data(sessionContext.proxy, param)))
                except:
                    TRACE("Exception %s requesting or displaying player data "
                          "for param %s"
                          % (sys.exc_info(), str(param)))

            params = [
                        "get_meta_year",
                        "get_meta_track",
                        "get_meta_genre",
                        "get_meta_comment",
                        "get_audio_bitrate",
                        "get_audio_samples",
                        "get_percent_pos",
                        "get_time_length",
                        "get_time_pos",
                        "get_sub_visibility",
                        "get_vo_fullscreen",
                        "get_audio_codec",
                        "get_video_codec",
                        "get_video_bitrate",
                        "get_video_resolution",
                        "get_property volume",
            ]
            for param in params:
                param = param.replace("get_", "")
                TRACE("requesting param %s" % param)
                try:
                    req.write("<tr><td class='label'>%s</td><td>%s</td></tr>\n"
                            % (param,
                                request_player_data(sessionContext.proxy, param)))
                except:
                    TRACE("Exception %s requesting or displaying player data "
                          "for param %s"
                          % (sys.exc_info(), str(param)))
        req.write(
            "</table>\n"
        )

    '''
    file_name = request_player_data(sessionContext.proxy, "file_name")
    title = request_player_data(sessionContext.proxy, "title")
    artist = request_player_data(sessionContext.proxy, "artist")
    album = request_player_data(sessionContext.proxy, "album")
        % (str(state), str(title), str(artist), str(album), file_name)
    '''

    stop_state = 'disabled'
    play_state = 'disabled'
    pause_state = 'disabled'
    ff_state = 'disabled'
    rewind_state = 'disabled'
    previous_state = 'disabled'
    next_state = 'disabled'
    if (state == States.STOPPED) or (state == States.TRACK_COMPLETE) or (state == States.MPLAYER_NOT_RUNNING):
        stop_state = 'active'
        play_state = 'enabled'
    elif state == States.PLAYING:
        play_state = 'active'
        stop_state = 'enabled'
        pause_state = 'enabled'
        ff_state = 'enabled'
        rewind_state = 'enabled'
        previous_state = 'enabled'
        next_state = 'enabled'
    elif state == States.PAUSED:
        play_state = 'enabled'
        stop_state = 'enabled'
        pause_state = 'active'
        ff_state = 'enabled'
        rewind_state = 'enabled'
        previous_state = 'enabled'
        next_state = 'enabled'
    else:
      TRACE("ERROR: failed to identify the player state (state = %d)" % state)

    return ''.join([
      req.getvalue(),
      "</td>\n"
      "<td class='slider' rowspan='2'>\n",
        "<div class='slider'>\n"
          "  <img "
          "    class='volume-slider-active' "
          "    src='", config.template_path, "/buttons4/enabled.svg'"
          "    onclick='", "do_something()" , "' "
          "  />\n"
        "</div>\n"
      "</td>\n"
      "<td class='slider' rowspan='2'>\n"
      "</td>\n"
      "</tr>\n"
      "<tr>\n"
        "<td class='controls'>\n",
            MediaButton('stop',     stop_state,     sessionContext).to_html(),
            MediaButton('pause',    pause_state,    sessionContext).to_html(),
            MediaButton('play',     play_state,     sessionContext).to_html(),
            MediaButton('rewind',   rewind_state,   sessionContext).to_html(),
            MediaButton('ff',       ff_state,       sessionContext).to_html(),
            MediaButton('previous', previous_state, sessionContext).to_html(),
            MediaButton('next',     next_state,     sessionContext).to_html(),
        "</td>\n"
      "</tr>\n"
      "<tr>\n"
      "<td class='progress' colspan='100%'>\n"
        "14:09 / 25:10"
      "</td>\n"
      "</tr>\n"
      "</table>\n"
    ])

def dump_player2_table(sessionContext):
  return ''.join([
    "<table id='table_player_example' class='player2'>\n"
      "<tr>\n"
        "<td class='player_graphic'>\n",
          "<img "
            "class='controls' "
            "src='", config.template_path, "/examples/ui-stereo-10.png' "
            "alt='PL' ",
            "onclick='play(%d, %d)' " % (sessionContext.sessionId, sessionContext.voterId),
          "/>\n"
        "</td>\n"
      "<td class='slider-concept' rowspan='2'>\n"
        "<img "
          "class='slider-concept' "
          "alt='volume1' "
          "src='", config.template_path, os.sep, "slider", os.sep, "13780468461885975955Audio_Fader_Slider.svg' "
        "/>\n"
      "</td>\n"
      "<td class='slider' rowspan='2'>\n"
        "<img "
          "class='slider-concept' "
          "alt='volume1' "
          "src='", config.template_path, os.sep, "slider", os.sep, "13780468461885975955Audio_Fader_Slider.svg' "
        "/>\n"
      "</td>\n"
      "</tr>\n"
    "</table>\n"
  ])

def dump_playlist_table(sessionContext):
    TRACE("enter dump_playlist_table (playlistId = %d)" % sessionContext.playlistId)

    conn = sessionContext.con
    playlistId = sessionContext.playlistId

    try:
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
    except:
        TRACE("Exception %s getting cursor " % (sys.exc_info()))
        return "Exception %s getting cursor " % (sys.exc_info())

    req = StringIO()
    req.write(''.join([
            "<table id='table_playlist' class='db_table'>\n"
                "<tr>\n"
                    "<th class='topheader' colspan='100%%'>",
                        dump_playlists_select(
                              sessionContext,
                              sessionContext.playlistId,
                              sessionContext.playlistName),
                    "</th>\n"
                "</tr>\n"
    ]))

    # major query: this is where the magic happens
    # this retreives the tally, including the score/ranking of every song
    # Note: the playlistHistory join corrupts the COUNT
    # This may need to be a sub-query
    sql.playlist_query(cur, playlistId)

    time_since_played_string = 'never'
    rows = cur.fetchall()
    if len(rows) < 1:
        req.write(
          "<tr><th>No playlists entries found for playlist %d</th></tr>\n"
          % playlistId
        )
    else:
        # TODO: list playlists
        req.write("<tr>\n")
        for col in range(len(rows[0])):
            heading = str(cur.description[col][0])
            heading = heading.replace('_', ' ')
            if sessionContext.narrow:
                if heading == 'title':
                    req.write(
                        "<th class='subheader'>"
                            "Title / Artist / Album / Filename"
                        "</th>\n"
                    )
                elif heading in ['artist', 'yaes', 'nays', 'album', 'filename']:
                    pass
                else:
                    req.write("<th class='subheader'>%s</th>\n" % (heading))
            else:
                req.write("<th class='subheader'>%s</th>\n" % (heading))
        req.write("</tr>\n")

        TRACE("getting current Track URL")
        currentTrackUrl = str(rows[0]['filename'])
        TRACE("Current Track URL: %s" % str(currentTrackUrl))
        time_since_played_string = 'never'
        for row in rows:
            try:
                score = str(row['score'])
                yaes = str(row['yaes'])
                nays = str(row['nays'])
                title = str(row['title'])
                artist = str(row['artist'])
                album = str(row['album'])
                filename = str(row['filename'])
                last_played = str(row['last_played'])

                if not 'time_since_played' in row.keys():
                    TRACE("ERROR: query results do not contain element "
                          "time_since_played: %d" % time_since_played)
                else:
                    time_since_played = constants.INFINITY
                    try:
                        time_since_played = int(row['time_since_played'])
                        # TRACE("  time_since_played: %d" % time_since_played)
                    except:
                        #TRACE("failed to get time_since_played from db: %s"
                        #    % str(sys.exc_info()))
                        pass
                    if time_since_played >= constants.INFINITY:
                        time_since_played_string = 'never'
                    else:
                        time_since_played_string = timedelta(
                                                seconds=int(time_since_played))
            except:
                TRACE("Exception %s getting playlist data from the DB "
                      % str(sys.exc_info()))

            req.write("<tr>\n")
            title = format_short_string(title)
            artist = format_short_string(artist)
            album = format_short_string(album)
            filename = format_short_string(os.path.basename(filename))
            for col in range(len(rows[0])):
                if sessionContext.narrow:
                    if str(cur.description[col][0]) == 'title':
                        req.write(
                            "<td class='title_narrow'>"
                                "<span class='title'>%s</span><br />\n"
                                "&nbsp;&nbsp;%s<br />\n"
                                "&nbsp;&nbsp;%s<br />\n"
                                "&nbsp;&nbsp;%s"
                            "</td>\n" %
                            (title, artist, album, filename)
                        )
                    elif str(cur.description[col][0]) == 'score':
                        req.write("<td class='score'><span class='score'>")
                        if len(score) < 1:
                            req.write("&nbsp;\n")
                        else:
                            req.write(
                                "%s </span><br /> %s yaes <br /> %s nays" 
                                % (score, yaes, nays)
                            )
                        req.write("</td>\n")
                    elif str(cur.description[col][0]) in [
                        'artist', 'album', 'yaes', 'nays', 'filename']:
                            pass
                    elif str(cur.description[col][0]) == 'last_played':
                        if last_played == None:
                            last_played = 'never'
                        if str(last_played) == 'None':
                            last_played = 'never'
                        if last_played != 'never':
                            last_played = last_played.replace(
                                                        'T', '<br />', 1
                                                    ) + " GMT"
                        req.write(
                            "<td class='%s'>%s</td>"
                            % (str(cur.description[col][0]), last_played)
                        )
                    elif str(cur.description[col][0]) == 'time_since_played':
                        if time_since_played_string != 'never':
                            time_since_played_string = \
                                        str(time_since_played_string) \
                                        + " seconds"
                            time_since_played_string = \
                                        time_since_played_string.replace(
                                            ':', ' hours<br />', 1
                                        )
                        else:
                            time_since_played_string = "infinity"
                        # tip on how to do an efficient, single-pass
                        # multi-string replace:
                        # http://emilics.com/blog/article/multi_replace.html
                        rdict = {
                                '01 hours<br />': '01 hour<br />',
                                            ':': ' minutes<br />',
                            '01 minutes<br />': '01 minute<br />',
                            '01 seconds<br />': '01 second<br />',
                                            ', ': '<br />',
                                    '<br />0': '<br />'
                        }
                        robj = re.compile('|'.join(rdict.keys()))
                        time_since_played_string = robj.sub (
                            lambda m: rdict[m.group(0)],
                            time_since_played_string
                        )
                        req.write(
                            "<td class='%s'>%s</td>"
                            % (
                                str(cur.description[col][0]),
                                time_since_played_string
                            )
                        )
                    else:
                        req.write(
                            "<td class='%s'>"
                            % str(cur.description[col][0])
                        )
                        s = str(row[col])
                        if len(s) < 1:
                            req.write("&nbsp;\n")
                        else:
                            req.write(s)
                        req.write("</td>\n")
                else:
                    req.write("<td class='%s'>" % cur.description[col][0])
                    if row[col] == None:
                        req.write("&nbsp;\n")
                    else:
                        s = str(row[col])
                        if str(cur.description[col][0]) == 'filename':
                            filename = os.path.basename(s)
                            filename = format_short_string(filename)
                            req.write(filename)
                        else:
                            req.write(s)
                    req.write("</td>\n")
            req.write("</tr>\n")
    req.write("</table>\n")
    return req.getvalue()


def dump_pyplayer_config_table(sessionContext):

    from inspect import getmembers, ismethod, isfunction, isroutine, isbuiltin
    import pyplayer.config

    req = StringIO()
    req.write(
        "<table id='table_pyplayer_config' class='settings_table'>\n"
        "<tr>\n"
        "   <th class='topheader' colspan='100%'>\n"
        "       Pyplayer Config\n"
        "   </th>\n"
        "</tr>\n"
        "<tr>\n"
        "   <th>Name</th><th>Value</th>\n"
        "</tr>\n"
    )
    for f in getmembers(pyplayer.config):
        if f[1] is None: continue
        name = str(f[0])
        val = str(f[1])
        if name == '__builtins__': continue
        if name == 'authkey': val = "*****"
        #if not ismethod(f[1]): continue
        if name[0] == '_': continue

        # replace special chars in val by HTML equivalent
        val = val.replace("<", "&lt;")
        val = val.replace(">", "&gt;")

        TRACE("config member: %s, val2=%s" % (str(f), val))
        req.write(
            "<tr>\n"
            "<td class='setting_name'>%s</td>\n"
            "<td class='setting_value'>%s</td>\n"
            "</tr>\n"
            % (name, val)
        )
    req.write(
        "</table>\n"
    )
    return req.getvalue()

def dump_session_context_table(sessionContext):
    TRACE("enter dump_session_context_table")
    from inspect import getmembers, ismethod, isfunction, isroutine, isbuiltin
    import pyplayer.config
    key_fields = [
                    'admin',
                    'clientHeight',
                    'clientWidth',
                    'sessionId',
                    'narrow',
                    'playlistId',
                    'playlistName',
                    'record',
                    'sessionId',
                    'show_debug',
                    'url',
                    'urldict',
                    'voterId',
                    'voterName'
    ]
    '''
                    'environ',
                    'DB_FILE',
                    'DIR',
                    'LOGDIR',
                    'LOGFILE',
                    'SQL_LOGFILE',
                    'critter_list_web_root',
                    'css_path',
                    'datetime',
                    'defaultClientHeigh',
                    'defaultClientWidth',
                    'default_playlistId',
                    'default_sessionId',
                    'default_url',
                    'default_voterId',
                    'javascript_path',
                    'now',
                    'template_path',
                    'wsgi_path'
    '''
    req = StringIO()
    req.write(
        "<table id='table_session' class='settings_table'>\n"
        "<tr>\n"
        "   <th class='topheader' colspan='100%'>\n"
        "       sessionContext\n"
        "   </th>\n"
        "</tr>\n"
        "<tr>\n"
        "   <th>Name</th><th>Value</th>\n"
        "</tr>\n"
    )

    for name in key_fields:
        if name in sessionContext.__dict__.keys():
            val = str(sessionContext.__dict__[name])
            if name == '__builtins__': continue
            val = val.replace("<", "&lt;")
            val = val.replace(">", "&gt;")
            TRACE("config member: %s, val3=%s" % (name, val))
            req.write(
                "<tr>\n"
                "<td class='setting_name'>%s</td>\n"
                "<td class='setting_value'>%s</td>\n"
                "</tr>\n"
                % (name, val)
            )
    for f in getmembers(config):
        if f[1] is None:
            continue
        name = str(f[0])
        if name in key_fields:
            continue
        val = str(f[1])
        if name == '__builtins__': continue
        #if not ismethod(f[1]): continue
        if name[0] == '_': continue

        # consider *not* showing __dict__; it's probably redundant
        if name == 'environ' or name == '__dict__':
            val = val.replace(",", "<br />\n")

        # replace special chars in val by HTML equivalent
        val = val.replace("<", "&lt;")
        val = val.replace(">", "&gt;")

        TRACE("config member: %s, val1=%s" % (str(f), val))
        req.write(
            "<tr>\n"
            "<td class='setting_name'>%s</td>\n"
            "<td class='setting_value'>%s</td>\n"
            "</tr>\n"
            % (name, val)
        )
    req.write(
        "</table>\n"
    )
    return req.getvalue()

def dump_settings_table(sessionContext):
    """
    @TODO enumerate settings out of the config and contants file
    """


    """
    for setting in inspect(config.settings)
        ...
    for setting in inspect(pyplayer.config.settings)
        ...
    """

    from inspect import getmembers, ismethod, isfunction, isroutine, isbuiltin
    import pyplayer.config

    req = StringIO()
    req.write(
        "<table id='table_config' class='settings_table'>\n"
        "<tr>\n"
        "  <th class='topheader' colspan='100%%'>\n"
        "     Config\n"
        "  </th>\n"
        "</tr>\n"
        "<tr>\n"
        "   <th>Name</th><th>Value</th>\n"
        "</tr>\n"
    )
    for f in getmembers(config):
        if f[1] is None: continue
        name = str(f[0])
        val = str(f[1])
        if name == '__builtins__': continue
        #if not ismethod(f[1]): continue
        if name[0] == '_': continue

        # replace special chars in val by HTML equivalent
        val = val.replace("<", "&lt;")
        val = val.replace(">", "&gt;")

        TRACE("config member: %s, val1=%s" % (str(f), val))
        req.write(
            "<tr>\n"
            "<td class='setting_name'>%s</td>\n"
            "<td class='setting_value'>%s</td>\n"
            "</tr>\n"
            % (name, val)
        )
    req.write(
        "</table>\n"
    )
    return req.getvalue()

def dump_voters_table(sessionContext):
  if not sessionContext.admin:
    raise "Permission Denied Exception"
  rows = sql.voters_query(sessionContext.cur)
  if len(rows) < 1:
    return (
      "<table id='table_voters' class='db_table'>\n"
        "<tr>\n"
          "<th>\n"
            "No<br />\n"
            "voters<br />\n"
            "have<br />\n"
            "been<br />\n"
            "registered\n"
          "</th>\n"
        "</tr>\n"
      "</table>\n"
    )
  req = StringIO()
  req.write(
    "<table id='table_voters' class='db_table'>\n"
      "<tr>\n"
        "<th class='topheader' colspan='100%'>Voters</th>\n"
      "</tr>\n"
    "<tr>"
  )
  # TODO: this string could probably be created with some kind of transform
  #       instead of a clumbsy loop
  req.write("<th>access</th>\n")
  for col in rows[0].keys():
    req.write("<th>" + col + "</th>\n")
  access = "voter"
  if sessionContext.admin:
    access = "admin"
    req.write("<th>cut</th>\n")
    req.write("<th>del</th>\n")
  req.write("</tr>")
  for row in rows:
      TRACE("dump_voters_table: type(row['voterId']) = '%s'"
            % str(type(row['voterId'])))
      selectedVoterClass = ""
      if sessionContext.voterId == row['voterId']:
        selectedVoterClass = "class='voterCell'"
      else:
        selectedVoterClass = "class='unSelectedVoterCell'"
      req.write(''.join([
        "<tr>\n"
          "<td class='", access, "'>", access, "</td>\n"
          "<td class='id'>%03d</td>\n" % int(row['voterId']),
          "<td ", selectedVoterClass, ">"
            "<a "
              "href='", sessionContext.url, "?voterId=", str(row['voterId']),
            "'>",
              str(row['voterName']),
            "</a>"
          "</td>\n"
          "<td class='voter_num_votes'>",
            str(row['votes']),
          "</td>\n"
          "<td class='voter_num_sessions'>",
            str(row['sessions']),
          "</td>\n"
          "<td class='started_session'>",
            str(row['started']),
          "</td>\n"
      ]))
      # TODO: should not need voterId in the ajax javascript calls since this
      # info is contained with in the sessionId; lVoterId is, of course,
      # necessary
      if sessionContext.admin == True:
        disconnect_img = "disconnect2d.svg"
        req.write(''.join([
          "<td class='disconnect-voter'>\n"
            "<a class='disconnect-voter' "
              "onclick='disconnect_voter(",
                str(sessionContext.sessionId), ", ",
                str(sessionContext.voterId), ", ",
                str(row['voterId']), ")'>\n"
              "<img "
                "class='disconnect-voter' "
                "src='", config.template_path, "/", disconnect_img, "' "
              "/>\n"
            "</a>\n"
          "</td>\n"
          "<td class='delete-voter'>\n"
              "<a class='delete-voter' "
                "onclick='delete_voter(",
                  str(sessionContext.sessionId), ", ",
                  str(row['voterId']),
              ")'>\n"
                "<img "
                  "class='delete-voter' "
                  "src='", config.template_path, "/close.button.png' "
                  "onclick='delete_voter(",
                    str(sessionContext.sessionId), ", ",
                    str(row['voterId']),
                ")' />\n"
              "</a>\n"
          "</td>\n"
        ]))
      req.write("</tr>\n")
  req.write("</table>\n")
  return req.getvalue()

def dump_vote_cell(sessionId, trackId, voterId, trackvoterId, vote, vote_cell_id):
    TRACE("enter tables.dump_vote_cell: trackId = %s" % str(trackId))
    vote_yae_class = "vote"
    vote_nay_class = "vote"
    if str(trackvoterId) == str(voterId):
        if vote == "1" or vote == "yae":
            vote_yae_class = "votedyae"
        if vote == "0" or vote == "nay":
            vote_nay_class = "votednay"
    vote_yae_function = (
        "vote_yae(%s, %s, \"%s\")"
        % (sessionId, trackId, vote_cell_id)
    )
    vote_nay_function = (
        "vote_nay(%s, %s, \"%s\")"
        % (sessionId, trackId, vote_cell_id)
    )
    vote_yae_img = "thumbs_up.png"
    vote_nay_img = "thumbs_down2.png"
    output = ''.join([
      "<table><tr>\n"
      "<td class='", vote_yae_class, "'>\n"
        "<a class='", vote_yae_class, "' onclick='", vote_yae_function, "'> \n"
          "<img src='", config.template_path, "/", vote_yae_img, "' />\n"
        "</a>\n"
      "</td><td class='", vote_nay_class, "'>\n"
        "<a class='", vote_nay_class, "' onclick='", vote_nay_function, "'> \n"
          "<img src='", config.template_path, "/", vote_nay_img, "' />\n"
        "</a>\n"
      "</td>\n"
      "</tr></table>\n"
    ])
    TRACE("dump_vote_cell output: \n%s" % output)
    return output

def dump_tracks_table(sessionContext,
                      playlistId,
                      playlistName,
                      controls = False,
                      playlist_dropdown = False):
    """
    @TODO random order
    """

    sessionId = sessionContext.sessionId
    voterId = sessionContext.voterId
    narrow = sessionContext.narrow
    currentTrackUrl = ""

    req = StringIO()
    req.write("<table id='table_tracks' class='db_table'>\n")

    try:
        sessionContext.con.row_factory = sqlite3.Row
    except:
        e = ("Exception getting cursor: " % str(sys.exc_info()))
        TRACE(e)
        return e

    try:
        querystr = sql.vote_tracks_query(sessionContext.voterId, playlistId)
        TRACE(querystr)
        sessionContext.cur.execute(querystr)
    except:
        e = sys.exc_info()[0]
        TRACE("Exception running %s: %s" % (str(sys.argv[0]), str(e)))
        print_exc(file=sessionContext.env['wsgi.errors'])
        return str(sys.exc_info()[0]) + "\n" + format_exc(10)

    TRACE(
        "dump_tracks_table: "
        "voterId = %d, playlistId = %d, table headings: '%s'"
        % (voterId, playlistId, str())
    )
    if (voterId == 0):
        req.write(
            "<tr>"
                "<td>"
                    "ERROR: voterId is null.<br />"
                    "Vote selection is dependent on voterId."
                "</td>"
            "</tr>\n"
        )
    else:
        col_list = list(list(zip(*sessionContext.cur.description))[0])
        rows = sessionContext.cur.fetchall()
        TRACE("dump_tracks_table: col_list = '%s', number of rows: %d"
              % (str(col_list), len(rows)))
    if len(rows) < 1:
        req.write("<tr><th>No tracks are available</th></tr>\n")
    else:
        if sessionContext.narrow:
            req.write(''.join([
                "<tr><th colspan='100%%' class='playlist_header'>"
                "Playlist: ", playlistName, "</th></tr>\n"
                "<tr>\n"
                "<th class='subheader'>Title /\n"
                "Artist /\n"
                "Album /\n"
                "Filename</th>\n"
            ]))
        else:
            req.write(''.join([
                "<tr><th colspan='100%%' class='playlist_header'>"
                "Playlist: ", playlistName, "</th></tr>\n"
                "<tr>\n"
                "<th>Title</th>\n"
                "<th>Artist</th>\n"
                "<th>Album</th>\n"
                "<th>filename</th>\n"
            ]))
        if sessionContext.admin:
            req.write(
                "<th class='exists'>Exists</th>\n"
                "<th class='del'>del</th>\n"
            )
        else:
            req.write(
                "<th class='exists'>Exists</th>\n"
                "<th class='subheader'>Votes</th>\n"
            )
        if controls:
            req.write(
                "<th class='controls'>Controls</th>\n"
            )
        req.write(
            "</tr>\n"
        )
        # TODO: switch to row_factory methods and dump this array shit
        
        #ALTERNATIVE!!!
        # >>> dict([(list(list(zip(*sessionContext.cur.description))[0])[n], n) for n in range(len(sessionContext.cur.description))])
        # {'vote': 3, 'voterId': 2, 'filename': 1, 'artist': 5, 'album': 6, 'title': 4, 'id': 0}
        # >>> rows[1][row_lookup['artist']]
        # 'Feindflug'
        # TODO: figure out if there's a way to eliminate the 'for' statement
        
        currentTrackUrl = str(rows[0][1])
        for row in rows:

            # col_list = '['score', 'yaes', 'nays', 'title', 'artist', 'album', 'filename', 'last_played', 'time_since_played']'
            '''
            score = str(row[0])
            yaes = str(row[1])
            nays = str(row[2])
            title = str(row[3])
            artist = str(row[4])
            album = str(row[5])
            filename = str(row[6])
            last_played = str(row[7])
            time_since_played = str(row[8])
            '''

            trackId = str(row[0])
            filename = str(row[1])
            trackvoterId = str(row[2])
            vote = str(row[3])
            title = str(row[4])
            artist = str(row[5])
            album = str(row[6])
            basename = os.path.basename(filename)
            if len(title) < 1:
                title = basename
            if len(artist) < 1:
                artist = "&lt;artist unknown&gt;"
            if len(album) < 1:
                album = "&lt;album unknown&gt;"
            if len(basename) > 48:
                basename = basename[1:24] + " ... " + basename[-24:]

            if sessionContext.narrow:
                req.write(
                    "<tr>"
                    "<td class='track_info'>"
                    "<span class='title'>%s</span><br />\n"
                    "&nbsp;&nbsp;%s<br />\n"
                    "&nbsp;&nbsp;%s<br />\n"
                    "&nbsp;&nbsp;%s</td>\n" %
                    (title, artist, album, basename)
                )
            else:
                req.write(
                    "<tr>"
                    "<td class='track_info'>"
                    "<span class='title'>%s</span>"
                    "</td>\n"
                    "<td>%s</td>\n"
                    "<td>%s</td>\n"
                    "<td>%s</td>\n" %
                    (title, artist, album, basename)
                )
            if (os.path.exists(filename)):
                req.write("<td class='exists'>Y</td>\n")
            else:
                req.write("<td class='exists'>N</td>\n")
            if sessionContext.admin:
                req.write(
                    "<td class='delete_track'>\n"
                        "<img "
                            "class='delete_track' "
                            "src='%s/close.button.png' "
                            "onclick='delete_track(%s, %s, %s)' "
                        "/>\n"
                    "</td>\n" 
                    % (
                        config.template_path,
                        sessionContext.sessionId,
                        voterId,
                        trackId
                    )
                )
            else:
                vote_cell_id = "vote_%s_%s" % (str(trackId), str(voterId))
                req.write(''.join([
                    "<td id='", vote_cell_id, "' class='vote_class'>\n",
                        dump_vote_cell(
                              sessionContext.sessionId,
                              trackId,
                              voterId,
                              trackvoterId,
                              vote,
                              vote_cell_id),
                    "</td>\n"
                ]))
            if controls:
                req.write(
                    "<td class='controls'>\n"
                        "<img "
                            "class='controls' "
                            "src='%s/play.png' "
                            "alt='PL' "
                            "onclick='play(%d, %d)' "
                        "/>\n" 
                    "</td>\n"
                    % (
                        config.template_path,
                        sessionContext.sessionId,
                        sessionContext.voterId
                    )
                )
            req.write("</tr>\n")
    req.write("</table>\n")
    return req.getvalue()

def dump_sessions_table(cur, admin = False):
    req = StringIO()
    req.write("<table id='table_sessions' class='db_table'>\n")
    try:
        querystr = sql.sessions_query()
        TRACE(querystr)
        cur.execute(querystr)
    except:
        e = sys.exc_info()[0]
        TRACE("Exception running %s: %s" % (str(sys.argv[0]), str(e)))
        print_exc(file=sessionContext.env['wsgi.errors'])
        return str(sys.exc_info()[0]) + "\n" + format_exc(10)
    rows = cur.fetchall()
    if len(rows) < 1:
        req.write(
            "<tr><th class='topheader'>Sessions</th></tr>\n"
            "<tr>"
                "<td>No<br />sessions<br />have<br />been<br />registered</td>"
            "</tr>\n"
        )
    else:
        TRACE("dump_sessions_table: %d sessions found" % rows.__len__())
        cols = len(rows[0])
        req.write("<tr><th colspan='%d'>Last 5 Sessions</th></tr>\n" % cols)
        req.write("<tr>\n")
        for col in range(cols):
            req.write("<th>" + str(cur.description[col][0]) + "</th>\n")
        req.write("</tr>\n")
        for row in rows:
            req.write("<tr>\n")
            for col in range(cols):
                a = str(cur.description[col][0])
                #TRACE("type row[col] = %s" % str(type(row[col])))
                b = str(row[col])
                req.write("<td class='%s'>%s</td>\n"
                    % (a, b)
                )
            req.write("</tr>\n")
    req.write("</table>\n")
    return req.getvalue()

def dump_swipe_table(sessionContext):
    sessionContext.admin = False
    return ''.join([
        "<h1>This will be the SWIPE page</h1>"
    ])

def dump_votes_table(sessionContext):
    cur = sessionContext.cur
    req = StringIO()
    req.write("<table id='table_votes' class='db_table'>\n")
    querystr = sql.votes_query(sessionContext.playlistId)
    TRACE("dump_votes_table: querystr(type = %s): '%s'"
          % (type(querystr), str(querystr)))
    cur.execute(querystr)
    rows = cur.fetchall()
    if len(rows) < 1:
        req.write(
            "<tr>\n"
                "<th>No<br />votes<br />have<br />been<br />registered</th>\n"
            "</tr>\n"
        )
    else:
        cols = len(rows[0])
        req.write(
            "<tr><th class='topheader' colspan='100%'>Votes</th></tr>\n"
            "<tr>\n"
        )
        if sessionContext.narrow:
            for col in range(1, cols):
                colname = str(cur.description[col][0])
                if colname == 'title':
                    req.write(
                        "<th class='subheader'>Title /\n"
                        "Artist /\n"
                        "Album /\n"
                        "Filename</th>\n"
                    )
                elif       colname == 'artist' \
                        or colname == 'album' \
                        or colname == 'filename':
                    pass
                else:
                    req.write("<th class='subheader'>%s</th>\n" % colname)
        else:
            for col in range(1, cols):
                req.write(
                    "<th class='subheader'>%s</th>\n"
                    % str(cur.description[col][0])
                )
        if sessionContext.admin:
            req.write("<th>del</th>\n")
        req.write("</tr>\n")
        for row in rows:
            vote = row[1]
            title = row[2]
            artist = row[3]
            album = row[4]
            votername = row[5]
            datetime = row[6]
            filename = row[7]

            if str(vote) == '1' or str(vote) == 'yae':
                req.write("<tr class='yaevote'>\n")
            elif str(vote) == '0' or str(vote) == 'nay':
                req.write("<tr class='nayvote'>\n")
            else:
                req.write("<tr>\n")

            if len(title) < 1:
                title = filename
            if len(artist) < 1:
                artist = "&lt;artist unknown&gt;"
            if len(album) < 1:
                album = "&lt;album unknown&gt;"

            filename = str(filename)
            filename = os.path.basename(filename)
            if len(filename) > 48:
                filename = filename[1:24] + " ... " + filename[-24:]

            for col in range(1, len(rows[0])):
                colname = str(cur.description[col][0])
                if row[col] == None:
                    req.write("<td>&nbsp;\n")
                else:
                    if colname == 'filename':
                        if not sessionContext.narrow:
                            req.write("<td>%s" % filename)
                    elif colname == 'title':
                        title = str(title)
                        title = os.path.basename(title)
                        if len(title) > 48:
                            title = title[1:24] + " ... " + title[-24:]
                        if sessionContext.narrow:
                            req.write(
                                "<td class='title'>\n"
                                "<span class='title'>%s</span><br />\n"
                                "&nbsp;&nbsp;%s<br />\n"
                                "&nbsp;&nbsp;%s<br />\n"
                                "&nbsp;&nbsp;%s<br />\n"
                                "</td>\n" %
                                (title, artist, album, filename)
                            )
                        else:
                            req.write("<td class='%s'>%s" 
                                % (cur.description[col][0], title))
                    elif colname == 'artist':
                        if not sessionContext.narrow:
                            req.write("<td>%s" % artist)
                    elif colname == 'album':
                        if not sessionContext.narrow:
                            req.write("<td>%s" % album)
                    elif colname == 'votedon':
                        req.write("<td class='%s'>" % colname)
                        datetime = str(row[col])
                        if sessionContext.narrow:
                            datetime = datetime.replace(" ","<br />", 1)
                        datetime = datetime.replace("T","<br />", 1)
                        req.write(datetime)
                    elif colname == 'vote':
                        if str(row[col]) == '1' or str(row[col]) == 'yae':
                            req.write(
                                "<td class='yaevote'>"
                                "<img src='%s/thumbs_up.png' />"
                                % config.template_path
                            )
                        elif str(row[col]) == '0' or str(row[col]) == 'nay':
                            req.write(
                                "<td class='nayvote'>"
                                "<img src='%s/thumbs_down.png' />"
                                % config.template_path
                            )
                        else:
                            req.write(
                                "<td class='%s'>%s"
                                % (colname, str(row[col]))
                            )
                    elif colname == 'name':
                        req.write(
                            "<td class='voter'>" +
                            str(row[col])
                        )
                    else:
                        req.write(
                            "<td class='%s'>%s"
                            % (colname, str(row[col]))
                        )
                if not sessionContext.narrow:
                    req.write("</td>\n")
                else:
                    if  colname != 'title' and \
                        colname != 'artist' and \
                        colname != 'album' and \
                        colname != 'filename':
                        req.write("</td>\n")
            if sessionContext.admin == True:
                voteId = row[0]
                req.write(
                    "<td class='delete-vote'>\n"
                    "<img "
                        "class='delete-vote' "
                        "src='%s/close.button.png' "
                        "onclick='delete_vote(%d, %d, %d)' "
                    "/>\n"
                    "</td>\n"
                    % (
                        config.template_path,
                        sessionContext.sessionId,
                        sessionContext.voterId,
                        voteId
                    )
                )
            req.write("</tr>\n")
    req.write("</table>\n")
    return req.getvalue()

def dump_tally_table(cur, playlistId, narrow = True):
    req = StringIO()
    req.write("<table id='table_tally' class='db_table'>\n")
    # major query: this is where the magic happens
    # this retreives the tally, including the score/ranking of every song
    querystr = sql.tally_query(playlistId)
    TRACE("dump_tally_table: querystr(type = %s): '%s'"
          % (type(querystr), str(querystr)))
    cur.execute(querystr)
    rows = cur.fetchall()
    if len(rows) < 1:
        req.write("<tr><th>No tracks are available</th></tr>\n")
    else:
        cols = len(rows[0])
        req.write(
            "<tr>"
                "<th class='topheader' colspan='%d'>Tally</th>"
            "</tr>\n"
            "<tr>\n"
            % cols
        )
        for col in range(cols):
            heading = str(cur.description[col][0])
            if narrow:
                if heading == 'title':
                    req.write(
                        "<th class='subheader'>"
                            "Title / Artist / Album / Filename"
                        "</th>\n"
                    )
                elif heading == 'artist' or \
                    heading == 'yaes' or \
                    heading == 'nays' or \
                    heading == 'album' or \
                    heading == 'filename':
                        pass
                else:
                   req.write(
                       "<th class='subheader'>%s</th>\n"
                       % str(cur.description[col][0])
                    )
            else:
                req.write(
                    "<th class='subheader'>%s</th>\n"
                    % str(cur.description[col][0])
                )
        req.write("</tr>\n")
        for row in rows:
            # TODO: convert this to dictionary
            score = str(row[0])
            yaes = str(row[1])
            nays = str(row[2])
            title = str(row[3])
            artist = str(row[4])
            album = str(row[5])
            filename = str(row[6])
            filename = format_short_string(os.path.basename(filename))
            if len(title) < 1:
                title = filename
            if len(artist) < 1:
                artist = "&lt;artist unknown&gt;"
            if len(album) < 1:
                album = "&lt;album unknown&gt;"
            req.write("<tr>\n")
            for col in range(cols):
                colname = str(cur.description[col][0])
                if narrow:
                    if colname == 'title':
                        req.write(
                            "<td class='title_narrow'>"
                            "<span class='title'>%s</span><br />\n"
                            "&nbsp;&nbsp;%s<br />\n"
                            "&nbsp;&nbsp;%s<br />\n"
                            "&nbsp;&nbsp;%s</td>\n" %
                            (title, artist, album, filename)
                        )
                    elif colname == 'score':
                        req.write("<td class='score'><span class='score'>")
                        if len(score) < 1:
                            req.write("&nbsp;\n")
                        else:
                            # TODO: consider converting database results to
                            #       dict objects
                            req.write(
                                "%s </span><br /> %s yaes <br /> %s nays" 
                                % (score, yaes, nays)
                            )
                        req.write("</td>\n")
                    elif colname == 'artist' or \
                         colname == 'album' or \
                         colname == 'yaes' or \
                         colname == 'nays' or \
                         colname == 'filename':
                            pass
                    else:
                        req.write("<td>")
                        s = str(row[col])
                        if len(s) < 1:
                            req.write("&nbsp;\n")
                        else:
                            req.write(s)
                        req.write("</td>\n")
                else:
                    req.write("<td>")
                    s = str(row[col])
                    if len(s) < 1:
                        req.write("&nbsp;\n")
                    else:
                        if colname == 'title':
                            req.write(title)
                        elif colname == 'filename':
                            req.write(filename)
                        elif colname == 'artist':
                            req.write(artist)
                        elif colname == 'album':
                            req.write(album)
                        else:
                            req.write(s)
                    req.write("</td>\n")
            req.write("</tr>\n")
    req.write("</table>\n")
    return req.getvalue()

def dump_winners_table(cur, playlistId, numrecords = 0, narrow = True):
    req = StringIO()
    req.write("<table class='winners'>\n")
    querystr = sql.winners_query(playlistId, numrecords)
    TRACE("dump_winners_table: querystr(type = %s): '%s'"
          % (type(querystr), str(querystr)))
    cur.execute(querystr)
    rows = cur.fetchall()
    if len(rows) < 1:
        req.write("<tr><th>No tracks are available</th></tr>\n")
    else:
        req.write(
            "<tr><th class='header' colspan='100%'>Biggest Winners</th></tr>\n"
            "<tr>\n"
        )
        cols = len(rows[0])
        for col in range(cols):
            heading = str(cur.description[col][0])
            if narrow:
                if heading == 'title':
                    req.write("<th>Title / Artist / Album / Filename</th>\n")
                elif heading == 'artist' or \
                    heading == 'yaes' or \
                    heading == 'nays' or \
                    heading == 'album' or \
                    heading == 'filename':
                    pass
                else:
                    req.write("<th>%s</th>\n" % heading)
            else:
                req.write("<th>%s</th>\n" % heading)
        req.write("</tr>\n")
        for row in rows:
            if row[2] == 0 and row[3] == 0:
                continue
            req.write("<tr>\n")
            for col in range(cols):
                if narrow:
                    score = str(row[0])
                    yaes = str(row[1])
                    nays = str(row[2])
                    title = format_short_string(str(row[3]))
                    artist = format_short_string(str(row[4]))
                    album = format_short_string(str(row[5]))
                    filename = str(row[6])
                    basefilename = os.path.basename(filename)
                    basefilename = format_short_string(basefilename)
                    if str(cur.description[col][0]) == 'title':
                        req.write(
                            "<td class='title_narrow'>"
                            "<span class='title'>%s</span><br />\n"
                            "&nbsp;&nbsp;%s<br />\n"
                            "&nbsp;&nbsp;%s<br />\n"
                            "&nbsp;&nbsp;%s</td>\n" %
                            (title, artist, album, basefilename)
                        )
                    elif str(cur.description[col][0]) == 'score':
                        # TODO: make this more pythonic, with % args in strings
                        #       and conditional args
                        req.write("<td class='score'><span class='score'>")
                        if len(score) < 1:
                            req.write("&nbsp;\n")
                        else:
                            req.write(
                                "%s </span><br /> %s yaes <br /> %s nays"
                                % (score, yaes, nays)
                            )
                        req.write("</td>\n")
                    elif str(cur.description[col][0]) == 'artist' or \
                        str(cur.description[col][0]) == 'album' or \
                        str(cur.description[col][0]) == 'yaes' or \
                        str(cur.description[col][0]) == 'nays' or \
                        str(cur.description[col][0]) == 'filename':
                        pass
                    else:
                        req.write("<td>")
                        s = str(row[col])
                        if len(s) < 1:
                            req.write("&nbsp;\n")
                        else:
                            req.write(s)
                        req.write("</td>\n")
                else:
                    s = str(row[col])
                    if len(s) < 1:
                        s = "&nbsp;\n"
                    elif cur.description[col][0] == 'filename':
                        filename = os.path.basename(s)
                        basefilename = os.path.basename(filename)
                        basefilename = format_short_string(basefilename)
                        s = basefilename
                    req.write(
                        "<td class='%s'>%s</td>\n" % 
                        (cur.description[col][0], s)
                    )
            req.write("</tr>\n")
    req.write("</table>\n")
    return req.getvalue()

def dump_losers_table(cur, playlistId, numrecords = 0, narrow = True):
    req = StringIO()
    req.write("<table class='losers'>\n")
    querystr = sql.losers_query(playlistId, numrecords)
    TRACE("dump_losers_table: querystr(type = %s): '%s'"
          % (type(querystr), str(querystr)))
    cur.execute(querystr)
    rows = cur.fetchall()
    if len(rows) < 1:
        req.write("<tr><th>No tracks are available</th></tr>\n")
    else:
        cols = len(rows[0])
        req.write(
            "<tr><th class='header' colspan='100%'>Biggest Losers</th></tr>\n"
            "<tr>\n"
        )
        for col in range(cols):
            heading = str(cur.description[col][0])
            if narrow:
                if heading == 'title':
                    req.write("<th>Title / Artist / Album / Filename</th>\n")
                elif heading == 'artist' or \
                    heading == 'yaes' or \
                    heading == 'nays' or \
                    heading == 'album' or \
                    heading == 'filename':
                    pass
                else:
                    req.write("<th>%s</th>\n" % heading)
            else:
                req.write("<th>%s</th>\n" % heading)
        req.write("</tr>\n")
        for row in rows:
            if row[2] == 0 and row[3] == 0:
                # if there's been neither yaes or nays, then it's no point
                # reporting them here
                continue
            req.write("<tr>\n")
            for col in range(cols):
                if narrow:
                    score = str(row[0])
                    yaes = str(row[1])
                    nays = str(row[2])
                    title = format_short_string(str(row[3]))
                    artist = format_short_string(str(row[4]))
                    album = format_short_string(str(row[5]))
                    filename = str(row[6])
                    basefilename = os.path.basename(filename)
                    basefilename = format_short_string(basefilename)
                    if str(cur.description[col][0]) == 'title':
                        req.write(''.join([
                            "<td class='title_narrow'>"
                            "<span class='title'>", title, "</span><br />\n"
                            "&nbsp;&nbsp;", artist, "<br />\n"
                            "&nbsp;&nbsp;", album, "<br />\n"
                            "&nbsp;&nbsp;", basefilename, "</td>\n"
                        ]))
                    elif str(cur.description[col][0]) == 'score':
                        req.write("<td class='score'><span class='score'>")
                        if len(score) < 1:
                            req.write("&nbsp;\n")
                        else:
                            req.write(
                                "%s </span><br /> %s yaes <br /> %s nays" 
                                % (score, yaes, nays)
                            )
                        req.write("</td>\n")
                    elif str(cur.description[col][0]) in [
                        'artist', 'album', 'yaes', 'nays', 'filename']:
                        pass
                    else:
                        req.write("<td>")
                        s = str(row[col])
                        if len(s) < 1:
                            req.write("&nbsp;\n")
                        else:
                            req.write(s)
                        req.write("</td>\n")
                else:
                    s = str(row[col])
                    if len(s) < 1:
                        s = "&nbsp;\n"
                    else:
                        if cur.description[col][0] == 'filename':
                            s = os.path.basename(s)
                        s = format_short_string(s)
                    req.write(
                        "<td class='%s'>%s</td>\n" % 
                        (cur.description[col][0], s)
                    )
            req.write("</tr>\n")
    req.write("</table>\n")
    return req.getvalue()

def dump_admin_tables(sessionContext, controls = False):
    TRACE("dump_admin_tables: "
        "url = %s, sessionId = %d, voterId = %d, playlistId = %d"
        % (url, sessionId, voterId, playlistId)
    )
    return ''.join([
        "<div id='div_tables'>\n"
        "<div id='div_voters' class='db_table'>\n",
        dump_voters_table(sessionContext),
        "</div>\n"
        "<div id='sessions' class='db_table'>\n",
        dump_sessions_table(sessionContext.cur, sessionContext.admin),
        "</div>\n"
        "</div>\n"
    ])


def dump_vote_tables(sessionContext, controls = False):
    TRACE("dump_vote_tables: "
        "url = %s, sessionId = %d, voterId = %d, playlistId = %d"
        % (sessionContext.url, sessionContext.sessionId, sessionContext.voterId, sessionContext.playlistId)
    )
    return dump_tracks_table(
              sessionContext,
              sessionContext.playlistId,
              sessionContext.playlistName,
              controls = controls,
              select_playlist_dropdown = False)


'''

# I was considering the use of this as an AJAX response to update an entire row of data on-the-fly after an action (bwh)


def dump_track_row(sessionContext, trackId):

    # do query with trackId?
    trackId = str(row[0])
    filename = str(row[1])
    trackvoterId = str(row[2])
    vote = str(row[3])
    title = str(row[4])
    artist = str(row[5])
    album = str(row[6])
    basename = os.path.basename(filename)
    if len(title) < 1:
        title = basename
    if len(artist) < 1:
        artist = "&lt;artist unknown&gt;"
    if len(album) < 1:
        album = "&lt;album unknown&gt;"
    if len(basename) > 48:
        basename = basename[1:24] + " ... " + basename[-24:]

    vote_cell_id = "vote_%s_%s" % (str(trackId), str(voterId))

    vote_yae_function = (
        "vote_yae(%s, %s, %s, \"%s\")"
        % (sessionContext.sessionId, trackId, sessionContext.voterId, html_escape(basename))
    )
    vote_nay_function = (
        "vote_nay(%s, %s, %s, \"%s\")"
        % (sessionContext.sessionId, trackId, sessionContext.voterId, html_escape(basename))
    )

    TRACE("dump_track_row: "
        "url = %s, sessionId = %d, voterId = %d, playlistId = %d"
        % (url, sessionContext.sessionId, sessionContext.voterId, sessionContext.playlistId)
    )

    req = StringIO()
    if sessionContext.narrow:
        req.write(
            "<tr>"
            "<td class='track_info'>"
            "<span class='title'>%s</span><br />\n"
            "&nbsp;&nbsp;%s<br />\n"
            "&nbsp;&nbsp;%s<br />\n"
            "&nbsp;&nbsp;%s</td>\n" %
            (title, artist, album, basename)
        )
    else:
        req.write(
            "<tr>"
            "<td class='track_info'>"
            "<span class='title'>%s</span>"
            "</td>\n"
            "<td>%s</td>\n"
            "<td>%s</td>\n"
            "<td>%s</td>\n" %
            (title, artist, album, basename)
        )

    vote_yae_class = "vote"
    vote_nay_class = "vote"
    if str(trackvoterId) == str(voterId):
        if vote == "1" or vote == "yae":
            vote_yae_class = "votedyae"
        if vote == "0" or vote == "nay":
            vote_nay_class = "votednay"
    req.write(''.join([
        "<td class='", vote_yae_class, "' onclick='", vote_yae_function, "'>\n"
            "<a id='track_", str(trackId), "' class='votedyae' onclick='", vote_yae_function, "'> \n"
                "<img src='", config.template_path, "/up_arrow2.png' onclick='", vote_yae_function, "' />"
            "</a>"
        "</td>\n"
        "<td class='", vote_nay_class, "' onclick='", vote_nay_function, "'>\n"
            "<a id='track_", str(trackId), "' class='votedyae' onclick='", vote_nay_function, "'> \n"
                "<img src='", config.template_path, "/up_arrow2.png' onclick='", vote_nay_function, "' />"
            "</a>"
        "</td>\n"
    ])

    if (os.path.exists(filename)):
        req.write("<td class='exists'>Y</td>\n")
    else:
        req.write("<td class='exists'>N</td>\n")
    if sessionContext.admin == True:
        req.write(
            "<td class='delete_track'>\n"
                "<img "
                    "class='delete_track' "
                    "src='%s/close.button.png' "
                    "onclick='delete_track(%s, %s, %s)' "
                "/>\n"
            "</td>\n" 
            % (config.template_path, sessionId, voterId, trackId)
        )
    if controls == True:
        req.write(
            "<td class='controls'>\n"
                "<img "
                    "class='controls' "
                    "src='%s/play.png' "
                    "alt='PL' "
                    "onclick='play(%d, %d)' "
                "/>\n" 
            "</td>\n"
            % (config.template_path, sessionId, voterId )
        )
    req.write("</tr>\n")

    return req.getvalue()
'''


