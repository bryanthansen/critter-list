# -- coding: utf-8 --
#!/usr/bin/python3 -u

"""
config.py - global configuration information

@author: Bryant Hansen <github1@bryanthansen.net>
@license: GPLv3
"""

from datetime import datetime

DIR = '/var/www/localhost/critter_list'
LOGDIR = '/projects/critter_list/log'

now = datetime.now().strftime("%Y%m%d_%H%M%S")

# @NOTE: if time is given to the second, a new file will be created for each
#        web request; therefore, truncate to the day
LOGFILE = "%s/%s.log" % (LOGDIR, now[0:8])
SQL_LOGFILE = "%s/%s.%s.log" % (LOGDIR, 'sql', now)

# web paths
critter_list_web_root = "/critter_list"
wsgi_path =             critter_list_web_root + "/wsgi"
javascript_path =       critter_list_web_root + "/js"
template_path =         critter_list_web_root + "/template"
css_path =              template_path + "/css"
DB_FILE = 'db/critters.db'

default_url = "player.wsgi"

# @TODO switch from dummy session ID to a real one, composed of a sha256 hash
default_sessionId = 123456789
default_voterId = 0
default_playlistId = 1

defaultClientWidth = 1024
defaultClientHeight = 768

narrow = False
