# -- coding: utf-8 --
#!/usr/bin/python3 -u

"""
critter_constants.py - miscellaneous constants used throughout the app

@author: Bryant Hansen <github1@bryanthansen.net>
@license: GPLv3
"""

ONE_MINUTE = 60
FIVE_MINUTES = 300
FIVE_HOURS = 18000
INFINITY = 1000000000
# we will not automatically repeat a song again within this interval, unless
# it's absolutely necessary
MIN_REPEAT_TIME = FIVE_HOURS
