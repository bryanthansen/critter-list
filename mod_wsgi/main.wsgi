# -- coding: utf-8 --
#!/usr/bin/python3 -u

"""
@brief
main.wsgi - entry point from apache call
part of the Critter List - a dynamic voting system for playlists

@author: Bryant Hansen <sourcecode@bryanthansen.net>
@license: GPLv3
"""

import sys
import os
import logging

from traceback import format_exc, print_exc

'''
import os, sys, inspect
 # realpath() will make your script run, even if you symlink it :)
 cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
 if cmd_folder not in sys.path:
     sys.path.insert(0, cmd_folder)

 # use this if you want to include modules from a subfolder
 cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile( inspect.currentframe() ))[0],"subfolder")))
 if cmd_subfolder not in sys.path:
     sys.path.insert(0, cmd_subfolder)

 # Info:
 # cmd_folder = os.path.dirname(os.path.abspath(__file__)) # DO NOT USE __file__ !!!
 # __file__ fails if script is called in different ways on Windows
 # __file__ fails if someone does os.chdir() before
 # sys.argv[0] also fails because it doesn't not always contains the path
 
'''

#sys.path.insert(0, "critter_list" + os.sep + "lib")
sys.path.insert(0, "/projects/critter_list/mod_wsgi/critter_list/lib")

import critter_list.lib.critter as critter
import critter_list.lib.login as login
import critter_list.lib.pages as pages
import critter_list.lib.ajax as ajax
import critter_list.config as config
from critter_list.lib.trace import TRACE
from session import SessionContext
from server import ServerContext

######################
# Main (mod_wsgi)
def application(environ, start_response):

    output = ""
    status = ""

    logging.basicConfig(filename=config.LOGFILE, level=logging.DEBUG)

    player_playlist_id = config.default_playlistId

    TRACE("Python Version: %s" % str(sys.version), logging.INFO)
    TRACE("Python Path: %s" % str(sys.path), logging.INFO)
    TRACE("os.path.dirname(__file__) = %s" % os.path.dirname(__file__))
    DIR = os.path.dirname(__file__)
    try:
        os.chdir(DIR)
    except:
        TRACE(
            "Failed to change current dir to %s; let's see what happens.  "
            "curdir = %s"
            % (DIR, os.getcwd())
        )
    try:
        # SCRIPT_FILENAME:
        #    /var/www/localhost/htdocs/critter_list/wsgi/admin.wsgi
        # parse from SCRIPT_FILENAME vs REQUEST_URI?
        # HTTP_REFERRER seems to stay current and not updated by the AJAX call
        # We want the base script here.
        # SCRIPT_FILENAME must be used after a logout, to restore the default
        # page on login.

        script = critter.get_base_filename_from_env(environ, 'SCRIPT_FILENAME')
        referrer =  critter.get_base_filename_from_env(environ, 'HTTP_REFERER')
        if environ['REQUEST_METHOD'] == 'POST':
            TRACE("POST DETECTED!")
        try:
            sessionContext = SessionContext(environ = environ, url = script)
        except RuntimeError:
            output = pages.dump_error_page(
                                    "RuntimeError creating session context")
        TRACE(
            "application: "
            "new sessionContext(sessionId = %d) created: "
            "voterId = %d, voterName = '%s', playlistId = '%s'"
            % (
                sessionContext.sessionId,
                sessionContext.voterId,
                sessionContext.voterName,
                str(sessionContext.playlistId)
            )
        )
        try:
            serverContext = ServerContext(sessionContext.voterId,
                                          sessionContext.urldict,
                                          sessionContext.con)
        except RuntimeError:
            output = pages.dump_error_page(
                                    "RuntimeError creating server context")
            player_playlist_id = config.default_playlistId

        standard_pages = [
            "admin_votes.wsgi",
            "admin_playlists.wsgi",
            "admin_params.wsgi",
            "admin_voters.wsgi",
            "add_track.wsgi",
            "other_playlists.wsgi",
            "debug.wsgi",
            "gong.wsgi",
            "docs.wsgi",
            "podcast.wsgi",
            "stats.wsgi",
            "swipe.wsgi",
            "sessions.wsgi",
            "tally.wsgi",
            "visuals.wsgi",
            "votes.wsgi"
        ]
        server_pages = [
            "playlist.wsgi",
            "session_context.wsgi",
            "player.wsgi",
            "player_config.wsgi",
            "concept.wsgi",
            "config_playlist.wsgi",
            "session_context.wsgi",
            "player_playlist.wsgi"
        ]
        ajax_server_commands = [
            "player_command.wsgi",
            "do_gong.wsgi",
            "set_player_playlist.wsgi"
        ]
        ajax_admin_commands = [
            "delete_voter.wsgi",
            "delete_track.wsgi",
            "delete_vote.wsgi",
            "disconnect_voter.wsgi"
        ]
        content_type = 'text/html'
        # to start with, if we do not have an identified voter ID...
        if sessionContext.voterId <= 0:
            # we're not logged in; go straight to the login page
            output = login.dump_login_page(environ, script, sessionContext.con)
            status = '200 OK'
        elif script == "logout.wsgi":
            output = login.dump_login_page(sessionContext)
            status = '200 OK'

        ############################
        # Page requests

        elif script in standard_pages:
            TRACE("%s is a standard page, "
                  "compliant with the pages.dump_page interface"
                  % script)
            output = pages.dump_page(sessionContext, script)
            status = '200 OK'
        elif script in server_pages:
            TRACE("%s is a server page, "
                  "compliant with the pages.dump_server_page interface"
                  % script)
            output = pages.dump_server_page(sessionContext,
                                              serverContext,
                                              script)
            status = '200 OK'
        elif script == "tracks.wsgi":
            output = pages.dump_tracks_page(sessionContext,
                                              sessionContext.playlistId,
                                              sessionContext.playlistName,
                                              "VOTE",
                                              False)
            status = '200 OK'


        ############################
        # AJAX commands

        elif script in ajax_server_commands:
            sessionContext.url = referrer
            TRACE("%s is a ajax_server_commands page, "
                  "compliant with the ajax.do_player_command interface"
                  % script)
            output = ajax.do_server_command(sessionContext, serverContext, script)
            status = '200 OK'
            content_type = 'text/plain'
        elif script in ajax_admin_commands:
            sessionContext.url = referrer
            TRACE("%s is a ajax_server_commands page, "
                  "compliant with the ajax.do_player_command interface"
                  % script)
            output = ajax.do_admin_command(sessionContext, serverContext, script)
            status = '200 OK'
            content_type = 'text/plain'
        elif script == "vote.wsgi":
            sessionContext.url = referrer
            output = ajax.do_vote(sessionContext)
            status = '200 OK'
            content_type = 'text/plain'
        else:
            # not found
            TRACE(
                "%s was not found; script = '%s', sessionId = %d, voterId = %d, " 
                % (
                    sessionContext.environ['REQUEST_URI'],
                    script,
                    sessionContext.sessionId,
                    sessionContext.voterId
                )
            )
            if script == 'other_playlists.wsgi':
                TRACE(
                    "%s was not found, but the script should match one in standard_pages: '%s'"
                    % (
                        script,
                        str(standard_pages)
                    )
                )
            output = (
                "File Not Found: uri = %s, script = '%s', sessionId = %d, voterId = %d"
                % (
                    sessionContext.environ['REQUEST_URI'],
                    script,
                    sessionContext.sessionId,
                    sessionContext.voterId
                )
            )
            status = '404 Page Not Found' 
            content_type = 'text/plain'
        sessionContext.con.close()
    except:
        e = sys.exc_info()[0]
        TRACE("Exception running " + str(sys.argv[0]))
        print_exc(file=environ['wsgi.errors'])
        #return HTTPError(500, "Internal Server Error", e, format_exc(10))
        status = '500 Internal Server Error' 
        output = str(sys.exc_info()[0]) + "\n" + format_exc(10)
        content_type = 'text/plain'

    out = output.encode('utf-8')

    response_headers = [('Content-type', str(content_type)),
                        ('Content-Length', str(len(out)))]

    start_response(status, response_headers)

    return [out]
