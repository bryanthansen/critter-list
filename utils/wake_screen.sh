#!/bin/bash
# Bryant Hansen

export DISPLAY=:0
xset dpms force on
xset s reset
qdbus \
| grep kscreenlocker_greet \
| xargs -I {} qdbus {} /MainApplication quit

