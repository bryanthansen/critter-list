# Bryant Hansen
# Critter List Makefile
# 20160130
#
# Used mainly for installing and uninstalling

MAKE = make

DT = $(shell date "+%Y%m%d_%H%M%S")
DB = /var/www/localhost/critter_list/db/critters.db.new

LOGDIR=/var/log/critter_list/
USER_LOGDIR=~/.log/critter_list/

.PHONY: all test install uninstall install-template save-template list-templates

all:
	cat INSTALL.md

pyplayer_install:
	$(MAKE) -C pyplayer2 install

pyplayer_uninstall:
	$(MAKE) -C pyplayer2 uninstall

check_dependencies:
	which rsync
	which exiftool
	# python-3.x

$(DB): db/blank.db.dump
# use shell wrapper here?  Is it automatic already?
# ie. $(shell cat $< | sqlite3 $@)
# backup the original, if it exists
# don't update the database if it exists; make the new version
# make it to the default location if it does not exist
#[ ! -f $@ ] || mv $@ .$@.$(shell date "+%Y%m%d_%H%M%S")
	[ ! -f $@ ] || touch $@
	[ -f $@ ] || cat $< | sqlite3 $@

install: pyplayer_install $(DB)
#	$(MAKE) -C install install
	./install/install.sh

uninstall:
#	$(MAKE) -C install uninstall
	./install/uninstall.sh

test:
	@echo TODO

#remove_identical: $(INSTALL_DIR) $(LAST_BACKUP_DIR)
#	# remove any subdirectory trees that are identical to the depencency
#	[ ! $(LAST_BACKUP_DIR) ] || [ ! -d $(LAST_BACKUP_DIR) ] || ! diff -r $(LAST_BACKUP_DIR) $(INSTALL_DIR) || rm -rf $(LAST_BACKUP_DIR)
