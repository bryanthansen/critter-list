Python 3.4.5 (default, Jan 22 2017, 14:54:07) 
[GCC 4.9.4] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import sqlite3
>>> 
>>> dbConnection = sqlite3.connect('/var/www/localhost/critter_list/wsgi/db/critters.db')
>>> cur = dbConnection.cursor()
>>> 
>>> playlistId = 2
>>> voterId = 1
>>> 
>>> sql = (
...     "SELECT "
...         "tracks.id AS id, "
...         "ltrim(urls.rpath, '.') AS filename, "
...         "votes.voterId, "
...         "votes.vote, "
...         "tracks.title AS title, "
...         "artists.name AS artist, "
...         "albums.name AS album "
...     "FROM playlist_tracks "
...     "LEFT JOIN tracks ON tracks.id = playlist_tracks.track_num "
...     "LEFT JOIN urls ON tracks.url = urls.id "
...     "LEFT JOIN votes "
...         "ON tracks.id = votes.trackId AND votes.voterId = '%d' "
...     "LEFT JOIN artists ON tracks.artist = artists.id "
...     "LEFT JOIN albums ON tracks.album = albums.id "
...     "WHERE playlist_tracks.playlist_id = '%d' "
...     "ORDER BY random() "
...     "LIMIT 500 " %
...     (voterId, playlistId)
... )
>>> print("sql = '%s" % sql)
sql = 'SELECT tracks.id AS id, ltrim(urls.rpath, '.') AS filename, votes.voterId, votes.vote, tracks.title AS title, artists.name AS artist, albums.name AS album FROM playlist_tracks LEFT JOIN tracks ON tracks.id = playlist_tracks.track_num LEFT JOIN urls ON tracks.url = urls.id LEFT JOIN votes ON tracks.id = votes.trackId AND votes.voterId = '1' LEFT JOIN artists ON tracks.artist = artists.id LEFT JOIN albums ON tracks.album = albums.id WHERE playlist_tracks.playlist_id = '2' ORDER BY random() LIMIT 500 

>>> cur.execute(sql)
<sqlite3.Cursor object at 0x7fc284f160a0>
>>> rows = cur.fetchall()
>>> for col in range(len(rows[0])):                                              
...     print("str(cur.description[%d]) = %s" % (col, str(cur.description[col])))
... 
str(cur.description[0]) = ('id', None, None, None, None, None, None)
str(cur.description[1]) = ('filename', None, None, None, None, None, None)
str(cur.description[2]) = ('voterId', None, None, None, None, None, None)
str(cur.description[3]) = ('vote', None, None, None, None, None, None)
str(cur.description[4]) = ('title', None, None, None, None, None, None)
str(cur.description[5]) = ('artist', None, None, None, None, None, None)
str(cur.description[6]) = ('album', None, None, None, None, None, None)
>>> zip(cur.description)
<zip object at 0x7fc284f36548>
>>> list(zip(cur.description))
[(('id', None, None, None, None, None, None),), (('filename', None, None, None, None, None, None),), (('voterId', None, None, None, None, None, None),), (('vote', None, None, None, None, None, None),), (('title', None, None, None, None, None, None),), (('artist', None, None, None, None, None, None),), (('album', None, None, None, None, None, None),)]
>>> list(zip(cur.description))[0]
(('id', None, None, None, None, None, None),)
>>> list((cur.description))   
[('id', None, None, None, None, None, None), ('filename', None, None, None, None, None, None), ('voterId', None, None, None, None, None, None), ('vote', None, None, None, None, None, None), ('title', None, None, None, None, None, None), ('artist', None, None, None, None, None, None), ('album', None, None, None, None, None, None)]
>>> list((cur.description))[0]
('id', None, None, None, None, None, None)
>>> cur.description[0]
('id', None, None, None, None, None, None)
>>> cur.description[0][0]
'id'
>>> cur.description[:][0]
('id', None, None, None, None, None, None)
>>> cur.description[0:3][0]
('id', None, None, None, None, None, None)
>>> cur.description[0][0]     
'id'




>>> cur.description
(('id', None, None, None, None, None, None), ('filename', None, None, None, None, None, None), ('voterId', None, None, None, None, None, None), ('vote', None, None, None, None, None, None), ('title', None, None, None, None, None, None), ('artist', None, None, None, None, None, None), ('album', None, None, None, None, None, None))

>>> list(zip(*cur.description))
[('id', 'filename', 'voterId', 'vote', 'title', 'artist', 'album'), (None, None, None, None, None, None, None), (None, None, None, None, None, None, None), (None, None, None, None, None, None, None), (None, None, None, None, None, None, None), (None, None, None, None, None, None, None), (None, None, None, None, None, None, None)]

>>> list(zip(*cur.description))[0]
('id', 'filename', 'voterId', 'vote', 'title', 'artist', 'album')

>>> type(list(zip(*cur.description))[0])
<class 'tuple'>

>>> list(list(zip(*cur.description))[0])
['id', 'filename', 'voterId', 'vote', 'title', 'artist', 'album']

>>> dict([ (n, list(list(zip(*cur.description))[0])[n]) for n in range(len(cur.description))])
{0: 'id', 1: 'filename', 2: 'voterId', 3: 'vote', 4: 'title', 5: 'artist', 6: 'album'}

>>> dict([ (list(list(zip(*cur.description))[0])[n], n) for n in range(len(cur.description))])
{'vote': 3, 'voterId': 2, 'filename': 1, 'artist': 5, 'album': 6, 'title': 4, 'id': 0}
