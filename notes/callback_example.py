class Something:
    def my_callback(self, arg_a):
        print(arg_a)

class SomethingElse:
    def __init__(self, callback):
        self.callback = callback

something = Something()
something_else = SomethingElse(something.my_callback)
something_else.callback("It works...")


