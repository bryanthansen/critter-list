#!/bin/bash
# Bryant Hansen
# GPLv3

# The Democratic DJ - a critter list of entertainment


ME="$(basename "$0")"
DB="critters.db"

CONF="./critters.conf"

create_tables() {
    local DB="$1"
    sqlite3 "$DB" "CREATE TABLE storage_devices (id INTEGER PRIMARY KEY, name VARCHAR(256), label VARCHAR(64), uuid VARCHAR(64), type VARCHAR(12), partuuid VARCHAR(64));"
    sqlite3 "$DB" "CREATE TABLE files (id INTEGER PRIMARY KEY, storage_device INTEGER, path VARCHAR(256), filename VARCHAR(1024));"
    sqlite3 "$DB" "CREATE TABLE playlist (id INTEGER PRIMARY KEY, trackId INTEGER, play_order INTEGER, yaes INTEGER, nahs INTEGER);"
    sqlite3 "$DB" "CREATE TABLE voters (id INTEGER PRIMARY KEY, name VARCHAR(256), status INTEGER);"
    sqlite3 "$DB" "CREATE TABLE votes (id INTEGER PRIMARY KEY, trackId INTEGER, voterId INTEGER, vote INTEGER, votedon DATETIME);"
    sqlite3 "$DB" "CREATE TABLE sessions (id INTEGER PRIMARY KEY, name VARCHAR(256), voterId INTEGER, ip VARCHAR(20), mac VARCHAR(30), starttime DATETIME, endtime DATETIME, status INTEGER);"
}

# Description:
#   Takes a device
#   Determines relative parameters via the blkid call
#   Checks for existing record in the db
#   If no record matches exactly, then add a new record
# sample blkid output:
# /dev/sdb3: LABEL="DATA_005" UUID="0f6706f2-3bc6-41a8-a636-f423c84bdc70" TYPE="xfs" PARTUUID="547e0e9d-d994-4aa8-89d5-bd883861534b"
add_device() {
    # TODO: use  path as an argument, rather than the device
    echo "$ME: $0 $1" >&2

    local name="$1"
    local info="$(blkid | grep "^${name}.*$")"
    local lines="$(echo "$info" | wc -l)"
    local dev=""
    local label=""
    local uuid=""
    local partuuid=""
    local fstype=""
    case $lines in
    0)
        echo "No data found for device $name" >&2
        return 2
        ;;
    1)
        echo "$name params: $info"
        for l in $info ; do
            [[ ! "$l" ]] && continue
            l="${l//\"/}"
            [[ ! "${l%%/*}" ]] && [[ ! "${l##*:}" ]] && dev="${l%:}"
            [[ ! "${l%%LABEL=*}" ]] && label="${l#LABEL=}"
            [[ ! "${l%%UUID=*}" ]] && uuid="${l#UUID=}"
            [[ ! "${l%%TYPE=*}" ]] && fstype="${l#TYPE=}"
            [[ ! "${l%%PARTUUID=*}" ]] && partuuid="${l#PARTUUID=}"
        done
        # first check for a record that matches exactly; if it doesn't exist, add a new one
        #echo "sqlite3 $DB \"SELECT * FROM storage_devices WHERE name LIKE '${name}' AND label LIKE '${label}' AND uuid LIKE '${uuid}' AND type LIKE '${fstype}' AND partuuid LIKE '${partuuid}'\""
        records="$(sqlite3 $DB "SELECT * FROM storage_devices WHERE name LIKE '${name}' AND label LIKE '${label}' AND uuid LIKE '${uuid}' AND type LIKE '${fstype}' AND partuuid LIKE '${partuuid}'";)"
        if [[ "$records" ]] ; then
            echo -e "records exist: $records" >&2
        else
            #echo "sqlite3 $DB \"INSERT INTO storage_devices (name, label, uuid, type, partuuid) VALUES (${name}, ${label}, ${uuid}, ${fstype}, ${partuuid});\""
            echo "${DB}: adding $name" >&2
            sqlite3 $DB "INSERT INTO storage_devices (name, label, uuid, type, partuuid) VALUES ('${name}', '${label}', '${uuid}', '${fstype}', '${partuuid}');"
        fi
        ;;
    *)
        echo "ERROR: $lines lines found.  Expected only 1!" >&2
        echo -e "lines: $info" >&2
        ;;
    esac
}

add_track() {
    local filename="$1"
    filename="${filename//\'/''}"
    records="$(sqlite3 $DB "SELECT * FROM files WHERE filename LIKE '${filename}'";)"
    if [[ "$records" ]] ; then
        echo -e "records exist: $records" >&2
    else
        echo "adding record to DB $DB for $filename" >&2
        sqlite3 $DB "INSERT INTO files (filename) VALUES ('${filename}');"
    fi
    (( num_files++ ))
}

add_voter() {
    local name="$1"
    name="${name//\'/''}"
    records="$(sqlite3 $DB "SELECT * FROM voters WHERE name LIKE '${name}'";)"
    if [[ "$records" ]] ; then
        echo -e "voter record exists: $records" >&2
    else
        echo "adding voter record to DB $DB for voter $name" >&2
        sqlite3 $DB "INSERT INTO voters (name) VALUES ('${name}');"
    fi
    (( num_voters++ ))
    return 0
}

add_vote() {
    local voterId="$1"
    local trackId="$2"
    local vote="$3"
    name="${name//\'/''}"
    records="$(sqlite3 $DB "SELECT * FROM votes WHERE voterId = '${voterId}' AND trackId = '${trackId}' AND vote = '${vote}'";)"
    if [[ "$records" ]] ; then
        echo -e "${DB}: vote exists: voterId '${voterId}', trackId '${trackId}', vote='${vote}'.  Updating..." >&2
        sqlite3 $DB "UPDATE votes SET vote = '${vote}' WHERE voterId = '${voterId}' AND trackId = '${trackId}';"
    else
        echo "${DB}: adding vote of $vote for voterId $voterId and trackId $trackId" >&2
        sqlite3 $DB "INSERT INTO votes (voterId, trackId, vote) VALUES ('${voterId}', '${trackId}', '${vote}');"
    fi
    (( num_votes++ ))
    return 0
}

# add some number of random votes
add_random_vote() {
    local num_voters="$1"
    local num_files="$2"
    local vote="$3"

    voterId=$(expr $RANDOM % $num_voters + 1)
    trackId=$(expr $RANDOM % $num_files + 1)
    vote=$(expr $RANDOM % 2)

    add_vote $voterId $trackId $vote
    return $?
}

# sqlite3 pivot table tip from here:
#   http://softwaresalariman.blogspot.ch/2008/05/pivot-table-hack-in-sqlite3-and-mysql.html
#   (looks unnecessarily-hard)
dump_in_order_of_score() {
    sqlite3 $DB "SELECT files.filename, \
        COUNT(CASE when vote = '1' THEN vote END) AS yaes, \
        COUNT(CASE when vote = '0' THEN vote END) AS nahs, \
        (COUNT(CASE WHEN vote = '1' THEN vote END) - COUNT(CASE WHEN vote = '0' THEN vote END)) AS score \
        FROM files \
        LEFT JOIN votes ON votes.trackId=files.id \
        GROUP BY files.id \
        ORDER BY score DESC, yaes;"
}


########################
# Main

./amarokdb_to_sqlite.sh "$DB"

create_tables "$DB"

chgrp apache "$DB"
chmod g+w "$DB"

exit 0
