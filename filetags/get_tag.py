#!/usr/bin/env python3
"""
@author: Bryant Hansen
@license: GPLv3

@brief id3ted wrapper: list all tags

@note id3v1 tags are automatically converted to id3v2

@note: warning: this program is heavily-dependent on the output format of id3ted
"""

import sys
from lib.filetags import *

##################################################################
# Constants
#
ME = sys.argv[0]
USAGE = "%s  file  tag" % ME


##################################################################
# Argument processing
#
if len(sys.argv) <= 2:
    sys.stderr.write("ERROR: this program requires 2 arguments")
    sys.exit(2)
filename = sys.argv[1]
matchtag = sys.argv[2]


##################################################################
# Main
#
tag = get_tag(filename, matchtag)
if tag != None:
    if type(tag) == str:
        if len(tag) > 0:
            sys.stdout.write('%s\n' % tag)
            sys.exit(0)

sys.stderr.write("ERROR: failed to get tag '%s' from '%s'\n"
                 % (matchtag, filename))
sys.exit(1)
