#!/usr/bin/env python3
"""
@author: Bryant Hansen
@license: GPLv3

@brief id3ted wrapper: list all tags

@note id3v1 tags are automatically converted to id3v2

@note: warning: this program is heavily-dependent on the output format of id3ted
"""

import sys
from lib.filetags import *

##################################################################
# Constants
#
ME = sys.argv[0]
USAGE = "%s  file" % ME


##################################################################
# Argument processing
#
if len(sys.argv) <= 1:
    sys.stderr.write("ERROR: this program requires 1 argument")
    sys.exit(2)
filename = sys.argv[1]


##################################################################
# Main
#
tags = list_tags(filename)
if tags != None:
    if type(tags) == dict:
        if len(tags.keys()) > 0:
            sys.stdout.write('%s\n' % tags)
            sys.exit(PASSED)
        else:
            sys.stderr.write("ERROR: file %s contains no detectable tags\n"
                             % filename)
    else:
        sys.stderr.write("ERROR: list_tags return data type %s.  "
                         "It should be a dict.\n"
                         % str(type(tags)))
else:
    sys.stderr.write("ERROR: list_tags returned nothing.\n")
sys.exit(FAILED)