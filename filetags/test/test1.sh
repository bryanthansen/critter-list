#!/bin/sh
# Bryant Hansen

PASSED=0
FAILED=1

[[ -f ./test.conf ]] && . ./test.conf
[[ -f ./test/test.conf ]] && . ./test/test.conf

log() {
    echo -e "$(dt): $0: $1" >&2
}

do_test() {
    log "$1 $2 $3 $4"
    if $1 "$2" "$3" "$4" ; then
        # test passed
        return $PASSED
    fi
    log "*** test $1 $2 $3 $4: return code indicated error $?.  test failed"
    return $FAILED
}

do_test_error_handling() {
    local RESULT=$PASSED
    file="./does_not_exist.mp3"
    log "=================================="
    log "executing test error handling array file $file"
    do_test ./get_tag.py   "$file" TBPM            && RESULT=$FAILED
    do_test ./set_tag.py   "$file" COMM "$0 $(dt)" && RESULT=$FAILED
    do_test ./get_tag.py   "$file" COMM            && RESULT=$FAILED
    do_test ./set_tag.py   "$file" TIT2 "Fever"    && RESULT=$FAILED
    do_test ./get_tag.py   "$file" TIT2            && RESULT=$FAILED
    do_test ./list_tags.py "$file"                 && RESULT=$FAILED
    if [[ $RESULT == $PASSED ]] ; then
        log "test_error_handling(${file}): all tests passed"
    else
        log "test_error_handling(${file}): at least one test failed"
    fi
    log "==================================\n"
    return $RESULT
}

do_test_sequence() {
    local RESULT=$PASSED
    file="$1"
    log "=================================="
    log "executing test array on file $file"
    # TBPM is only present in id3v2; don't call it a failure if missing
    do_test ./get_tag.py   "$file" TBPM
    do_test ./set_tag.py   "$file" COMM "$0 $(dt)" || RESULT=$FAILED
    do_test ./get_tag.py   "$file" COMM            || RESULT=$FAILED
    do_test ./get_tag.py   "$file" TIT2            || RESULT=$FAILED
    do_test ./set_tag.py   "$file" TIT2 "FEVER"    || RESULT=$FAILED
    do_test ./list_tags.py "$file"                 || RESULT=$FAILED
    do_test ./get_tag.py   "$file" TIT2            || RESULT=$FAILED
    do_test ./set_tag.py   "$file" TIT2 "Fever"    || RESULT=$FAILED
    do_test ./get_tag.py   "$file" TIT2            || RESULT=$FAILED
    do_test ./list_tags.py "$file"              || RESULT=$FAILED
    if [[ $RESULT == $PASSED ]] ; then
        log "test_sequence(${file}): all tests passed"
    else
        log "test_sequence(${file}): at least one test failed"
    fi
    log "==================================\n"
    return $RESULT
}

do_readback_tests() {
    local RESULT=$PASSED
    file="$1"
    log "=================================="
    log "executing readback test on $file"
    bpm_val=123.45
    do_test ./set_tag.py   "$file" TBPM "$bpm_val"   || RESULT=$FAILED
    val="$(./get_tag.py   "$file" TBPM)"
    log "do_readback_tests(${file}): read back $val"
    if [[ "$val" == "$bpm_val" ]] ; then
        log "wrote $bpm_val to tag TBPM in $file and read it back successfully"
    else
        log "ERROR: wrote '$bpm_val' to tag TBPM in $file, but read back '$val'.  test failed"
        RESULT=$FAILED
    fi
    if [[ $RESULT == $PASSED ]] ; then
        log "do_readback_tests(${file}): test passed"
    else
        log "do_readback_tests(${file}): test failed"
    fi
    log "==================================\n"
    return $RESULT
}

RESULT=$PASSED
for file in $files ; do
    do_test_sequence "$file" || RESULT=$FAILED
    do_readback_tests "$file"
done
do_test_error_handling || RESULT=$FAILED

if [[ $RESULT == $PASSED ]] ; then
    log "all tests passed"
else
    log "at least one test filed"
fi

