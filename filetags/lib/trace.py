# -- coding: utf-8 --
#!/usr/bin/python3 -u

"""
trace.py - general purpose logging wrapper

@author: Bryant Hansen <github1@bryanthansen.net>
@license: GPLv3
"""

import logging
from sys import stderr
from datetime import datetime
from inspect import stack
#import config

default_level = logging.DEBUG


try:
    from io import StringIO
except ImportError:
    from cStringIO import StringIO


class Singleton:

    _instance = None
    _debuglog = None
    _logger = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(Singleton, cls).__new__(
                                cls, *args, **kwargs)
        return cls._instance

    def initialize_logger(self):
        logging.basicConfig(level = default_level)

    def __init__(self):
        if self._debuglog == None:
            self._debuglog = StringIO()
            self.initialize_logger()

    def trace(self, msg,  level = default_level):
        m = "%s: %s" % (datetime.now().strftime("%Y%m%d_%H%M%S.%f")[0:-3], msg)
        self._debuglog.write("%s\n" % m)
        logging.log(level, m)


def TRACE(msg, level = default_level):
    # get the name of the calling function: str(stack()[2][3])
    calling_function = ""
    try:
        calling_function = stack()[2][3]
    except:
        pass
    if type(level) != type(logging.DEBUG):
        Singleton().trace(
            "ERROR: invalid param type passed to TRACE: %s; should be %s; "
            "calling_function: %s"
            % (
                str(type(level)),
                str(type(logging.DEBUG)),
                str(calling_function)
            ),
            logging.ERROR
        )
        level = logging.ERROR
    m = "%s: %s" % (calling_function, msg)
    Singleton().trace(m, level)

