#!/usr/bin/env python3
"""
@author: Bryant Hansen
@license: GPLv3

@brief id3ted wrapper: list all tags

@note id3v1 tags are automatically converted to id3v2

@note: warning: this program is heavily-dependent on the output format of id3ted
"""

import sys
import subprocess
from lib.trace import TRACE

ID3TED_FILE="/projects/critter_list/id3ted/id3ted"
VERBOSE = True
VERBOSE = False

PASSED = 0
FAILED = 1

id3conv = {
            'Title': 'TIT2',
            'Track': 'TRCK',
            'Artist': 'TPE1',
            'Year': 'TDRC',
            'Album': 'TALB',
            'Genre': 'TCON',
            'Comment': 'COMM'
          }


##################################################################
# Functions
#
def dump_command(command):
    try:
        dict = str.maketrans("[],'", "    ")
        cmd = str(command)
        cmd = cmd.translate(dict)
        cmd = cmd.strip()
        cmd = cmd.replace('  ', ' ')
        TRACE("%s: sending command '%s'" % (ME, str(cmd)))
    except:
        TRACE("%s: Exception %s displaying command '%s'"
                         % (
                             ME,
                             str(sys.exc_info()),
                             str(command)
                           ))
        traceback.print_exc()

def dump_tag(tagline, matchtag = None):
    t = tagline.split(':')
    t2 = t[0].strip()
    val = ""
    tagV2 = t2
    try:
        tagV2 = id3conv[t2]
    except:
        pass
    val = ""
    try:
        if len(t) == 2:
            # the expected case of 1 delimiter
            val = t[1].strip()
        if len(t) > 2:
            # multiple delimiters - must restore the rest of the string
            if len(str(tagline)) > 6:
                val = str(tagline[6:])
            else:
                val = ""
        # print("%s(%s) = %s" % (tagV2, t2, val))
        if tagV2 == matchtag or matchtag == None:
            if VERBOSE:
                TRACE("lib.dump_tag returning %s, %s" % (tagV2, val))
            return tagV2, val
    except:
        TRACE("Exception %s reading tag from line %s"
              % (sys.exc_info(), tagline))
        traceback.print_exc()
    return None, None

def list_tags(filename):
    command = [ ID3TED_FILE, "-l", filename ]

    if VERBOSE: dump_command(command)
    proc = subprocess.Popen(command,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            shell=False)

    # capture output, wait for completion, and dump the output
    stdout, stderr = proc.communicate(input=input)
    returncode = proc.returncode
    if VERBOSE:
        TRACE("subprocess.Popen complete; result = %s"
                        % str(returncode))
    if returncode != 0:
        TRACE("subprocess.Popen %s return code(%d) indicates a "
                        "failure.  "
                        "Exiting abnormally."
                        % (command, returncode))
        sys.exit(returncode)

    if VERBOSE:
        TRACE(stdout)
        TRACE(stderr)

    # initialze a blank dict
    tags = {}
    id3v1 = False
    id3v2 = False
    tagfound = False
    out = stdout.decode('utf8')
    for line in out.split('\n'):
        line = line.rstrip()
        if len(line) > 0:
            s = line
            if type(line) == bytes: s = line.decode('utf8')
            tagfound = True
            if s[0:5] == "ID3v1":
                if (VERBOSE):
                    TRACE("%s tag:" % s1[0:5])
                id3v1 = True
                id3v2 = False
                continue
            if s[0:5] == "ID3v2":
                if (VERBOSE):
                    TRACE("%s tag:" % s1[0:5])
                id3v1 = False
                id3v2 = True
                continue
            if id3v1:
                # this is done to parse the funny format of id3ted v1 tag output
                if type(line) == bytes:
                    s1 = line[0:41].decode('utf8').strip()
                    s2 = line[41:].decode('utf8').strip()
                else:
                    s1 = line[0:41].strip()
                    s2 = line[41:].strip()
                if len(s1) > 0:
                    tag, val = dump_tag(s1)
                    if tag != None: tags[tag] = val
                if len(s2) > 0:
                    tag, val = dump_tag(s2)
                    if tag != None: tags[tag] = val
            else:
                tag, val = dump_tag(s)
                if VERBOSE:
                    TRACE("%s" % s)
                if tag != None: tags[tag] = val

    if not tagfound:
        TRACE("ERROR: no id3v2 tags found for %s\n"
              "TODO: try to copy v1 tag"
              % filename)

    # TODO: return a dict of tags
    return tags

def get_tag(filename, matchtag):

    command = [ ID3TED_FILE, "-l", filename ]
    if VERBOSE:
        dump_command(command)
    proc = subprocess.Popen(command,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            shell=False)

    # capture output, wait for completion, and dump the output
    stdout, stderr = proc.communicate(input=input)
    returncode = proc.returncode
    if VERBOSE:
        TRACE("subprocess.Popen complete; result = %s"
                        % str(returncode))
    if returncode != 0:
        TRACE("subprocess.Popen %s return code(%d) indicates a "
                        "failure.  "
                        "Exiting abnormally."
                        % (command, returncode))
        sys.exit(returncode)

    if VERBOSE:
        TRACE(stdout)
        TRACE(stderr)

    id3v1 = False
    id3v2 = False
    tagfound = False
    retval = None
    out = stdout.decode('utf8')
    for line in out.split('\n'):
        line = line.rstrip()
        if len(line) > 0:
            s = line
            if type(s) == bytes:
                s = line.decode('utf8')
                s1 = line[0:41].decode('utf8').strip()
                s2 = line[41:].decode('utf8').strip()
            else:
                s1 = line[0:41].strip()
                s2 = line[41:].strip()
            tagfound = True
            if s1[0:5] == "ID3v1":
                if (VERBOSE):
                    TRACE("%s tag:" % s1[0:5])
                id3v1 = True
                id3v2 = False
                continue
            if s1[0:5] == "ID3v2":
                if (VERBOSE):
                    TRACE("%s tag:" % s1[0:5])
                id3v1 = False
                id3v2 = True
                continue
            if id3v1:
                if len(s1) > 0:
                    tag, val = dump_tag(s1, matchtag)
                if len(s2) > 0:
                    tag, val = dump_tag(s2, matchtag)
            else:
                tag, val = dump_tag(s, matchtag)
            if val != None:
                if type(val) == str:
                    if len(val) > 0:
                        retval = val
        else:
            break

    if not tagfound:
        TRACE("ERROR: no id3v2 tags found for %s\n"
                        "TODO: try to copy v1 tag"
                        % filename)
        return None
    TRACE("lib.get_tag returning %s" % retval)
    return retval

def set_tag(filename, tag, val):
    command = [ ID3TED_FILE, "--%s" % tag, str(val), filename ]
    if VERBOSE:
        dump_command(command)
    proc = subprocess.Popen(command,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            shell=False)

    # capture output, wait for completion, and dump the output
    stdout, stderr = proc.communicate(input=input)
    returncode = proc.returncode
    if VERBOSE:
        TRACE("subprocess.Popen complete; result = %s"
                        % str(returncode))
    if returncode != 0:
        TRACE("subprocess.Popen %s return code(%d) indicates a "
                        "failure.  "
                        "Exiting abnormally."
                        % (command, returncode))
        sys.exit(returncode)

    out = stdout.decode('utf8')
    if len(out) > 0:
        TRACE("%s stdout:\n%s" % (command, stdout.decode('utf8')))
    err = stderr.decode('utf8')
    if len(err) > 0:
        TRACE("%s stdout:\n%s" % (command, stdout.decode('utf8')))

