#!/usr/bin/env python3
"""
@author: Bryant Hansen
@license: GPLv3

@brief id3ted wrapper: set a single id3v2 tag (either create or overwrite)

@note: warning: this program is heavily-dependent on the output format of id3ted
"""

import sys
from lib.filetags import *

##################################################################
# Constants
#
ME = sys.argv[0]
USAGE = "%s  file  tag  value" % ME


##################################################################
# Argument processing
#
if len(sys.argv) <= 3:
    sys.stderr.write("ERROR: this program requires 3 arguments\n")
    sys.exit(2)

filename = sys.argv[1]
tag = sys.argv[2]
val = sys.argv[3]


##################################################################
# Main
#
result = set_tag(filename, tag, val)
sys.exit(result)
