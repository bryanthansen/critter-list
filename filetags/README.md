
# filetags

These are the scripts currently involved in the tag interface:

  get_tag.py
  list_tags.py
  set_tag.py

They are simply python wrappers for the 

Several python tag libraries were researched & explored

Requirements

They are currently proof-of-concept scripts for the interface, but they seem to work well.

list_tags lists both id3v1 and id3v2 tags
  - one tag/value pair per line
  - id3v1 tags are converted to their id3 equivalent.

Examples:

  > ./set_tag.py tmp/fever.mp3 COMM "test 2"

  > ./list_tags.py ../tmp/fever.mp3 
  TIT2: Fever
  TRCK: 9
  TPE1: Judas Priest
  TDRC: 
  TALB: Screaming For Vengeance
  TCON: Rock (17)
  COMM: 
  TMED: DIG
  TLEN: 321489
  TCON: rock
  TRCK: 9
  TALB: Screaming For Vengeance
  TPE1: Judas Priest
  TIT2: Fever
  COMM: [](XXX): test 2
  TBPM: 111.96

  > ./set_tag.py ../tmp/fever_only_id3v2.mp3 TIT2 "Fever"

TODO: make a python extension for id3ted


